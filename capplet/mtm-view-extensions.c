/*
 *
 * mtm-view-extensions.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#include <config.h>
#include <gnome.h>

#include <mtm/mtm-theme.h>
#include <../mtm/mtm-ext.h>
#include "mtm-selector.h"
#include "mtm-view-extensions.h"
#include "mtm-view-extensions-selector.h"

static void
mtm_view_extensions_update (MtmSelector *selector, GtkWidget *extensions)
{
	MtmTheme *theme;
	MtmExt *ext;
	MtmEnv *env;
	GList *extlist;
	GList *list;
	GList *l;
	GHashTable *tmp;
	gchar *type;
	
	g_return_if_fail (MTM_IS_SELECTOR (selector));
	g_return_if_fail (MTM_IS_EXT_SELECTOR (extensions));

	theme = mtm_selector_get_selected_theme (selector);
	env = mtm_selector_get_env (selector);

	if (theme == NULL) {
		mtm_ext_selector_clear (MTM_EXT_SELECTOR (extensions));
		return;
	}

	g_return_if_fail (MTM_IS_THEME (theme));

	/* Ugly stuff ... Clean */
	extlist = mtm_theme_get_exts (theme);
	
	g_return_if_fail (extlist != NULL);

	tmp = g_hash_table_new (g_str_hash, g_str_equal);

#if 0
	if (sc->priv->fake_exts)
	{
		l = sc->priv->fake_exts;
		while (l)
		{
			gtk_object_destroy (GTK_OBJECT (l->data));
			l = l->next;
		}
		g_list_free (sc->priv->fake_exts);
		sc->priv->fake_exts = NULL;
	}
#endif	
	
	l = extlist;
	while (l)
	{
		ext = MTM_EXT (l->data);
		g_hash_table_insert (tmp, ext->type, ext);
		l = l->next;
	}
	
	list = g_list_copy (extlist);

	l = mtm_env_get_all_ext_handlers (env);
	while (l)
	{
		type = MTM_HANDLER (l->data)->key;
		if (!g_hash_table_lookup (tmp, type))
		{
			ext = mtm_ext_new_with_type (env, type);
			list = g_list_prepend (list, ext);
#if 	0
			sc->priv->fake_exts = g_list_prepend (sc->priv->fake_exts, ext);
#endif	
		}
		l = l->next;
	}
	
 	list = g_list_sort (list, mtm_ext_compare);

	mtm_ext_selector_clear (MTM_EXT_SELECTOR (extensions));

	l = list;
	while (l)
	{
		ext = MTM_EXT (l->data);
		if (ext->handler && !ext->handler->hidden)
			mtm_ext_selector_add_ext (MTM_EXT_SELECTOR (extensions),
								 ext,
								 ext->activate);
		l = l->next;
	}
	
	g_list_free (list);
	g_hash_table_destroy (tmp);
}

GtkWidget *
mtm_view_extensions_new (MtmSelector *selector)
{
	GtkWidget *extensions;

	extensions = mtm_ext_selector_new ();
	mtm_ext_selector_set_options (MTM_EXT_SELECTOR (extensions),
							MTM_EXT_SELECTOR_EXT_TYPE);

	g_signal_connect (G_OBJECT (selector), "selection_changed",
			  (GCallback) mtm_view_extensions_update, extensions);

	return extensions;
}

