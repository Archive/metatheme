/*
 *
 * mtm-ext-selector.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include "mtm-view-extensions-selector.h"
#include <gtk/gtktreeview.h>
#include <gtk/gtkliststore.h>
#include <config.h>
#include <gnome.h>
#include "capplet-marshal.h"

static GtkObjectClass *mtm_ext_selector_parent_class;

struct _MtmExtSelectorPrivate
{
	GtkTreeView *view;
	GtkListStore *store;
	GList *exts;
	MtmExtSelectorOptions options;
};

enum
{
	EXT_TOGGLED,
	LAST_SIGNAL
};

guint mtm_ext_selector_signals[LAST_SIGNAL] = { 0 };

enum
{
	ARG_0,
	ARG_OPTIONS
};

enum
{
	COL_ENABLED,
	COL_FILE,
	COL_TYPE,
	COL_EXT,
	LAST_COL
};

static void mtm_ext_selector_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec);
static void mtm_ext_selector_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec);

static void
mtm_ext_selector_destroy (GtkObject *object)
{
	MtmExtSelector *sel = MTM_EXT_SELECTOR (object);
	
	g_free (sel->priv);

	mtm_ext_selector_parent_class->destroy (object);
}

static void
mtm_ext_selector_class_init (GObjectClass *object_class)
{
	mtm_ext_selector_parent_class = gtk_type_class (gtk_scrolled_window_get_type ());
	
	mtm_ext_selector_signals[EXT_TOGGLED] =
		g_signal_new ("ext_toggled", 
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmExtSelectorClass, ext_toggled),
			      NULL, NULL,
			      capplet_marshal_INT__OBJECT,
			      G_TYPE_INT, 1, MTM_EXT_TYPE);

	object_class->set_property = mtm_ext_selector_set_arg;
	object_class->get_property = mtm_ext_selector_get_arg;

	g_object_class_install_property
		(object_class, ARG_OPTIONS,
		 g_param_spec_int ("options", "options", "options",
		  		   0, 3,
		 		   MTM_EXT_SELECTOR_FILENAME | MTM_EXT_SELECTOR_EXT_TYPE,
				   G_PARAM_READWRITE));

	GTK_OBJECT_CLASS (object_class)->destroy = mtm_ext_selector_destroy;
}

static void
cell_toggled_cb (GtkCellRendererToggle *toggle, const gchar *path, MtmExtSelector *sel)
{
	GtkTreeIter iter;
	MtmExt *ext;
	
	if (!gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (sel->priv->store), &iter, path))
		return;
	
	gtk_tree_model_get (GTK_TREE_MODEL (sel->priv->store), &iter,
			    COL_EXT, &ext, -1);

	mtm_ext_selector_ext_toggled (sel, ext);
}

static void
mtm_ext_selector_init (GObject *object)
{
	MtmExtSelector *sel = MTM_EXT_SELECTOR (object);
	gchar *titles[] = { N_("Use"), N_("File"), N_("Type"), NULL};
	GtkCellRenderer *cell;
	int i;

	gtk_scrolled_window_set_hadjustment (GTK_SCROLLED_WINDOW (sel),
		NULL);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (sel),
		NULL);

	sel->priv = g_new0 (MtmExtSelectorPrivate, 1);
	sel->priv->exts = NULL;
	sel->priv->options = MTM_EXT_SELECTOR_FILENAME | MTM_EXT_SELECTOR_TYPE;
	
	for (i = 0; titles[i] != NULL; i++)
		titles[i] = gettext (titles[i]);
	
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sel),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	
	sel->priv->store = gtk_list_store_new (LAST_COL, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING, MTM_EXT_TYPE);
	sel->priv->view = GTK_TREE_VIEW (gtk_tree_view_new_with_model (GTK_TREE_MODEL (sel->priv->store)));
	gtk_container_add (GTK_CONTAINER (sel), GTK_WIDGET (sel->priv->view));
	
	cell = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (cell), "toggled", (GCallback) cell_toggled_cb, sel);
	gtk_tree_view_insert_column_with_attributes (sel->priv->view,
		-1, titles[COL_ENABLED], cell, "active", COL_ENABLED, NULL);
	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (sel->priv->view,
		-1, titles[COL_FILE], cell, "text", COL_FILE, NULL);
	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (sel->priv->view,
		-1, titles[COL_TYPE], cell, "text", COL_TYPE, NULL);

	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (sel->priv->view), GTK_SELECTION_BROWSE);
	gtk_tree_view_column_set_clickable (gtk_tree_view_get_column (sel->priv->view, COL_ENABLED), TRUE);

	gtk_widget_show (GTK_WIDGET (sel->priv->view));
}

GType
mtm_ext_selector_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmExtSelectorClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_ext_selector_class_init,
			NULL,
			NULL,
			sizeof (MtmExtSelector),
			0,	
			(GInstanceInitFunc) mtm_ext_selector_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_SCROLLED_WINDOW,
					       "MtmExtSelector",
					       &info, 0);
	}

	return type;
}

GtkWidget*
mtm_ext_selector_new (void)
{
	MtmExtSelector *sel = g_object_new (mtm_ext_selector_get_type (), NULL);

	return GTK_WIDGET (sel);
}

static void
mtm_ext_selector_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec)
{
	MtmExtSelector *sel = MTM_EXT_SELECTOR (object);

	switch (arg_id)
	{
		case ARG_OPTIONS:
			mtm_ext_selector_set_options (sel, 
				g_value_get_int (value));
			break;
	}
}

static void
mtm_ext_selector_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec)
{
	MtmExtSelector *sel = MTM_EXT_SELECTOR (object);

	switch (arg_id)
	{
		case ARG_OPTIONS:
			g_value_set_int (value, mtm_ext_selector_get_options (sel));
	}
}

void
mtm_ext_selector_add_ext (MtmExtSelector *sel, MtmExt *ext, gboolean enabled)	
{
	GtkTreeIter iter;
	const gchar *type;

	g_return_if_fail (sel != NULL);
	g_return_if_fail (ext != NULL);

	sel->priv->exts = g_list_append (sel->priv->exts, ext);
	if (ext->handler && MTM_HANDLER (ext->handler)->desc)
		type = MTM_HANDLER (ext->handler)->desc;
        else
		type = ext->type;
	
	gtk_list_store_append (sel->priv->store, &iter);
	gtk_list_store_set (sel->priv->store, &iter,
			    COL_ENABLED, enabled,
			    COL_FILE, ext->file,
			    COL_TYPE, type,
			    COL_EXT, ext,
			    -1);
}

#if 0
void
mtm_ext_selector_remove_ext (MtmExtSelector *sel, MtmExt *ext)	
{
	GList *l, *l2;
	int n = 0;
	
	g_return_if_fail (sel != NULL);
	g_return_if_fail (ext != NULL);

	l = sel->priv->exts;
	
	while (l)
	{
		l2 = l->next;
		if (l->data == ext)
		{
			gtk_clist_remove (sel->priv->clist, n);
			g_list_remove_link (sel->priv->exts, l);
		}
		l = l2;
		n++;
	}
}
#endif

void
mtm_ext_selector_ext_toggled (MtmExtSelector *sel, MtmExt *ext)
{
	int retval = -1;
	GtkTreeIter iter;
	gboolean res;
	
	g_return_if_fail (sel != NULL);
	g_return_if_fail (ext != NULL);
	
	g_signal_emit (GTK_OBJECT (sel), 
		       mtm_ext_selector_signals[EXT_TOGGLED], 0,
		       ext, &retval);
	
	if (retval == -1)
	{
		return;
	}

	for (res = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (sel->priv->store), &iter);
	     res;
	     res = gtk_tree_model_iter_next (GTK_TREE_MODEL (sel->priv->store), &iter))
	{
		MtmExt *e;
		gboolean enabled;
		
		gtk_tree_model_get (GTK_TREE_MODEL (sel->priv->store), &iter,
				    COL_EXT, &e,
				    COL_ENABLED, &enabled,
				    -1);
		
		if (e == ext)
			gtk_list_store_set (sel->priv->store,
					    &iter, COL_ENABLED, !enabled, -1);
	}
}	

MtmExtSelectorOptions mtm_ext_selector_get_options (MtmExtSelector *sel)
{
	g_return_val_if_fail (sel != NULL, 0);
	
	return sel->priv->options;
}

void mtm_ext_selector_set_options (MtmExtSelector *sel, MtmExtSelectorOptions options)
{
	gboolean cols[2] = { FALSE, FALSE };
	int i;

	g_return_if_fail (sel != NULL);

	sel->priv->options = options;
	
	if (options & MTM_EXT_SELECTOR_FILENAME)
		cols[0] = TRUE;
	if (options & MTM_EXT_SELECTOR_EXT_TYPE)
		cols[1] = TRUE;
	
	for (i = 0; i < 2; i++)
		gtk_tree_view_column_set_visible (gtk_tree_view_get_column (sel->priv->view, i + 1), cols[i]);
}

void
mtm_ext_selector_clear (MtmExtSelector *sel)
{
	g_return_if_fail (sel != NULL);

	gtk_list_store_clear (sel->priv->store);

	if (sel->priv->exts)
	{
		g_list_free (sel->priv->exts);
		sel->priv->exts = NULL;
	}
}
