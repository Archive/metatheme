/*
 *
 * mtm-selector-list.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio
 * 	    Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include <string.h>

#include <mtm/mtm-theme.h>
#include "mtm-selector.h"
#include "mtm-control-list.h"

static void
mtm_control_list_selection_changed (GtkMenuItem *item, gpointer data)
{
	MtmSelector *selector = MTM_SELECTOR (data);
	MtmTheme *theme;

	g_return_if_fail (MTM_IS_SELECTOR (data));

	theme = g_object_get_data (G_OBJECT (item), "theme");
	mtm_selector_select_theme (selector, theme);
}
	

static void
mtm_control_list_files_changed (MtmSelector *selector, GtkOptionMenu *option)
{
	MtmTheme *theme;
	GList *themes;
	GList *list;
	GtkMenu *menu;
	GtkWidget *item;
	gchar *name;

	g_return_if_fail (MTM_IS_SELECTOR (selector));
	g_return_if_fail (GTK_IS_OPTION_MENU (option));

	themes = mtm_selector_get_themes (selector);
	list = g_list_copy (themes);

	list = g_list_sort (list, mtm_theme_compare);
	
	menu = GTK_MENU (gtk_menu_new ());
	
	for (; list != NULL; list = list->next) {
		theme = list->data;
		g_return_if_fail (MTM_IS_THEME (theme));

		name = mtm_theme_dup_name (theme);
		item = gtk_menu_item_new_with_label (name);
		g_object_set_data (G_OBJECT (item), "theme", theme);
		g_signal_connect (G_OBJECT (item), "activate",
				  (GCallback)  mtm_control_list_selection_changed,
				  selector);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), GTK_WIDGET (item));
		g_free (name);
	}
	gtk_option_menu_set_menu (option, GTK_WIDGET (menu));

	g_list_free (list);
}

static void new_clicked_cb (GtkWidget *button, MtmSelector *sel)
{
	g_spawn_command_line_async ("metatheme-edit", NULL);
}

static void
edit_clicked_cb (GtkWidget *button, MtmSelector *sel)
{
	MtmTheme *theme;
	gchar *command;
	g_return_if_fail (MTM_IS_SELECTOR (sel));

	theme = mtm_selector_get_selected_theme (sel);
	/* This should never happen */ 
	if (!(theme && theme->filename))
		return;

	command = g_strdup_printf ("metatheme-edit \"%s\"", theme->filename);
	g_spawn_command_line_async (command, NULL);
	g_free (command);
}

static void
manage_clicked_cb (GtkWidget *button, MtmSelector *sel)
{
	gchar *themedir = g_build_filename (g_get_home_dir (), ".metathemes",
					    NULL);
	gchar *command = g_strdup_printf ("nautilus --no-desktop %s",
					  themedir);
	g_spawn_command_line_async (command, NULL);
	g_free (command);
	g_free (themedir);
}


GtkWidget *
mtm_control_list_new (MtmSelector *selector)
{
	GtkWidget *hbox;
	GtkWidget *b_new, *b_edit, *b_manage;
	GtkWidget *option;

	hbox = gtk_hbox_new (FALSE, 4);
	
	b_manage = gtk_button_new_with_label (_("Manage Themes..."));
	g_signal_connect (G_OBJECT (b_manage), "clicked", (GCallback) manage_clicked_cb, selector);
	gtk_box_pack_end (GTK_BOX (hbox), b_manage, FALSE, FALSE, 0);

	b_edit = gtk_button_new_with_label (_("Edit..."));
	g_signal_connect (G_OBJECT (b_edit), "clicked", (GCallback) edit_clicked_cb, selector);
	gtk_box_pack_end (GTK_BOX (hbox), b_edit, FALSE, FALSE, 0);

	b_new = gtk_button_new_with_label (_("New..."));
	g_signal_connect (G_OBJECT (b_new), "clicked", (GCallback) new_clicked_cb, selector);
	gtk_box_pack_end (GTK_BOX (hbox), b_new, FALSE, FALSE, 0);
	
	option = gtk_option_menu_new ();
	gtk_box_pack_start (GTK_BOX (hbox), option, FALSE, FALSE, 0);

	g_signal_connect (GTK_OBJECT (selector), "files_changed",
			  (GCallback) mtm_control_list_files_changed, option);
	g_object_set_data (G_OBJECT (option), "selector", selector);
	g_object_set_data (G_OBJECT (hbox), "option", option);
	
	return hbox;
}

void
mtm_control_list_select_default (GtkWidget *cl)
{
	GList *l;
	GtkWidget *menu;
	int i = 0;
	gchar *current;
	MtmSelector *selector;
	GtkWidget *clist;
	
	g_return_if_fail (cl != NULL);
	
	clist = g_object_get_data (G_OBJECT (cl), "option");
	current = gnome_config_get_string ("/metatheme_selector_capplet/current/filename");
	selector = g_object_get_data (G_OBJECT (clist), "selector");
	
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (clist));
	
	for (l = GTK_MENU_SHELL (menu)->children; l != NULL; l = l->next)
	{
		MtmTheme *theme = MTM_THEME (g_object_get_data (G_OBJECT (l->data), "theme"));
		
		if (!current)
		{
			gtk_option_menu_set_history (GTK_OPTION_MENU (clist), 0);
			mtm_selector_select_theme (selector, theme);
			break;
		}	
		
		if (!strcmp (theme->filename, current))
		{
			gtk_option_menu_set_history (GTK_OPTION_MENU (clist), i);
			mtm_selector_select_theme (selector, theme);
		}

		i++;
	}
}
