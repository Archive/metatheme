/*
 *
 * mtm-selector.c : This is the selector object. You can attach controllers
 *                  like a clist or views like the Preview.
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#include <sys/stat.h> /* for stat() */
#include <dirent.h>   /* for DIR */
#include <config.h>
#include <gnome.h>

#include <../mtm/mtm-env.h>
#include <../mtm/mtm-util.h>
#include <../mtm/mtm-theme.h>

#include "mtm-selector.h"


static GObjectClass *mtm_selector_parent_class;

enum
{
	FILES_CHANGED,
	SELECTION_CHANGED,
	LAST_SIGNAL
};

guint mtm_selector_signals[LAST_SIGNAL] = {0 };

struct _MtmSelectorPrivate {
	MtmEnv *env;
	MtmTheme *selected_theme;
	GList *files;  /* list of gchar's with full path */
	GList *themes; /* list of MtmTheme */
	MtmTheme *backup;
};


static void
mtm_selector_destroy (GtkObject *object)
{
	MtmSelector *mc;

	mc = MTM_SELECTOR (object);
	
	if (mc->priv->backup)
		gtk_object_destroy (GTK_OBJECT (mc->priv->backup));

	gtk_object_destroy (GTK_OBJECT (mc->priv->env));

	g_free (mc->priv);

	if (GTK_OBJECT_CLASS (mtm_selector_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (mtm_selector_parent_class)->destroy) (object);
}

		
static void
mtm_selector_class_init (GObjectClass *object_class)
{
	mtm_selector_parent_class = gtk_type_class (gtk_object_get_type ());

	mtm_selector_signals[SELECTION_CHANGED] =
		g_signal_new ("selection_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmSelectorClass, selection_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	mtm_selector_signals[FILES_CHANGED] =
		g_signal_new ("files_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmSelectorClass, files_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	GTK_OBJECT_CLASS (object_class)->destroy = mtm_selector_destroy;
}

static void
mtm_selector_init (GObject *object)
{
	MtmSelector *mc = MTM_SELECTOR (object);

	mc->priv = g_new0 (MtmSelectorPrivate, 1);

	mc->priv->env = mtm_env_new ();
	mc->priv->files = NULL;
	mc->priv->themes = NULL;
}

GType
mtm_selector_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmSelectorClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_selector_class_init,
			NULL,
			NULL,
			sizeof (MtmSelector),
			0,	
			(GInstanceInitFunc) mtm_selector_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "MtmSelector",
					       &info, 0);
	}

	return type;
}

static gint
mtm_selector_add_file (MtmSelector *selector, gchar *file_name)
{
	struct stat buf;
	MtmTheme *theme;
	GList *list;

	g_return_val_if_fail (MTM_IS_SELECTOR (selector), FALSE);
	g_return_val_if_fail (file_name != NULL, FALSE);

	if (!(stat (file_name, &buf) == 0)) {
		g_warning ("[%s,%i] - Could not add file %s",
				 __FILE__, __LINE__, file_name);
		g_free (file_name);
		return FALSE;
	}

	/* add the theme */
	list = selector->priv->themes;
	theme = mtm_theme_new_from_file (selector->priv->env, file_name);
	if (!MTM_IS_THEME (theme)) {
		g_warning ("Could not load theme from %s\n", file_name);
		g_free (file_name);
		return FALSE;
	}
	list = g_list_prepend (list, theme);
	selector->priv->themes = list;

	/* add the file */
	list = selector->priv->files;
	list = g_list_prepend (list, file_name);
	selector->priv->files = list;

	return FALSE;
}

static void
mtm_selector_add_dir (MtmSelector *selector, const gchar *dir_name)
{
	struct dirent *de;
	gchar *file_name;
	DIR *dir;

	g_return_if_fail (MTM_IS_SELECTOR (selector));
	g_return_if_fail (dir_name != NULL);

	dir = opendir (dir_name);
	if (dir == NULL)
		return;

	while ((de = readdir (dir)))
	{
		if (de->d_name[0] == '.')
			continue;
		
		file_name = g_strconcat (dir_name, "/", de->d_name, NULL);
		mtm_selector_add_file (selector, file_name);
	}
	
	closedir (dir);
}

void
mtm_selector_add_default_dirs (MtmSelector *selector)
{
	gchar *sysdir;
	gchar *userdir;

	g_return_if_fail (MTM_IS_SELECTOR (selector));
				   
	sysdir  = g_strdup_printf ("%s/share/metathemes", mtm_get_prefix ());
	userdir = g_strdup_printf ("%s/.metathemes", g_get_home_dir ());
     /* Create it if it doesn't exist */
	mtm_check_dir (userdir);

	mtm_selector_add_dir (selector, sysdir);
	mtm_selector_add_dir (selector, userdir);
	
	g_free (sysdir);
	g_free (userdir);

}

void
mtm_selector_files_changed (MtmSelector *selector)
{
	g_return_if_fail (MTM_IS_SELECTOR (selector));

	g_signal_emit (GTK_OBJECT (selector),
		       mtm_selector_signals[FILES_CHANGED], 0, NULL);
}


GList *
mtm_selector_get_themes (MtmSelector *selector)
{
	g_return_val_if_fail (MTM_IS_SELECTOR (selector), NULL);

	return selector->priv->themes;
}

MtmEnv *
mtm_selector_get_env (MtmSelector *selector)
{
	g_return_val_if_fail (MTM_IS_SELECTOR (selector), NULL);

	return selector->priv->env;
}

void
mtm_selector_select_theme (MtmSelector *selector, MtmTheme *theme)
{
	if (selector->priv->selected_theme == theme)
		return;

	g_return_if_fail (theme == NULL ||
				   g_list_find (selector->priv->themes, theme));

	selector->priv->selected_theme = theme;
	
	g_signal_emit (GTK_OBJECT (selector),
		       mtm_selector_signals[SELECTION_CHANGED], 0, NULL);

}

void
mtm_selector_unselect_theme (MtmSelector *selector)
{
	mtm_selector_select_theme (selector, NULL);
}



MtmSelector *
mtm_selector_new (void)
{
	MtmSelector *selector;

	selector = g_object_new (mtm_selector_get_type (), NULL);

	return selector;
}

MtmTheme *
mtm_selector_get_selected_theme (MtmSelector *selector)
{
	g_return_val_if_fail (MTM_IS_SELECTOR (selector), NULL);

	return selector->priv->selected_theme;
}

static GtkWidget *
mtm_selector_show_dialog (MtmTheme *theme)
{
	GtkWidget *dialog;
	gchar *message;

	g_return_val_if_fail (MTM_IS_THEME (theme), NULL);

	message = g_strdup_printf (_("Applying theme \"%s\", please wait ..."),
						  theme->name ? theme->name : theme->filename);
	dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_NONE, message);
	gtk_widget_show_now (dialog);
	g_free (message);
	
	while (gtk_events_pending ())
		gtk_main_iteration ();

	return dialog;
}


void
mtm_selector_activate_selected_theme (MtmSelector *selector)
{
	MtmTheme *theme;
	GtkWidget *dialog;

	g_return_if_fail (MTM_IS_SELECTOR (selector));

	theme = mtm_selector_get_selected_theme (selector);

	if (!MTM_IS_THEME (theme))
		return;

	dialog = mtm_selector_show_dialog (theme);

	mtm_theme_activate (theme);
	mtm_theme_update_exts (theme);

	gtk_widget_destroy (dialog);
	
	gnome_config_set_string ("/metatheme_selector_capplet/current/filename",
				 theme->filename);
	gnome_config_sync ();
}


void
mtm_selector_record_current_theme (MtmSelector *selector)
{
	MtmTheme *backup = NULL;
	GList *l;
	gchar *oldcurrent;
	
	g_return_if_fail (MTM_IS_SELECTOR (selector));

	/* No need to make a second backup */
	if (selector->priv->backup)
		return;

	l = mtm_env_get_all_ext_handlers (selector->priv->env);
	while (l)
	{
		MtmExtHandler *handler = MTM_EXT_HANDLER (l->data);
		MtmExt *ext;
		gchar *current;
		
		if (handler->hidden)
		{
			l = l->next;
			continue;
		}
		
		ext = mtm_ext_new_with_type (selector->priv->env, MTM_HANDLER (handler)->key);
		
		current = (* handler->get_current_theme) (handler);
		if (current)
		{
			mtm_ext_set_file (ext, current);
			g_free (current);
		}
		
		mtm_ext_handler_check_args (ext->handler);
		if (ext->handler->n_subclass_args)
		{
		/* This preserves the old ext values (the ext_context
		 * argument values are initialized to the current
		 * ones) */
			g_object_set (G_OBJECT (ext->handler),
				      "ext_context", ext,
				      NULL);
		}
		
		/* Delayed theme initialization in case we never reach this point */
		if (!backup)
			backup = mtm_theme_new (selector->priv->env);

		mtm_theme_add_ext (backup, ext);

		l = l->next;
	}

	selector->priv->backup = backup;
	oldcurrent = gnome_config_get_string ("/metatheme_selector_capplet/current/filename");
	if (oldcurrent)
		gnome_config_set_string ("/metatheme_selector_capplet/current/oldcurrent", oldcurrent);
	gnome_config_sync ();
}


void
mtm_selector_rollback_theme (MtmSelector *selector)
{
	MtmTheme *backup;
	gchar *oldcurrent;
	
	g_return_if_fail (MTM_IS_SELECTOR (selector));
	
	if (!selector->priv->backup)
		return;
	
	backup = selector->priv->backup;

	mtm_theme_activate (backup);
	mtm_theme_update_exts (backup);

	gtk_object_destroy (GTK_OBJECT (backup));
	
	selector->priv->backup = NULL;
	
	oldcurrent = gnome_config_get_string ("/metatheme_selector_capplet/current/oldcurrent");
	if (oldcurrent)
		gnome_config_set_string ("/metatheme_selector_capplet/current/filename", oldcurrent);
	gnome_config_sync ();
}
