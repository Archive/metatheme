#ifndef __MTM_CONTROL_LIST_H__
#define __MTM_CONTROL_LIST_H__

GtkWidget * mtm_control_list_new      (MtmSelector *controller);
void mtm_control_list_select_default (GtkWidget *clist);

#endif /* __MTM_CONTROL_LIST_H__ */
