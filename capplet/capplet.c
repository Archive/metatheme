/*
 *
 * capplet.c : Metatheme capplet.
 *             The mtm-selector is the main object of the metatheme capplet
 *             to this object we attach controls (like control-list) and
 *             views (like preview or info).
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *          Chema Celorio <chema@ximian.com>
 *
 */

#include <config.h>
#include <../mtm/mtm.h>
#if 0
#include <mtm-ui/mtm-ext-selector.h>
#endif
#include <glade/glade.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <gnome.h>
#include "mtm-selector.h"
#include "mtm-control-list.h"
#include "mtm-view-preview.h"
#include "mtm-view-extensions.h"
#include "mtm-view-author.h"

static void
capplet_ok_cb (GtkObject *capplet, MtmSelector *selector)
{
	g_return_if_fail (MTM_IS_SELECTOR (selector));

	mtm_selector_activate_selected_theme (selector);
}

static void
capplet_changed_cb (MtmSelector *selector, GtkDialog *capplet)
{
	g_return_if_fail (MTM_IS_SELECTOR (selector));
	gtk_dialog_set_response_sensitive (GTK_DIALOG (capplet),
		  		     GTK_RESPONSE_APPLY, TRUE);
}

static void
dialog_cb (GtkDialog *dialog, gint response_id, gpointer data)
{
	switch (response_id)
	{
	case GTK_RESPONSE_NONE:
	case GTK_RESPONSE_CLOSE:
		gtk_main_quit ();
		break;
	case GTK_RESPONSE_APPLY:
		capplet_ok_cb (GTK_OBJECT (dialog), data);
		break;
	case GTK_RESPONSE_HELP:
		break;
	}
}

static gchar *set_theme;

int
main (int argc, char **argv)
{
	MtmSelector *selector;
	GtkWidget *main_vbox;
	GtkWidget *capplet;
	GtkWidget *preview;
	GtkWidget *extensions;
	GtkWidget *clist;
	GtkWidget *w;
	GladeXML *xml;
	
	static struct poptOption options[] = {
		{ "set-theme", '\0', POPT_ARG_STRING, &set_theme, 0,
			N_("Set the current theme."), "FILENAME"},
		{ NULL, '\0', 0, NULL, 0}
	};

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	gnome_program_init (argv[0], VERSION, LIBGNOMEUI_MODULE, argc, argv,
			    GNOME_PARAM_POPT_TABLE, options, NULL);
	mtm_init ();

	xml = glade_xml_new (SHAREDIR "mtm-selector.glade", "vbox16", NULL);
	g_return_val_if_fail (xml != NULL, -1);

	main_vbox = glade_xml_get_widget (xml, "vbox16");
	g_return_val_if_fail (GTK_IS_WIDGET (main_vbox), -1);

	/* The selector is the main object, you can then attach controls or views */
	selector = mtm_selector_new ();
	g_return_val_if_fail (MTM_IS_SELECTOR (selector), -1);
	mtm_selector_add_default_dirs (selector);

	/* Add the "available themes" GtkCList control */
	w = glade_xml_get_widget (xml, "optionmenu_vbox");
	g_return_val_if_fail (w != NULL, -1);
	clist = mtm_control_list_new (selector);
	gtk_box_pack_start (GTK_BOX (w), clist, TRUE, TRUE, 0);

	/* Add the Preview window view */
	w = glade_xml_get_widget (xml, "preview_scrolled_window");
	g_return_val_if_fail (w != NULL, -1);
	preview = mtm_view_preview_new (selector);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (w),
								    preview);
	gtk_widget_set_size_request (w, 300, 300);

	/* Add the author view */
	w = glade_xml_get_widget (xml, "author_hbox");
	g_return_val_if_fail (w != NULL, -1);
	//author = mtm_view_author_new (selector);
	//gtk_box_pack_start (GTK_BOX (w), author, FALSE, FALSE, 0);

	/* Add the extensions selector view  */
	w = glade_xml_get_widget (xml, "extensions_window");
	g_return_val_if_fail (w != NULL, -1);
	extensions = mtm_view_extensions_new (selector);
//	gtk_box_pack_start (GTK_BOX (w), extensions,
//					TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (w), extensions);
	/* Get the dialog for the theme info */
	//info_dialog = mtm_view_info_dialog_new (selector);
	
	/* Emit a signal so that we update the list of themes */
	mtm_selector_files_changed (selector);
	mtm_control_list_select_default (clist);
		
	capplet = gtk_dialog_new_with_buttons (_("Desktop theme selector"),
		NULL, -1,
		GTK_STOCK_APPLY, GTK_RESPONSE_APPLY,
		GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
		NULL);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (capplet),
		  		     GTK_RESPONSE_APPLY, TRUE);
	g_signal_connect (G_OBJECT (capplet), "response", (GCallback) dialog_cb, selector);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (capplet)->vbox), main_vbox,
			    TRUE, TRUE, 0);

	g_signal_connect (G_OBJECT (selector), "selection_changed",
			  (GCallback) capplet_changed_cb, capplet);
	capplet_changed_cb (selector, GTK_DIALOG (capplet));
	gtk_widget_show_all (capplet);

	w = glade_xml_get_widget (xml, "button37");
	gtk_widget_hide (w);

	gtk_main ();

	gtk_object_destroy (GTK_OBJECT (selector));

	return 0;
}

