/*
 *
 * mtm-view-preview.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#include <config.h>
#include <gnome.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libart_lgpl/art_alphagamma.h>
#include <libart_lgpl/art_filterlevel.h>
#include <libart_lgpl/art_pixbuf.h>
#include <libart_lgpl/art_rgb_rgba_affine.h>
#include <libart_lgpl/art_affine.h>

#include <mtm/mtm-theme.h>
#include "mtm-selector.h"
#include "mtm-view-preview.h"

typedef struct _MtmPreviewUpdatePix MtmPreviewUpdatePix;

struct _MtmPreviewUpdatePix
{
	GtkWidget *vbox;
	gchar *file_name;
};

/* Returns: TRUE if scaled, FALSE if fits already */
static gboolean
mtm_view_preview_get_scaled_geometry (GdkPixbuf *pix, int the_max,
	int *width, int *height)
{
	int rwidth, rheight;
	
	g_return_val_if_fail (pix != NULL, FALSE);
	g_return_val_if_fail (width != NULL, FALSE);
	g_return_val_if_fail (height != NULL, FALSE);

	rwidth = gdk_pixbuf_get_width (pix);
	rheight = gdk_pixbuf_get_height (pix);

	if (rwidth <= the_max && rheight <= the_max)
		return FALSE;

	if (rwidth > rheight)
	{
		*width = the_max;
		*height = the_max * (((double)rheight)/rwidth);
	}
	else
	{
		*height = the_max;
		*width = the_max * (((double)rwidth)/rheight);
	}
	return TRUE;
}

static gint
mtm_view_preview_update_pixmap (MtmPreviewUpdatePix *data)
{
	GtkWidget *vbox = data->vbox;
	gchar *pix_file = data->file_name;
	static GtkWidget *gtk_pixmap = NULL;
	GdkPixbuf *temp;
	static GdkPixbuf *pix = NULL;
	
	g_return_val_if_fail (GTK_IS_VBOX (vbox), FALSE);
	g_free (data);

	if (pix_file) {
		int width, height;
		temp = gdk_pixbuf_new_from_file (pix_file, NULL);
		if (!mtm_view_preview_get_scaled_geometry (temp, 295,
						      &width, &height))
		{
			pix = temp;
		}
		else
		{
			pix = gdk_pixbuf_scale_simple (temp, width, height, GDK_INTERP_BILINEAR);
			g_object_unref (G_OBJECT (temp));
		}

		if (!pix)
			return FALSE;

		if (gtk_pixmap == NULL) {
			gtk_pixmap = gtk_image_new_from_pixbuf (pix);
			gtk_box_pack_start (GTK_BOX (vbox), gtk_pixmap, FALSE, FALSE, 1);
			gtk_widget_show (gtk_pixmap);
		} else {
			gtk_image_set_from_pixbuf (GTK_IMAGE (gtk_pixmap), pix);
		}
	} else {
		if (gtk_pixmap)
			gtk_widget_destroy (gtk_pixmap);
		gtk_pixmap = NULL;
	}

	if (pix)
		g_object_unref (G_OBJECT (pix));
	pix = NULL;

	return FALSE;
}

static void
mtm_view_preview_update (MtmSelector *selector, GtkWidget *vbox)
{
	MtmPreviewUpdatePix *user_data;
	MtmTheme *theme;
	const gchar *pixname = NULL;

	g_return_if_fail (MTM_IS_SELECTOR (selector));
	g_return_if_fail (GTK_IS_VBOX (vbox));

	theme = mtm_selector_get_selected_theme (selector);

	if (theme != NULL) {
		pixname = mtm_theme_get_preview (theme);
		if (pixname && !g_file_test (pixname, G_FILE_TEST_EXISTS))
			pixname = NULL;
	}

	user_data = g_new (MtmPreviewUpdatePix, 1);
	user_data->vbox = vbox;
	user_data->file_name = (gchar*) pixname;

	gtk_idle_add ((GtkFunction) mtm_view_preview_update_pixmap, user_data);
}

GtkWidget *
mtm_view_preview_new (MtmSelector *selector)
{
	GtkWidget *vbox;

	vbox = gtk_vbox_new (FALSE, 0);
	g_signal_connect (G_OBJECT (selector), "selection_changed",
			  (GCallback) mtm_view_preview_update, vbox);

	return vbox;
}
