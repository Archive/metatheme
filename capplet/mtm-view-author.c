/*
 *
 * mtm-author-view.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <gnome.h>

#include <mtm/mtm-theme.h>
#include "mtm-selector.h"
#include "mtm-view-author.h"

static void
mtm_view_author_selection_changed (MtmSelector *controller, GtkLabel *label)
{
	MtmTheme *theme;
	gchar *text;

	g_return_if_fail (MTM_IS_SELECTOR (controller));
	g_return_if_fail (GTK_IS_LABEL (label));
	
	theme = mtm_selector_get_selected_theme (controller);
	if (!theme) return;

	if (theme->author)
	{
		if (theme->author_email)
			text = g_strdup_printf ("%s <%s>",
						theme->author,
						theme->author_email);
		else
			text = g_strdup (theme->author);
	}
	else
		text = g_strdup (_("Unknown"));

	gtk_label_set_text (label, text);
	g_free (text);
}

GtkWidget *
mtm_view_author_new (MtmSelector *controller)
{
	GtkWidget *label;
	
	g_return_val_if_fail (MTM_IS_SELECTOR (controller), NULL);

	label = gtk_label_new ("");
	gtk_widget_show (label);

	g_signal_connect (G_OBJECT (controller), "selection_changed",
			  (GCallback) mtm_view_author_selection_changed,
			  label);
	return label;
}
