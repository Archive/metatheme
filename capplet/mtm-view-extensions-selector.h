#ifndef __MTM_EXT_SELECTOR_H__
#define __MTM_EXT_SELECTOR_H__

#include <gtk/gtkscrolledwindow.h>
#include <mtm/mtm-ext.h>

#define MTM_EXT_SELECTOR_TYPE		(mtm_ext_selector_get_type ())
#define MTM_EXT_SELECTOR(o)		(GTK_CHECK_CAST ((o), MTM_EXT_SELECTOR_TYPE, MtmExtSelector))
#define MTM_EXT_SELECTOR_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_EXT_SELECTOR_TYPE, MtmExtSelectorClass))
#define MTM_IS_EXT_SELECTOR(o)		(GTK_CHECK_TYPE ((o), MTM_EXT_SELECTOR_TYPE))
#define MTM_IS_EXT_SELECTOR_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_EXT_SELECTOR_TYPE))

typedef enum
{
	MTM_EXT_SELECTOR_FILENAME 	= 1 << 0,
	MTM_EXT_SELECTOR_EXT_TYPE	= 1 << 1,
} MtmExtSelectorOptions;

typedef struct _MtmExtSelector MtmExtSelector;
typedef struct _MtmExtSelectorPrivate MtmExtSelectorPrivate;

typedef struct _MtmExtSelectorClass MtmExtSelectorClass;

struct _MtmExtSelector
{
	GtkScrolledWindow parent;

	MtmExtSelectorPrivate *priv;
};

struct _MtmExtSelectorClass
{
	GtkScrolledWindowClass parent_class;

	/* Callbacks return: 0 for "not enabled" and 1 for "enabled" */
	int (*ext_toggled) (MtmExtSelector *sel, MtmExt *ext);
};

GType mtm_ext_selector_get_type (void);

GtkWidget* mtm_ext_selector_new (void);
	
void mtm_ext_selector_add_ext (MtmExtSelector *sel, MtmExt *ext,
	gboolean enabled);
//void mtm_ext_selector_remove_ext (MtmExtSelector *sel, MtmExt *ext);
void mtm_ext_selector_clear (MtmExtSelector *sel);

void mtm_ext_selector_ext_toggled (MtmExtSelector *sel, MtmExt *ext);

MtmExtSelectorOptions mtm_ext_selector_get_options (MtmExtSelector *sel);
void mtm_ext_selector_set_options (MtmExtSelector *sel, MtmExtSelectorOptions options);

#endif /* __MTM_EXT_SELECTOR_H__ */
