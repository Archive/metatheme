#ifndef __MTM_VIEW_INFO_H__
#define __MTM_VIEW_INFO_H__

GtkWidget * mtm_view_info_new        (MtmSelector *selector);
GtkWidget * mtm_view_info_dialog_new (MtmSelector *selector);

#endif /* __MTM_VIEW_INFO_H__ */




