/*
 *
 * mtm-view-preview.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Chema Celorio <chema@ximian.com>
 *
 */

#include <config.h>
#include <gnome.h>

#include <mtm/mtm-theme.h>
#include "mtm-selector.h"
#include "mtm-view-info.h"

#if 0 /* GtkHTML is crashing on me, use GtkText for now */

static void
mtm_view_info_update (MtmSelector *selector, GtkWidget *html_widget)
{
	static gchar *skel = "<p>%s</p>";
	GtkHTMLStream *handle;
	MtmTheme *theme;
	GtkHTML *html;
	gchar *text;

	html = GTK_HTML (html_widget);
	g_return_if_fail (GTK_IS_HTML (html));

	handle = gtk_html_begin (html);

	theme = mtm_selector_get_selected_theme (selector);

	if (theme == NULL) {
		gtk_html_write (html, handle, " ", 1);
	} else {
		text = g_strdup_printf (skel, "Foo");
		gtk_html_write (html, handle, text, strlen (text));
		g_free (text);
	}

	gtk_html_end (html, handle, GTK_HTML_STREAM_OK);
}

GtkWidget *
mtm_view_info_new (MtmSelector *selector)
{
	GtkWidget *html;
	
	html = gtk_html_new ();

	g_signal_connect (G_OBJECT (selector), "selection_changed",
					mtm_view_info_update, html);

	return html;
}

#else

static void
mtm_view_info_update (MtmSelector *selector, GtkWidget *text_widget)
{
	static gchar *skel = "<p>%s</p>";
	MtmTheme *theme;
	gchar *text;
	gint pos;

	g_return_if_fail (GTK_IS_TEXT (text_widget));

	gtk_editable_delete_text (GTK_EDITABLE (text_widget), 0, -1);
	
	theme = mtm_selector_get_selected_theme (selector);

	if (theme != NULL) {
		text = g_strdup_printf (skel, mtm_theme_get_desc (theme));
		pos = 0;
		gtk_editable_insert_text (GTK_EDITABLE (text_widget),
							 text,
							 strlen (text),
							 &pos);
		g_free (text);
	}
}

GtkWidget *
mtm_view_info_new (MtmSelector *selector)
{
	GtkWidget *text;
	
	text = gtk_text_new (NULL, NULL);

	g_signal_connect (G_OBJECT (selector), "selection_changed",
					mtm_view_info_update, text);

	return text;
}
#endif

/*
 *
 * mtm_view_info_dialog_new:
 * @selector: 
 * 
 * Is the same view, but inside a GnomeDialog
 * 
 * Return Value: 
 **/
GtkWidget *
mtm_view_info_dialog_new (MtmSelector *selector)
{
	GtkWidget *dialog;
	GtkWidget *info;

	dialog = gnome_dialog_new ("Theme Info",
						  GNOME_STOCK_BUTTON_OK,
						  NULL);

	gnome_dialog_close_hides (GNOME_DIALOG (dialog), TRUE);
	gnome_dialog_set_close   (GNOME_DIALOG (dialog), TRUE);

	info = mtm_view_info_new (selector);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
					info, TRUE, TRUE, 10);

	gtk_widget_show (info);
	
	return dialog;
}
