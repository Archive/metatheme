#ifndef __MTM_SELECTOR_H__
#define __MTM_SELECTOR_H__

#define MTM_SELECTOR_TYPE          (mtm_selector_get_type ())
#define MTM_SELECTOR(o)            (GTK_CHECK_CAST ((o), MTM_SELECTOR_TYPE, MtmSelector))
#define MTM_SELECTOR_CLASS(k)      (GTK_CHECK_CLASS_CAST((k), MTM_SELECTOR_TYPE, MtmSelectorClass))
#define MTM_IS_SELECTOR(o)         (GTK_CHECK_TYPE ((o), MTM_SELECTOR_TYPE))
#define MTM_IS_SELECTOR_CLASS(k)   (GTK_CHECK_CLASS_TYPE ((k), MTM_SELECTOR_TYPE))

typedef struct _MtmSelector        MtmSelector;
typedef struct _MtmSelectorPrivate MtmSelectorPrivate;
typedef struct _MtmSelectorClass   MtmSelectorClass;

#include <gtk/gtkobject.h>

struct _MtmSelector
{
	GtkObject parent;

	MtmSelectorPrivate *priv;
};

struct _MtmSelectorClass
{
	GtkObjectClass parent_class;

	void (*selection_changed) (MtmSelector *selector);
	void (*files_changed)     (MtmSelector *selector);
	void (*file_added)        (MtmSelector *selector, gchar *file_name);
};

GType mtm_selector_get_type (void);

MtmSelector*   mtm_selector_new (void);

void           mtm_selector_select_theme       (MtmSelector *selector, MtmTheme *theme);
void           mtm_selector_unselect_theme     (MtmSelector *selector);
void           mtm_selector_activate_selected_theme (MtmSelector *selector);

/* Used for "try/cancel" */
void           mtm_selector_record_current_theme (MtmSelector *selector);
void           mtm_selector_rollback_theme (MtmSelector *selector);

void           mtm_selector_add_default_dirs   (MtmSelector *selector);
void           mtm_selector_files_changed      (MtmSelector *selector);

MtmTheme *     mtm_selector_get_selected_theme (MtmSelector *selector);
MtmEnv *       mtm_selector_get_env            (MtmSelector *selector);
GList *        mtm_selector_get_themes         (MtmSelector *selector);


#endif /* __MTM_SELECTOR_H__ */












