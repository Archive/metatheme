# Galician translation of Metatheme application for GNOME
# Copyright (C) 2001-2002 Free Software Foundation, Inc.
# Manuel A. Fernández Montecelo <manuel@sindominio.net>, 2001-2002.
#
msgid ""
msgstr ""
"Project-Id-Version: Metatheme\n"
"POT-Creation-Date: 2002-05-26 01:23+0200\n"
"PO-Revision-Date: 2002-06-08 08:43+0200\n"
"Last-Translator: Manuel A. Fernández Montecelo <manuel@sindominio.net>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: capplet/capplet.c:93
msgid "Set the current theme."
msgstr "Establecer o tema actual."

#: capplet/capplet.c:153
msgid "Desktop theme selector"
msgstr "Selector de temas do escritorio"

#: capplet/mtm-control-list.c:129
msgid "Manage Themes..."
msgstr "Xestionar temas..."

#: capplet/mtm-control-list.c:133
msgid "Edit..."
msgstr "Editar..."

#: capplet/mtm-control-list.c:137
msgid "New..."
msgstr "Novo..."

#: capplet/mtm-sel.glade.h:1
msgid "Advanced"
msgstr "Avanzado"

#: capplet/mtm-sel.glade.h:2
msgid "Basic"
msgstr "Básico"

#: capplet/mtm-sel.glade.h:3
msgid "Edit Theme..."
msgstr "Editar tema..."

#: capplet/mtm-sel.glade.h:4
msgid "Enable the following extensions:"
msgstr "Activar as seguintes extensións:"

#: capplet/mtm-sel.glade.h:5
msgid "Grab Screenshot..."
msgstr "Capturar pantalla..."

#: capplet/mtm-sel.glade.h:6
msgid "Images"
msgstr "Imaxes"

#: capplet/mtm-sel.glade.h:7
msgid "List"
msgstr "Lista"

#: capplet/mtm-sel.glade.h:8
msgid "New Theme..."
msgstr "Novo tema..."

#: capplet/mtm-sel.glade.h:9
msgid "Preview Image:"
msgstr "Previsualizar imaxe:"

#: capplet/mtm-sel.glade.h:10
msgid "Remove Theme"
msgstr "Eliminar tema"

#: capplet/mtm-sel.glade.h:11
msgid "Save Config..."
msgstr "Gardar configuración..."

#: capplet/mtm-sel.glade.h:12
msgid "Theme Description"
msgstr "Descrición do tema"

#: capplet/mtm-sel.glade.h:13
msgid "Theme Description:"
msgstr "Descrición do tema:"

#: capplet/mtm-sel.glade.h:14
msgid "Theme Name:"
msgstr "Nome do tema:"

#: capplet/mtm-sel.glade.h:15
msgid "Update Theme"
msgstr "Actualizar tema"

#: capplet/mtm-sel.glade.h:16
msgid "View as:"
msgstr "Ver como:"

#: capplet/mtm-selector.c:301
#, c-format
msgid "Applying theme \"%s\", please wait ..."
msgstr "Aplicando tema \"%s\", agarde por favor ..."

#: capplet/mtm-selector.glade.h:1
msgid "Basics"
msgstr "Básicos"

#: capplet/mtm-selector.glade.h:2
msgid "Extensions"
msgstr "Extensións"

#: capplet/mtm-selector.glade.h:3
msgid "Manage themes..."
msgstr "Xestionar temas..."

#: capplet/mtm-selector.glade.h:4
msgid "Metatheme:"
msgstr "Metatema:"

#: capplet/mtm-selector.glade.h:5 libbackground/preview-file-selection.c:198
#: metatheme-edit/metatheme-edit.glade.h:4 plugins/background/translatable.c:8
#: plugins/gtk-2/gtk-config-gui.c:136 plugins/xmms/xmms-config-gui.c:111
msgid "Preview"
msgstr "Previsualizar"

#: capplet/mtm-view-author.c:54
msgid "Unknown"
msgstr "Descoñecido"

#: capplet/mtm-view-extensions-selector.c:124
msgid "Use"
msgstr "Usar"

#: capplet/mtm-view-extensions-selector.c:124
msgid "File"
msgstr "Ficheiro"

#: capplet/mtm-view-extensions-selector.c:124 libbackground/applier.c:239
msgid "Type"
msgstr "Tipo"

#: libbackground/applier.c:240
msgid ""
"Type of bg_applier: BG_APPLIER_ROOT for root window or BG_APPLIER_PREVIEW "
"for preview"
msgstr "Tipo de bg_applier: BG_APPLIER_ROOT para a fiestra raíz (root) ou BG_APPLIER_PREVIEW para previsualizar"

#: libbackground/applier.c:247
msgid "Preview Width"
msgstr "Largura de previsualización"

#: libbackground/applier.c:248
msgid "Width if applier is a preview: Defaults to 64."
msgstr "Largura se o aplicador é unha previsualización: por defecto a 64."

#: libbackground/applier.c:255
msgid "Preview Height"
msgstr "Altura de previsualización"

#: libbackground/applier.c:256
msgid "Height if applier is a preview: Defaults to 48."
msgstr "Altura se o aplicador é unha previsualización: por defecto a 48."

#: libbackground/applier.c:525
msgid "Disabled"
msgstr "Desactivado"

#: libbackground/preview-file-selection.c:193
msgid "Can't find an hbox, using a normal file selection"
msgstr ""
"Non se pode atopar unha hbox (caixa h-orizontal), usando unha escolla normal "
"de ficheiro"

#: metatheme-edit/editor-main-page.c:146
msgid ""
"Select an existing metatheme for your desktop, or create your own. To edit a "
"theme, change the settings in the following tabs, and press \"Save\"."
msgstr ""
"Escolla un metatema existente para o escritorio ou faga un propio. Para "
"editar un tema, mude a configuración nas seguintes pestanas e prema "
"\"Gardar\"."

#: metatheme-edit/editor-page.c:172
msgid "[No information available]"
msgstr "[Non hai información dispoñible]"

#: metatheme-edit/editor-page.c:193
#, c-format
msgid "%s theme: "
msgstr "Tema de %s: "

#: metatheme-edit/editor-shell.c:70
msgid "Metatheme"
msgstr "Metatema"

#: metatheme-edit/main.c:34
#, c-format
msgid "%s - Metatheme Editor"
msgstr "%s - Editor de metatemas"

#: metatheme-edit/main.c:35
msgid "Unnamed"
msgstr "Sen nome"

#: metatheme-edit/metatheme-edit.glade.h:1
msgid "Enabled"
msgstr "Activado"

#: metatheme-edit/metatheme-edit.glade.h:2
msgid "Grab Preview from Screen"
msgstr "Capturar previsualización da pantalla"

#: metatheme-edit/metatheme-edit.glade.h:3
msgid "Metatheme Editor"
msgstr "Editor de metatemas"

#: metatheme-edit/metatheme-edit.glade.h:5
#: plugins/sawfish/sawfish-plugin.c:294
msgid "Sawfish"
msgstr "Sawfish"

#: metatheme-edit/metatheme-edit.glade.h:6
msgid "Sawfish theme: "
msgstr "Tema do Sawfish: "

#: metatheme-edit/metatheme-edit.glade.h:7
msgid "Theme name: "
msgstr "Nome do tema: "

#: metatheme-edit/save.c:19
msgid ""
"Unable to save the metatheme. Please check to make sure you have write "
"permissions to the directory specified."
msgstr ""
"Non se puido gardar o metatema. Asegúrese de ter permisos de escritura "
"no directorio especificado."

#: metatheme-edit/save.c:125
msgid "Theme unsaved. Save before exiting?"
msgstr "Tema non gardado. ¿Gardar antes de saír?"

#: plugins/background/background-plugin.c:171
msgid "Gnome Background Image"
msgstr "Imaxe de fondo de GNOME"

#: plugins/background/background-plugin.c:179
msgid "Background"
msgstr "Fondo"

#: plugins/background/background-plugin.c:180
msgid ""
"Your background is composed of two parts: the background image, and the "
"background pattern, which appears behind the background image."
msgstr ""
"O fondo está composto de dúas partes: a imaxe de fondo e o patrón, que "
"aparece detrás da imaxe de fondo."

#: plugins/background/bg-config-gui.c:179
msgid "Please select a background image"
msgstr "Escolla unha imaxe de fondo"

#: plugins/background/bg-ext-handler.c:203
msgid "Primary Color"
msgstr "Cor primaria"

#: plugins/background/bg-ext-handler.c:203
msgid "Secondary Color"
msgstr "Cor secundaria"

#: plugins/background/bg-ext-handler.c:203
msgid "Use Gradient"
msgstr "Usar gradiente"

#: plugins/background/bg-ext-handler.c:203
msgid "Gradient Type"
msgstr "Tipo de gradiente"

#: plugins/background/bg-ext-handler.c:203
msgid "Wallpaper Layout Type"
msgstr "Tipo de composición do fondo"

#.
#. * Translatable strings file generated by Glade.
#. * Add this file to your project's POTFILES.in.
#. * DO NOT compile it as part of your application.
#.
#: plugins/background/translatable.c:7
msgid "window1"
msgstr "fiestra1"

#: plugins/background/translatable.c:9
msgid "Pattern"
msgstr "Patrón"

#: plugins/background/translatable.c:10
msgid "Type:"
msgstr "Tipo:"

#: plugins/background/translatable.c:11
msgid "Solid color"
msgstr "Cor sólida"

#: plugins/background/translatable.c:12
msgid "Vertical gradient"
msgstr "Gradiente vertical"

#: plugins/background/translatable.c:13
msgid "Horizontal gradient"
msgstr "Gradiente horizontal"

#: plugins/background/translatable.c:14
msgid "Highlight color:"
msgstr "Cor de resalte:"

#: plugins/background/translatable.c:15 plugins/background/translatable.c:17
msgid "Pick a color"
msgstr "Escoller cor"

#: plugins/background/translatable.c:16
msgid "Primary color:"
msgstr "Cor primaria:"

#: plugins/background/translatable.c:18
msgid "Image alignment"
msgstr "Aliñamento da imaxe"

#: plugins/background/translatable.c:19
msgid "Tiled"
msgstr "Mosaico"

#: plugins/background/translatable.c:20
msgid "Centered"
msgstr "Centrada"

#: plugins/background/translatable.c:21
msgid "Scaled"
msgstr "Redimensionada"

#: plugins/background/translatable.c:22
msgid "Scaled (Keep aspect ratio)"
msgstr "Redimensionada (manter aspecto)"

#: plugins/gtk-2/gtk-plugin.c:198
msgid "Gtk+/GNOME 2.0 widgets"
msgstr "Widgets de GTK+/GNOME 2.0"

#: plugins/gtk-2/gtk-plugin.c:207
msgid "Gtk"
msgstr "GTK"

#: plugins/gtk-2/gtk-plugin.c:208
msgid ""
"Gtk widgets are the building blocks which control the overall look of "
"applications. The Gtk+ theme also controls the icons used by applications."
msgstr ""
"Os widgets de GTK son os bloques de construcción que controlan o aspecto "
"visual global das aplicacións. O tema GTK+ tamén controla as iconas que usan "
"as aplicacións."

#: plugins/gtk/gtk-plugin.c:469
msgid "Gtk+/GNOME widgets"
msgstr "Widgets de GTK+/GNOME"

#: plugins/gtk/gtk-plugin.c:478
msgid "Gtk (1.x)"
msgstr "GTK (1.x)"

#: plugins/gtk/gtk-plugin.c:479
msgid ""
"Gtk widgets are the building blocks which control the overall look of "
"applications. This extension only applies to older (1.x) gtk+ applications."
msgstr ""
"Os widgets de GTK son os bloques de construcción que controlan o aspecto "
"visual global das aplicacións. Esta extensión só se aplica ás vellas (1.x) "
"aplicacións GTK+."

#: plugins/nautilus/nautilus-config-gui.c:169
#: plugins/sawfish/sawfish-config-gui.c:149
msgid "Information"
msgstr "Información"

#: plugins/nautilus/nautilus-plugin.c:178
msgid "Nautilus file manager"
msgstr "Xestor de ficheiros Nautilus"

#: plugins/nautilus/nautilus-plugin.c:189
msgid "Nautilus"
msgstr "Nautilus"

#: plugins/nautilus/nautilus-plugin.c:190
msgid ""
"Nautilus is the GNOME file manager. Nautilus also controls the appearance "
"and behavior of desktop icons."
msgstr ""
"Nautilus é un xestor de ficheiros de GNOME. Nautilus tamén controla a "
"aparencia e comportamento das iconas do escritorio."

#: plugins/sawfish/sawfish-plugin.c:285
msgid "Sawfish Window Manager"
msgstr "Xestor de fiestras Sawfish"

#: plugins/sawfish/sawfish-plugin.c:295
msgid ""
"Sawfish is a window manager which controls the appearance and behavior of "
"your windows."
msgstr ""
"Sawfish é un xestor de fiestras que controla a aparencia e comportamento das "
"fiestras."

#: plugins/xmms/xmms-plugin.c:181
msgid "XMMS plugin"
msgstr "Conector (plugin) de XMMS"

#: plugins/xmms/xmms-plugin.c:196
msgid "XMMS skins"
msgstr "Peles (skins) de XMMS"

#: plugins/xmms/xmms-plugin.c:205
msgid "XMMS"
msgstr "XMMS"

#: plugins/xmms/xmms-plugin.c:206
msgid "XMMS is a cross-platform media player."
msgstr "XMMS é un reproductor multimedia para varias plataformas."

#: metatheme-edit/metatheme-edit.desktop.in.h:1
msgid "A program to create and edit desktop themes"
msgstr "Un programa para crear e editar temas de escritorio"

#: metatheme-edit/metatheme-edit.desktop.in.h:2
msgid "Desktop Theme Editor"
msgstr "Editor de temas de escritorio"

#: capplet/metatheme_selector.desktop.in.h:1
msgid "Desktop Theme"
msgstr "Tema de escritorio"

#: capplet/metatheme_selector.desktop.in.h:2
msgid "Selection of GNOME desktop themes"
msgstr "Escolla de temas de escritorio de GNOME"
