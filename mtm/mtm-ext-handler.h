#ifndef __MTM_EXT_HANDLER_H__
#define __MTM_EXT_HANDLER_H__

#define MTM_EXT_HANDLER_TYPE		(mtm_ext_handler_get_type ())
#define MTM_EXT_HANDLER(o)		(GTK_CHECK_CAST ((o), MTM_EXT_HANDLER_TYPE, MtmExtHandler))
#define MTM_EXT_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_EXT_HANDLER_TYPE, MtmExtHandlerClass))
#define MTM_IS_EXT_HANDLER(o)		(GTK_CHECK_TYPE ((o), MTM_EXT_HANDLER_TYPE))
#define MTM_IS_EXT_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_EXT_HANDLER_TYPE))

typedef struct _MtmExtHandler MtmExtHandler;
typedef struct _MtmExtHandlerPrivate MtmExtHandlerPrivate;

typedef struct _MtmExtHandlerClass MtmExtHandlerClass;

#include <mtm/mtm-theme.h>
#include <mtm/mtm-handler.h>
#include <mtm/mtm-util.h>

#define MTM_EXT_HANDLER_ID		"ext"

struct _MtmExtHandler
{
	MtmHandler parent;
	
	MtmResult (*activate)  (MtmExtHandler *handler, MtmExt *ext);
	gchar *   (*find)      (MtmExtHandler *handler, const gchar *name, gboolean is_root);
	MtmResult (*save)      (MtmExtHandler *handler, MtmExt *ext, const gchar *dirname);

	/* If possible, sends an update signal to wm/gtk/whatever */
	MtmResult (*update_ext) (MtmExtHandler *handler);
	
	/* Should return a fully qualified path if possible */
	gchar* (*get_current_theme) (MtmExtHandler *handler);

	gboolean (*ext_is_installed) (MtmExtHandler *handler);
	
	gchar* (*get_ext_version) (MtmExtHandler *handler);
	gboolean (*version_is_compat) (MtmExtHandler *handler,
							 gchar *ver1, gchar *ver2);

	gchar *editcmd; /* A default command to run for an editor */
	gchar *default_suffix; /* for example, .tar.gz */

	/* This member, if TRUE, basically means the handler is only
	 * meant to install and uninstall stuff. It will cause MtmExt's
	 * default "activate" to be FALSE and implies that any user dialogs
	 * should in most cases hide selection of this ext. */
	/* FIXME: Should this be linked to ext_is_installed? */
	gboolean hidden;

	/* You only need this function if you subclass MtmExtHandler and add
	 * arguments. */
	gchar* (*describe_arg) (MtmExtHandler *handler, gchar *arg);

	GParamSpec **subclass_args;
	guint n_subclass_args;
	
	MtmExtHandlerPrivate *priv;
};

struct _MtmExtHandlerClass
{
	MtmHandlerClass parent_class;
};

GType mtm_ext_handler_get_type (void);

MtmExtHandler* mtm_ext_handler_new (MtmEnv *env);
void mtm_ext_handler_construct (MtmExtHandler *handler, MtmEnv *env);

int mtm_ext_handler_default_cleanup (MtmHandler *handler);
MtmResult mtm_ext_handler_default_save (MtmExtHandler *handler,
	MtmExt *ext, const gchar *dirname);

MtmExtHandler* mtm_env_get_ext_handler (MtmEnv *env, gchar *key);
GList* mtm_env_get_all_ext_handlers (MtmEnv *env);

/* Fills in the info about the subclass */
void mtm_ext_handler_check_args (MtmExtHandler *handler);

gchar* mtm_ext_handler_get_arg_name (MtmExtHandler *handler, gchar *fullname);
void mtm_ext_handler_print_arg (MtmExtHandler *handler,
	FILE *file, GValue *arg);

void mtm_ext_handler_register_args (MtmExtHandler *handler, gchar **args);

#endif /* __MTM_EXT_HANDLER_H__ */
