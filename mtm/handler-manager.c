/*
 *
 * handler-manager.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include "handler-manager.h"

static GtkObjectClass *handler_manager_parent_class;

typedef struct {
	GList *handlers;
	GHashTable *keys; /* gchar* -> MtmHandler */
} HandlerList;

struct _HandlerManagerPrivate
{
	GHashTable *types; /* gchar* -> HandlerList */
};

static void
handler_list_destroy (gpointer key, HandlerList *value, gpointer user_data)
{
	GList *l;
	
	l = value->handlers;
	while (l)
	{
		g_object_unref (G_OBJECT (l->data));
		l = l->next;
	}
	g_list_free (l);
	g_hash_table_destroy (value->keys);
	g_free (value);
}

static void
handler_manager_destroy (GtkObject *object)
{
	HandlerManager *hm = HANDLER_MANAGER (object);
	
	g_hash_table_foreach (hm->priv->types, (GHFunc)handler_list_destroy, NULL);
	g_hash_table_destroy (hm->priv->types);
	g_free (hm->priv);

	if (handler_manager_parent_class->destroy)
		(*handler_manager_parent_class->destroy)(object);
}

static void
handler_manager_class_init (GObjectClass *object_class)
{
	handler_manager_parent_class = gtk_type_class (gtk_object_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = handler_manager_destroy;
}

static void
handler_manager_init (GObject *object)
{
	HandlerManager *hm = HANDLER_MANAGER (object);
	hm->priv = g_new0 (HandlerManagerPrivate, 1);
	hm->priv->types = g_hash_table_new (g_str_hash, g_str_equal);
}

GType
handler_manager_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (HandlerManagerClass),
			NULL,
			NULL,
			(GClassInitFunc) handler_manager_class_init,
			NULL,
			NULL,
			sizeof (HandlerManager),
			0,	
			(GInstanceInitFunc) handler_manager_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "HandlerManager",
					       &info, 0);
	}

	return type;
}

HandlerManager* handler_manager_new (void)
{
	return g_object_new (handler_manager_get_type (), NULL);
}

static void
handler_list_add (HandlerList *list, MtmHandler *handler)
{
	g_return_if_fail (list != NULL);
	g_return_if_fail (handler != NULL);
	
	if (g_hash_table_lookup (list->keys, handler->key))
		return;
		
	g_object_ref (G_OBJECT (handler));
	gtk_object_sink (GTK_OBJECT (handler));
	list->handlers = g_list_append (list->handlers, handler);
	g_hash_table_insert (list->keys, handler->key, handler);
}

static HandlerList*
handler_list_new ()
{
	HandlerList *list = g_new0 (HandlerList, 1);
	list->keys = g_hash_table_new (g_str_hash, g_str_equal);
	return list;
}

void
handler_manager_add (HandlerManager *hm, gchar *type, MtmHandler *handler)
{
	HandlerList *list;

	g_return_if_fail (hm != NULL);
	
	list = g_hash_table_lookup (hm->priv->types, type);
	
	if (!list)
	{
		list = handler_list_new ();
		g_hash_table_insert (hm->priv->types, type, list);
	}

	handler_list_add (list, handler);
}

MtmHandler*
handler_manager_get (HandlerManager *hm, const gchar *type, const gchar *key)
{
	HandlerList *list;

	g_return_val_if_fail (hm != NULL, NULL);
	
	list = g_hash_table_lookup (hm->priv->types, type);
	if (!list)
		return NULL;
	return g_hash_table_lookup (list->keys, key);
}

GList*
handler_manager_get_all (HandlerManager *hm, gchar *type)
{
	HandlerList *list;

	g_return_val_if_fail (hm != NULL, NULL);

	list = g_hash_table_lookup (hm->priv->types, type);
	if (!list)
		return NULL;
	return list->handlers;
}

