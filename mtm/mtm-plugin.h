#ifndef __MTM_PLUGIN_H__
#define __MTM_PLUGIN_H__

#define MTM_PLUGIN_TYPE		(mtm_plugin_get_type ())
#define MTM_PLUGIN(o)		(GTK_CHECK_CAST ((o), MTM_PLUGIN_TYPE, MtmPlugin))
#define MTM_PLUGIN_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_PLUGIN_TYPE, MtmPluginClass))
#define MTM_IS_PLUGIN(o)	(GTK_CHECK_TYPE ((o), MTM_PLUGIN_TYPE))
#define MTM_IS_PLUGIN_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_PLUGIN_TYPE))

typedef struct _MtmPlugin MtmPlugin;
typedef struct _MtmPluginPrivate MtmPluginPrivate;

typedef struct _MtmPluginClass MtmPluginClass;

/* The one function needed in all plugins */
extern int mtm_init_plugin (MtmPlugin *plugin);

#include <mtm/mtm-stateful.h>
#include <mtm/mtm-util.h>

struct _MtmPlugin
{
	MtmStateful parent;

	gchar *filename;

	gchar *title;
	
	int (*cleanup_plugin)	(MtmPlugin *plugin);
	gpointer user_data;

	MtmPluginPrivate *priv;
};

struct _MtmPluginClass
{
	MtmStatefulClass parent_class;
};

GType    mtm_plugin_get_type (void);
MtmPlugin* mtm_plugin_new (MtmEnv *env, const gchar *filename);
void       mtm_plugin_construct (MtmPlugin *plugin, MtmEnv *env, const gchar *filename);

/* Default cleanup function:
 * See semantics for mtm_handler_default_cleanup, this
 * does the same thing. 
 */
int mtm_plugin_default_cleanup (MtmPlugin *plugin);

#endif /* __MTM_PLUGIN_H__ */
