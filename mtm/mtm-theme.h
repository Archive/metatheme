#ifndef __MTM_THEME_H__
#define __MTM_THEME_H__

#define MTM_THEME_TYPE		(mtm_theme_get_type ())
#define MTM_THEME(o)		(GTK_CHECK_CAST ((o), MTM_THEME_TYPE, MtmTheme))
#define MTM_THEME_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_THEME_TYPE, MtmThemeClass))
#define MTM_IS_THEME(o)		(GTK_CHECK_TYPE ((o), MTM_THEME_TYPE))
#define MTM_IS_THEME_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_THEME_TYPE))

typedef struct _MtmTheme MtmTheme;
typedef struct _MtmThemePrivate MtmThemePrivate;

typedef struct _MtmThemeClass MtmThemeClass;

#include <mtm/mtm-stateful.h>
#include <mtm/mtm-ext.h>
#include <mtm/mtm-util.h>

struct _MtmTheme
{
	MtmStateful parent;
	
	/* All GtkArg-able */
	gchar *desc; 
	gchar *name; /* "Bob's WonderTheme" */
	gchar *filename;
	gchar *preview;
	gchar *author;
	gchar *author_email;
	
	MtmThemePrivate *priv;
};

struct _MtmThemeClass
{
	MtmStatefulClass parent_class;
};

GType mtm_theme_get_type (void);

MtmTheme *mtm_theme_new (MtmEnv *env);
MtmTheme *mtm_theme_new_from_file (MtmEnv *env, const gchar *filename);
MtmTheme *mtm_theme_new_from_defaults (MtmEnv *env);
void mtm_theme_construct (MtmTheme *theme, MtmEnv *env);
void mtm_theme_construct_from_file (MtmTheme *theme, MtmEnv *env,
	const gchar *filename);
void mtm_theme_construct_from_defaults (MtmTheme *theme, MtmEnv *env);

const gchar* mtm_theme_get_desc (MtmTheme *theme);
        void mtm_theme_set_desc (MtmTheme *theme, const gchar *desc);

const gchar* mtm_theme_get_name (MtmTheme *theme);
      gchar* mtm_theme_dup_name (MtmTheme *theme);
      void   mtm_theme_set_name (MtmTheme *theme, const gchar *name);

const gchar* mtm_theme_get_filename (MtmTheme *theme);
        void mtm_theme_set_filename (MtmTheme *theme, const gchar *filename);

const gchar* mtm_theme_get_preview (MtmTheme *theme);
	void mtm_theme_set_preview (MtmTheme *theme, const gchar *preview);

const gchar* mtm_theme_get_author (MtmTheme *theme);
	void mtm_theme_set_author (MtmTheme *theme, const gchar *author);

const gchar* mtm_theme_get_author_email (MtmTheme *theme);
	void mtm_theme_set_author_email (MtmTheme *theme, const gchar *author_email);

GList *mtm_theme_get_exts (MtmTheme *theme);

void mtm_theme_add_ext (MtmTheme *theme, MtmExt *ext);
void mtm_theme_remove_ext (MtmTheme *theme, MtmExt *ext);

/* The whole MtmResult thing is a bit broken at the moment.
 * In the future, there will be an error reporting service, with
 * nice signals and stuff. */
MtmResult mtm_theme_activate (MtmTheme *theme);
MtmResult mtm_theme_update_exts (MtmTheme *theme);

MtmResult mtm_theme_save (MtmTheme *theme);
MtmResult mtm_theme_save_as (MtmTheme *theme, const gchar *filename);


gint mtm_theme_compare (gconstpointer a, gconstpointer b);

#endif /* __MTM_THEME_H__ */
