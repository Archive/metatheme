#ifndef __HANDLER_MANAGER_H__
#define __HANDLER_MANAGER_H__

typedef struct _HandlerManager HandlerManager;
typedef struct _HandlerManagerPrivate HandlerManagerPrivate;

typedef struct _HandlerManagerClass HandlerManagerClass;

#define HANDLER_MANAGER_TYPE		(handler_manager_get_type ())
#define HANDLER_MANAGER(o)		(GTK_CHECK_CAST ((o), HANDLER_MANAGER_TYPE, HandlerManager))
#define HANDLER_MANAGER_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), HANDLER_MANAGER_TYPE, HAndlerManagerClass))
#define IS_HANDLER_MANAGER(o)		(GTK_CHECK_TYPE ((o), HANDLER_MANAGER__TYPE))
#define IS_HANDLER_MANAGER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), HANDLER_MANAGER_TYPE))

#include <gtk/gtkobject.h>
#include <mtm/mtm-handler.h>

struct _HandlerManager
{
	GtkObject parent;

	HandlerManagerPrivate *priv;
};

struct _HandlerManagerClass
{
	GtkObjectClass parent_class;
};

GType handler_manager_get_type (void);
HandlerManager* handler_manager_new (void);

void handler_manager_add (HandlerManager *hm, gchar *type, MtmHandler *handler);
MtmHandler* handler_manager_get (HandlerManager *hm, const gchar *type, const gchar *key);
GList* handler_manager_get_all (HandlerManager *hm, gchar *type);

#endif /* __HANDLER_MANAGER_H__ */

