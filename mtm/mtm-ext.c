/*
 *
 * mtm-ext.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <string.h>

#include <mtm/mtm-ext.h>
#include <mtm/mtm-ext-handler.h>
#include "internals.h"
#include <gtk/gtksignal.h>

static GObjectClass *mtm_ext_parent_class;

struct _MtmExtPrivate
{
	gboolean user_set_activate;
	gboolean user_set_update_ext;
};

enum
{
	ARG_0,
	ARG_ENABLED,
	ARG_ACTIVATE,
	ARG_UPDATE_EXT,
	ARG_TYPE,
	ARG_DESC,
	ARG_EDITCMD,
	ARG_FILE,
	ARG_URL,
};

enum
{
	CHANGED,
	LAST_SIGNAL
};

guint mtm_ext_signals[LAST_SIGNAL] = {0 };

static void mtm_ext_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec);
static void mtm_ext_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec);

static void
mtm_ext_dispose (GObject *object)
{
	MtmExt *ext = MTM_EXT (object);	

	if (ext->editcmd)
	{
		g_free (ext->editcmd);
		ext->editcmd = NULL;
	}
	
	if (ext->file)
	{
		g_free (ext->file);
		ext->file = NULL;
	}
	
	if (ext->type)
	{
		g_free (ext->type);
		ext->type = NULL;
	}
	
	mtm_ext_parent_class->dispose (object);
}

static void
mtm_ext_finalize (GObject *object)
{
	MtmExt *ext = MTM_EXT (object);
	g_free (ext->priv);

	mtm_ext_parent_class->finalize (object);
}

static void
mtm_ext_class_init (GObjectClass *object_class)
{
	mtm_ext_parent_class = gtk_type_class (mtm_stateful_get_type ());
	
	object_class->set_property = mtm_ext_set_arg;
	object_class->get_property = mtm_ext_get_arg;
	object_class->dispose = mtm_ext_dispose;
	object_class->finalize = mtm_ext_finalize;
	
	g_object_class_install_property
		(object_class, ARG_ACTIVATE,
		 g_param_spec_boolean ("activate",
			 	       "Activate",
				       "Whether to activate theme",
				       TRUE,
				       G_PARAM_READWRITE));

	g_object_class_install_property
		(object_class, ARG_UPDATE_EXT,
		 g_param_spec_boolean ("update_ext",
			 	       "Update ext",
				       "Whether to send an update signal",
				       TRUE,
				       G_PARAM_READWRITE));

	g_object_class_install_property
		(object_class, ARG_TYPE,
		 g_param_spec_string ("type",
			 	      "Type",
				      "The type of theme this ext is.",
				      NULL,
				      G_PARAM_READWRITE));

	g_object_class_install_property
		(object_class, ARG_EDITCMD,
		 g_param_spec_string ("editcmd",
			 	      "Edit command",
				      "Command to execute to edit ext.",
				      NULL,
				      G_PARAM_READWRITE));

	g_object_class_install_property
		(object_class, ARG_FILE,
		 g_param_spec_string ("file",
			 	      "File",
				      "Filename of the ext's theme.",
				      NULL,
				      G_PARAM_READWRITE));

	mtm_ext_signals[CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmExtClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

static void
mtm_ext_init (GObject *object)
{
	MtmExt *ext = MTM_EXT (object);
	ext->priv = g_new0 (MtmExtPrivate, 1);
	ext->priv->user_set_activate = FALSE;
	ext->priv->user_set_update_ext = FALSE;
}

/**
 * mtm_ext_get_type:
 * @void:
 *
 * Registers the #MtmExt class if necessary, and returns the type ID
 * associated to it.
 * 
 * Return value: The type ID of the #MtmExt class.
 */
GType
mtm_ext_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmExtClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_ext_class_init,
			NULL,
			NULL,
			sizeof (MtmExt),
			0,	
			(GInstanceInitFunc) mtm_ext_init,
			NULL
		};

		type = g_type_register_static (MTM_STATEFUL_TYPE,
					       "MtmExt",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_ext_new:
 * @env: The environment object.
 *
 * Creates a new theme extension.
 *
 * Return value: A newly-created theme extension.
 */
MtmExt *
mtm_ext_new (MtmEnv *env)
{
	MtmExt *ext;

	g_return_val_if_fail (env != NULL, NULL);

	ext = g_object_new (mtm_ext_get_type (), NULL);
	mtm_ext_construct (ext, env);
	return ext;
}

/**
 * mtm_ext_construct:
 * @ext: The uninitialized theme extension.
 * @env: The environment object.
 *
 * Intializes the provided theme extension.
 */
void
mtm_ext_construct (MtmExt *ext, MtmEnv *env)
{
	g_return_if_fail (ext != NULL);
	g_return_if_fail (env != NULL);

	MTM_STATEFUL (ext)->env = env;
}

/**
 * mtm_ext_new_with_type:
 * @env: The environment object.
 * @type: The type identifier
 *
 * Creates a new theme extension of the specified type.
 *
 * Return value: A newly-created theme extension.
 */
MtmExt *
mtm_ext_new_with_type (MtmEnv *env, const gchar *type)
{
	MtmExt *ext;
	
	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (type != NULL, NULL);
	
	ext = g_object_new (mtm_ext_get_type (), NULL);
	mtm_ext_construct_with_type (ext, env, type);
	return ext;
}

/**
 * mtm_ext_construct_with_type:
 * @ext: The uninitialized theme extension.
 * @env: The environment object.
 * @type: The type identifier
 * 
 * Intializes the provided theme extension to the specified type.
 */
void
mtm_ext_construct_with_type (MtmExt *ext, MtmEnv *env, const gchar *type)
{
	g_return_if_fail (ext != NULL);
	g_return_if_fail (type != NULL);

	MTM_STATEFUL (ext)->env = env;
	g_object_set (G_OBJECT (ext), "type", type, NULL);
}

static void
mtm_ext_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec)
{
	MtmExt *ext = MTM_EXT (object);
	
	switch (arg_id)
	{
		case ARG_ACTIVATE:
			mtm_ext_set_activate (ext, g_value_get_boolean (value));
			break;
		case ARG_UPDATE_EXT:
			mtm_ext_set_update_ext (ext, g_value_get_boolean (value));
			break;
		case ARG_TYPE:
			mtm_ext_set_ext_type (ext, g_value_get_string (value));
			break;
		case ARG_EDITCMD:
			mtm_ext_set_editcmd (ext, g_value_get_string (value));
			break;
		case ARG_FILE:
			mtm_ext_set_file (ext, g_value_get_string (value));
			break;
	}
}

static void
mtm_ext_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec)
{
	MtmExt *ext = MTM_EXT (object);

	switch (arg_id)
	{
		case ARG_ACTIVATE:
			g_value_set_boolean (value, ext->activate);
			break;
		case ARG_UPDATE_EXT:
			g_value_set_boolean (value, ext->update_ext);
			break;
		case ARG_TYPE:
			g_value_set_string (value, ext->type);
			break;
		case ARG_EDITCMD:
			g_value_set_string (value, ext->editcmd);
			break;
		case ARG_FILE:
			g_value_set_string (value, ext->file);
			break;
	}
}

/**
 * mtm_ext_dup:
 * @ext: The ext to be duplicated
 *
 * Creates a new theme extension which is identical to the one provided.
 * 
 * Return value: A newly-created copy of the provided theme extension.
 */
MtmExt *
mtm_ext_dup (MtmExt *ext)
{
	MtmExt *ret;
	
	g_return_val_if_fail (ext != NULL, NULL);
	
	ret = mtm_ext_new (MTM_STATEFUL (ext)->env);

	if (ext->type)
		mtm_ext_set_ext_type (ret, ext->type);
	
	/* Can't use set here, it will think the user set it */
	/* FIXME: Perhaps in the future we need a way to
	 * "clean" user_set flags from a public api */
	ret->activate = ext->activate;
	ret->priv->user_set_activate = ext->activate;
	ret->update_ext = ext->update_ext;
	ret->priv->user_set_update_ext = ext->update_ext;

	if (ext->editcmd)
		mtm_ext_set_editcmd (ret, ext->editcmd);

	if (ext->file)
		mtm_ext_set_file (ret, ext->file);

	return ret;
}

/**
 * mtm_ext_get_activate:
 * @ext: The theme extension
 *
 * Queries the "activate" flag of the theme extension.
 *
 * Return value: The current activate state.
 */
gboolean
mtm_ext_get_activate (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, FALSE);

	return ext->activate;
}

/**
 * mtm_ext_set_activate:
 * @ext: The theme extension
 * @activate: The new "activate" setting
 *
 * Sets the "activate" flag of the theme extension.
 */
void
mtm_ext_set_activate (MtmExt *ext, gboolean activate)
{
	g_return_if_fail (ext != NULL);

	ext->priv->user_set_activate = TRUE;
	ext->activate = activate;

	mtm_ext_changed (ext);
}

/**
 * mtm_ext_get_update_ext:
 * @ext: The theme extension
 *
 * Queries the "update_ext" flag of the theme extension.
 *
 * Return value: The current update_ext state.
 */
gboolean
mtm_ext_get_update_ext (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, FALSE);

	return ext->update_ext;
}

/**
 * mtm_ext_set_update_ext:
 * @ext: The theme extension
 * @update_ext: The new "update_ext" setting
 *
 * Sets the "update_ext" flag of the theme extension.
 */
void
mtm_ext_set_update_ext (MtmExt *ext, gboolean update_ext)
{
	g_return_if_fail (ext != NULL);

	ext->priv->user_set_update_ext = TRUE;
	ext->update_ext = update_ext;
	mtm_ext_changed (ext);
}

/**
 * mtm_ext_get_ext_type:
 * @ext: The theme extension
 *
 * Queries the extension type 
 *
 * Return value: The extension type.
 */
const gchar *
mtm_ext_get_ext_type (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, NULL);

	return ext->type;
}

/**
 * mtm_ext_set_ext_type:
 * @ext: The theme extension
 * @type: Type type identifier 
 *
 * Sets the extension type to the one provided.
 * If the "enabled", "activate", and "update_ext" flags have
 * not been set by the user, they will be updated to be either TRUE
 * or FALSE based on whether the extension handler of specified type
 * can be found. Otherwise, they will not be touched.
 * The "editcmd" member follows a similar pattern: It will only be
 * updated to reflect the new extension handler if the user has
 * not modified them.
 */
void 
mtm_ext_set_ext_type (MtmExt *ext, const gchar *type)
{
	MtmExtHandler *handler;
	gpointer tmp;
	MtmEnv *env = MTM_STATEFUL (ext)->env;

	g_return_if_fail (ext != NULL);
	g_return_if_fail (type != NULL);

	tmp = handler_manager_get (env->handler_manager,
		MTM_EXT_HANDLER_ID, type);

	if (tmp)
		handler = MTM_EXT_HANDLER (tmp);
	else
		handler = NULL;
	
	ext->handler = handler;
	
	if (ext->type)
		g_free (ext->type);
	ext->type = g_strdup (type);
	
	if (!handler)
	{
		ext->activate = FALSE;
		ext->update_ext = FALSE;
		return;
	}
	
	mtm_ext_handler_check_args (ext->handler);

	/* If the user explicitly disabled enabled, activate, or update_ext,
	 * we shouldn't alter their settings. */
	if (!ext->priv->user_set_activate)
		ext->activate = TRUE;
	if (!ext->priv->user_set_update_ext)
		ext->update_ext = TRUE;
	
	if (handler->hidden)
		ext->activate = FALSE;
	
	/* If this has been set differently, don't touch it. */
	if (ext->handler && ext->handler->editcmd && ext->editcmd)
		if (strcmp (ext->editcmd, ext->handler->editcmd) == 0)
		{
			g_free (ext->editcmd);
			ext->editcmd = g_strdup (handler->editcmd);
		}
	mtm_ext_changed (ext);
}

/**
 * mtm_ext_get_handler:
 * @ext: The extension handler
 *
 * Queries the theme extension for the associated extension handler.
 *
 * Return value: The associated extension handler.
 */
MtmExtHandler* 
mtm_ext_get_handler (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, NULL);

	return ext->handler;
}

/**
 * mtm_ext_get_editcmd:
 * @ext: The extension handler
 *
 * Queries the theme extension for the associated editor command,
 *
 * Return value: The associated editor command.
 */
const gchar *
mtm_ext_get_editcmd (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, NULL);
	
	return ext->editcmd;	
}

/**
 * mtm_ext_get_editcmd:
 * @ext: The extension handler
 * @editcmd: The new editing command.
 *
 * Sets the assoicated editing command. It should be a string of
 * the format "command %s".
 */
void
mtm_ext_set_editcmd (MtmExt *ext, const gchar *editcmd)
{
	g_return_if_fail (ext != NULL);
	g_return_if_fail (editcmd != NULL);
	
	if (ext->editcmd)
		g_free (ext->editcmd);
	ext->editcmd = g_strdup (editcmd);
	mtm_ext_changed (ext);
}

/**
 * mtm_ext_get_file:
 * @ext: The extension handler
 *
 * Queries the theme extension for the filename of the native theme,
 *
 * Return value: The filename of the native theme.
 */
const gchar *
mtm_ext_get_file (MtmExt *ext)
{
	g_return_val_if_fail (ext != NULL, NULL);
	
	return ext->file;	
}

/**
 * mtm_ext_set_update_ext:
 * @ext: The theme extension
 * @update_ext: The filename of the native theme
 *
 * Sets the filename of the native theme.
 */
void
mtm_ext_set_file (MtmExt *ext, const gchar *file)
{
	g_return_if_fail (ext != NULL);
	g_return_if_fail (file != NULL);
	
	if (ext->file)
		g_free (ext->file);
	ext->file = g_strdup (file);
	mtm_ext_changed (ext);
}

void
mtm_ext_changed (MtmExt *ext)
{
	g_return_if_fail (MTM_IS_EXT (ext));

	g_signal_emit (G_OBJECT (ext), mtm_ext_signals[CHANGED], 0);
}

MtmExt *
mtm_ext_new_from_default (MtmEnv *env, gchar *type)
{
	MtmExt *ext;
	
	g_return_val_if_fail (MTM_IS_ENV (env), NULL);
	g_return_val_if_fail (type != NULL, NULL);
	
	ext = g_object_new (mtm_ext_get_type (), NULL);
	mtm_ext_construct_from_default (ext, env, type);

	return ext;
}

void
mtm_ext_construct_from_default (MtmExt *ext, MtmEnv *env, gchar *type)
{
	MtmExtHandler *handler;
	gchar *current, *fullpath, *base;
	
	g_return_if_fail (MTM_IS_EXT (ext));
	g_return_if_fail (MTM_IS_ENV (env));
	g_return_if_fail (type != NULL);

	MTM_STATEFUL (ext)->env = env;

	mtm_ext_set_ext_type (ext, type);
	
	handler = ext->handler;
	if (!(handler && handler->get_current_theme))
		return;
		
	current = handler->get_current_theme (handler);
	if (!current)
		return;
	
	base = g_path_get_basename (current);
	/* Not a full path? */
	if (!strcmp (base, current))
	{
		fullpath = handler->find (handler, current, FALSE);
		if (!fullpath)
			fullpath = handler->find (handler, current, TRUE);
	}
	else
		fullpath = g_strdup (current);

	g_free (base);
	g_free (current);

	if (fullpath)
	{
		mtm_ext_set_file (ext, fullpath);
		g_free (fullpath);
	}
}

gint
mtm_ext_compare (gconstpointer a_ptr, gconstpointer b_ptr)
{
	const MtmExt *a = a_ptr;
	const MtmExt *b = b_ptr;
	gchar *atxt;
	gchar *btxt;

	if (a->handler)
		atxt = MTM_HANDLER (a->handler)->desc;
	else
		atxt = a->type;
	
	if (b->handler)
		btxt = MTM_HANDLER (b->handler)->desc;
	else
		btxt = b->type;

	return strcmp (atxt, btxt);
}


