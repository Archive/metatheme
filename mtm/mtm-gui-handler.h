#ifndef __MTM_GUI_HANDLER_H__
#define __MTM_GUI_HANDLER_H__

#define MTM_GUI_HANDLER_TYPE		(mtm_gui_handler_get_type ())
#define MTM_GUI_HANDLER(o)		(GTK_CHECK_CAST ((o), MTM_GUI_HANDLER_TYPE, MtmGuiHandler))
#define MTM_GUI_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_CAST ((k), MTM_GUI_HANDLER_TYPE, MtmGuiHandlerClass))
#define MTM_IS_GUI_HANDLER(o)		(GTK_CHECK_TYPE ((o), MTM_GUI_HANDLER_TYPE))
#define MTM_IS_GUI_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_GUI_HANDLER_TYPE))

typedef struct _MtmGuiHandler MtmGuiHandler;
typedef struct _MtmGuiHandlerPrivate MtmGuiHandlerPrivate;

typedef struct _MtmGuiHandlerClass MtmGuiHandlerClass;

#include <mtm/mtm-handler.h>
#include <mtm/mtm-config-gui.h>

#define MTM_GUI_HANDLER_ID		"gui"

/* This handler essentially serves as a ConfigGui factory. Any guis obtained
 * via create_gui should be reference counted and not explicitly destroyed.
 */
struct _MtmGuiHandler
{
	MtmHandler parent;

	MtmConfigGui* (*create_gui)	(MtmGuiHandler *handler);
	/* Like a key, but readable */
	gchar *name;

	MtmGuiHandlerPrivate *priv;
};

struct _MtmGuiHandlerClass
{
	MtmHandlerClass parent_class;
};

GType mtm_gui_handler_get_type (void);

MtmGuiHandler* mtm_gui_handler_new (MtmEnv *env);
void mtm_gui_handler_construct (MtmGuiHandler *handler, MtmEnv *env);

MtmGuiHandler* mtm_env_get_gui_handler (MtmEnv *env, gchar *key);
GList* mtm_env_get_all_gui_handlers (MtmEnv *env);

int mtm_gui_handler_default_cleanup (MtmHandler *handler);

#endif /* __MTM_GUI_HANDLER_H__ */
