#ifndef __INST_MANAGER_H__
#define __INST_MANAGER_H__

#define INST_MANAGER_TYPE		(inst_manager_get_type ())
#define INST_MANAGER(o)			(GTK_CHECK_CAST ((o), INST_MANAGER_TYPE, InstManager))
#define INST_MANAGER_CLASS(k)		(GTK_CHECK_CLASS_CAST((k), INST_MANAGER_TYPE, InstManagerClass))
#define IS_INST_MANAGER(o)		(GTK_CHECK_TYPE ((o), INST_MANAGER_TYPE))
#define IS_INST_MANAGER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), INST_MANAGER_TYPE))

typedef struct _InstManager InstManager;
typedef struct _InstManagerPrivate InstManagerPrivate;
typedef struct _InstManagerClass InstManagerClass;

#include <gtk/gtkobject.h>

/* Yes this is a wimpy object. I'm locating the code here so I can
 * have plenty of growing space in the future. */
 
struct _InstManager
{	
	GtkObject parent;

	InstManagerPrivate *priv;
};

struct _InstManagerClass
{
	GtkObjectClass parent_class;
};

GtkType inst_manager_get_type (void);
InstManager* inst_manager_new (void);

/* The MtmTheme functions wrap these. This is where it really happens. */
void inst_manager_set_installed (InstManager *manager, gchar *extfile,
	gchar *ext, gboolean is_root, gchar *installed);
	
gchar *inst_manager_get_installed (InstManager *manager, gchar *extfile,
	gchar *ext, gboolean is_root);

#endif /* __INST_MANAGER_H__ */

