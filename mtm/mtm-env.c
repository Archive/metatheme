/*
 *
 * mtm-env :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>

#include <mtm/mtm-env.h>
#include <mtm/mtm-ext-handler.h>
#include "internals.h"

static GtkObjectClass *mtm_env_parent_class;

/* Destroy handler for MtmEnv.
 * Removes temporary directory, destroys all managers */
static void
mtm_env_destroy (GtkObject *object)
{
	MtmEnv *env = MTM_ENV (object);
	
	mtm_remove_directory (env->tmpdir);

	gtk_object_destroy (GTK_OBJECT (env->handler_manager));
	gtk_object_destroy (GTK_OBJECT (env->plugin_manager));
	g_free (env->tmpdir);

	if (mtm_env_parent_class->destroy)
		(*mtm_env_parent_class->destroy)(object);
}

/* Class initialization function for MtmEnv */
static void
mtm_env_class_init (GObjectClass *object_class)
{
	mtm_env_parent_class = gtk_type_class (gtk_object_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = mtm_env_destroy;
}

/* Object initialization function for MtmEnv */
static void
mtm_env_init (GObject *object)
{
	MtmEnv *env = MTM_ENV (object);
	gchar *home;
	gchar tmpname[] = "/tmp/metatheme-XXXXXX";
	
	close (mkstemp (tmpname));
	unlink (tmpname);
	mkdir (tmpname, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	env->tmpdir = g_strconcat (tmpname, "/", NULL);

	env->handler_manager = handler_manager_new ();
	env->plugin_manager = plugin_manager_new ();

	plugin_manager_load_dir (env->plugin_manager, env, METATHEME_PLUGINS_DIR);
	home = g_strconcat (g_get_home_dir (), "/.metatheme_plugins/", NULL);
	plugin_manager_load_dir (env->plugin_manager, env, home);
	g_free (home);
}

/**
 * mtm_env_get_type:
 * @void:
 *
 * Registers the #MtmEnv class if necessary, and returns the type ID
 * associated to it. 
 * 
 * Return value: The type ID of the #MtmEnv class.
 */
GType
mtm_env_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmEnvClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_env_class_init,
			NULL,
			NULL,
			sizeof (MtmEnv),
			0,	
			(GInstanceInitFunc) mtm_env_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "MtmEnv",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_env_new:
 * @void:
 *
 * Creates a new metatheme environment variable.
 *
 * Return value: the newly-created MtmEnv object.
 */
MtmEnv *
mtm_env_new (void)
{
	return g_object_new (mtm_env_get_type (), NULL);
}

const gchar *
mtm_env_get_tmpdir (MtmEnv *env)
{
	g_return_val_if_fail (MTM_IS_ENV (env), NULL);
	return env->tmpdir;
}
