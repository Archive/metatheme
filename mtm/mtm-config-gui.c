/*
 *
 * mtm-config-gui:
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm-config-gui.h>
#include <gtk/gtksignal.h>

static GObjectClass *mtm_config_gui_parent_class;

struct _MtmConfigGuiPrivate
{
	gulong ext_changed;
};

enum
{
	ARG_0,
	ARG_EXT,
	ARG_FILE_LIST,
	ARG_CONFIG_AREA,
	ARG_FILE_ENTRY
};

enum
{
	SET_EXT,
	SET_CONFIG_AREA,
	SET_FILE_LIST,
	SET_FILE_ENTRY,
	EXT_MODIFIED,
	LAST_SIGNAL
};

guint mtm_config_gui_signals[LAST_SIGNAL] = {0 };

static void mtm_config_gui_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec);
static void mtm_config_gui_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec);

static void mtm_config_gui_ext_changed (MtmConfigGui *gui, MtmExt *ext);

static gchar** mtm_strdupv (const gchar** list);

/* Destroy handler for MtmConfigGui */
static void
mtm_config_gui_dispose (GObject *object)
{
	MtmConfigGui *gui = MTM_CONFIG_GUI (object);
	
	if (gui->file_list)
	{
		g_strfreev (gui->file_list);
		gui->file_list = NULL;
	}
	
	if (gui->ext)
	{
		if (gui->priv->ext_changed)
			g_signal_handler_disconnect (G_OBJECT (gui->ext),
					     gui->priv->ext_changed);
		g_object_unref (G_OBJECT (gui->ext));
		gui->ext = NULL;
	}

	mtm_config_gui_parent_class->dispose (object);
}

static void
mtm_config_gui_finalize (GObject *object)
{
	MtmConfigGui *gui = MTM_CONFIG_GUI (object);
	
	g_free (gui->priv);
	
	mtm_config_gui_parent_class->finalize (object);
}

/* Class initialization function for MtmConfigGui */
static void
mtm_config_gui_class_init (GObjectClass *object_class)
{
	mtm_config_gui_parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->set_property = mtm_config_gui_set_arg;
	object_class->get_property = mtm_config_gui_get_arg;
	object_class->dispose = mtm_config_gui_dispose;
	object_class->finalize = mtm_config_gui_finalize;

	mtm_config_gui_signals[SET_EXT] =
		g_signal_new ("set_ext",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmConfigGuiClass, set_ext),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, MTM_EXT_TYPE);

	mtm_config_gui_signals[SET_CONFIG_AREA] =
		g_signal_new ("set_config_area",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmConfigGuiClass, set_config_area),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, GTK_TYPE_WIDGET);

	mtm_config_gui_signals[SET_FILE_LIST] =
		g_signal_new ("set_file_list",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmConfigGuiClass, set_file_list),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);

	mtm_config_gui_signals[SET_FILE_ENTRY] =
		g_signal_new ("set_file_entry",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmConfigGuiClass, set_file_entry),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, GNOME_TYPE_FILE_ENTRY);

	mtm_config_gui_signals[EXT_MODIFIED] =
		g_signal_new ("ext_modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MtmConfigGuiClass, ext_modified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, MTM_EXT_TYPE);

	g_object_class_install_property
		(object_class, ARG_EXT,
		 g_param_spec_object ("ext",
			 	      "ext",
				      "ext",
				      MTM_EXT_TYPE,
				      G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_FILE_LIST,
		 g_param_spec_pointer ("file_list",
			 	       "file_list",
				       "file_list",
				      G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_CONFIG_AREA,
		 g_param_spec_object ("config_area",
			 	      "config_area",
				      "config_area",
				      GTK_TYPE_WIDGET,
				      G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_FILE_ENTRY,
		 g_param_spec_object ("file_entry",
			 	      "file_entry",
				      "file_entry",
				      GNOME_TYPE_FILE_ENTRY,
				      G_PARAM_READWRITE));

}

static void
mtm_config_gui_init (GObject *object)
{
	MtmConfigGui *gui = MTM_CONFIG_GUI (object);
	
	gui->priv = g_new0 (MtmConfigGuiPrivate, 1);

	gui->config_area = NULL;	
	gui->file_list = NULL;
	gui->priv->ext_changed = 0;
}


/**
 * mtm_config_gui_get_type:
 * @void:
 *
 * Registers the #MtmConfigGui class if necessary, and returns the type ID
 * associated to it.
 *
 * Return value: The type ID of the #MtmConfigGui class.
 */
GType
mtm_config_gui_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmConfigGuiClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_config_gui_class_init,
			NULL,
			NULL,
			sizeof (MtmConfigGui),
			0,	
			(GInstanceInitFunc) mtm_config_gui_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "MtmConfigGui",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_config_gui_new:
 * @void:
 * 
 * Creates a new ext configuration GUI.
 *
 * Return value: A newly created ext configuration GUI
 */
MtmConfigGui*
mtm_config_gui_new (void)
{
	MtmConfigGui *gui;

	gui = g_object_new (mtm_config_gui_get_type (), NULL);
	return gui;
}


/* g_strdup over a NULL-terminated list...counterpart to g_strfreev */
static gchar**
mtm_strdupv (const gchar** list)
{
	gchar **ret;
	int i, len;
	
	g_return_val_if_fail (list != NULL, NULL);
	
	for (len = 0; list[len] != NULL; len++)
	{
	}

	ret = g_new0 (gchar*, len + 1);
	
	for (i = 0; i < len; i++)
		ret[i] = g_strdup (list[i]);

	return ret;
}

/**
 * mtm_config_gui_get_file_list:
 * @gui: The configuration gui
 *
 * Queries the associated list of possible files.
 *
 * Return value: The null-terminated list of files.
 **/
gchar**
mtm_config_gui_get_file_list (MtmConfigGui *gui)
{
	g_return_val_if_fail (MTM_IS_CONFIG_GUI (gui), NULL);

	return gui->file_list;
}

/**
 * mtm_config_gui_set_file_list:
 * @gui: The configuration list
 * @file_list: The new list of possible files (null-terminated)
 *
 * Sets the associated list of possible files to the one provided.
 **/
void
mtm_config_gui_set_file_list (MtmConfigGui *gui, const gchar **file_list)
{
	gchar **new_file_list = NULL;

	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	
	if (gui->file_list)
		g_strfreev (gui->file_list);
	
	if (file_list)
		new_file_list = mtm_strdupv (file_list);
	
	gui->file_list = new_file_list;
}

void
mtm_config_gui_set_ext (MtmConfigGui *gui, MtmExt *ext)
{
	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	g_return_if_fail (MTM_IS_EXT (ext));
	
	/* We disconnect before emitting so as to prevent confusion in
	 * set_ext signal handlers */
	if (gui->ext && gui->priv->ext_changed)
			g_signal_handler_disconnect (G_OBJECT (gui->ext),
					       gui->priv->ext_changed);

	/* We emit first in case any objects need to know the old object */
	g_signal_emit (G_OBJECT (gui), mtm_config_gui_signals[SET_EXT],
		       0, ext);

	if (gui->ext)
		g_object_unref (G_OBJECT (gui->ext));

	gui->ext = ext;
	g_object_ref (G_OBJECT (ext));
	gtk_object_sink (GTK_OBJECT (ext));
	gui->priv->ext_changed = g_signal_connect_swapped (
					G_OBJECT (gui->ext),
					"changed",
					(GCallback) mtm_config_gui_ext_changed,
					GTK_OBJECT (gui));
}

const MtmExt*
mtm_config_gui_get_ext (MtmConfigGui *gui)
{
	g_return_val_if_fail (MTM_IS_CONFIG_GUI (gui), NULL);

	return gui->ext;
}

static void
mtm_config_gui_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec)
{
	MtmConfigGui *gui = MTM_CONFIG_GUI (object);

	switch (arg_id)
	{
		case ARG_EXT:
			mtm_config_gui_set_ext (gui, MTM_EXT (g_value_get_object (value)));
			break;
		case ARG_FILE_LIST:
			mtm_config_gui_set_file_list (gui, g_value_get_pointer (value));
			break;
		case ARG_CONFIG_AREA:
			mtm_config_gui_set_config_area (gui, GTK_WIDGET (g_value_get_object (value)));
			break;
		case ARG_FILE_ENTRY:
			mtm_config_gui_set_file_entry (gui, GNOME_FILE_ENTRY (g_value_get_object (value)));
			break;
	}
}

static void mtm_config_gui_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec)
{
	MtmConfigGui *gui = MTM_CONFIG_GUI (object);

	switch (arg_id)
	{
		case ARG_EXT:
			g_value_set_object (value, G_OBJECT (gui->ext));
			break;
		case ARG_FILE_LIST:
			g_value_set_pointer (value, gui->file_list);
			break;
		case ARG_CONFIG_AREA:
			g_value_set_object (value, G_OBJECT (gui->config_area));
			break;
		case ARG_FILE_ENTRY:
			g_value_set_object (value, G_OBJECT (gui->file_entry));
			break;
	}
}

static void
mtm_config_gui_ext_changed (MtmConfigGui *gui, MtmExt *ext)
{
	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	
	g_signal_emit (GTK_OBJECT (gui), mtm_config_gui_signals[EXT_MODIFIED],
		       0, ext);
}

GtkWidget*
mtm_config_gui_get_config_area (MtmConfigGui *gui)
{
	g_return_val_if_fail (MTM_IS_CONFIG_GUI (gui), NULL);
	
	return gui->config_area;
}

void
mtm_config_gui_set_config_area (MtmConfigGui *gui, GtkWidget *config_area)
{
	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	g_return_if_fail (GTK_IS_WIDGET (config_area));

	g_signal_emit (G_OBJECT (gui),
		       mtm_config_gui_signals[SET_CONFIG_AREA],
		       0, config_area);
	if (gui->config_area)
		g_object_unref (G_OBJECT (gui->config_area));

	gui->config_area = config_area;
	g_object_ref (G_OBJECT (config_area));
	gtk_object_sink (GTK_OBJECT (config_area));
}

GnomeFileEntry*
mtm_config_gui_get_file_entry (MtmConfigGui *gui)
{
	g_return_val_if_fail (MTM_IS_CONFIG_GUI (gui), NULL);

	return gui->file_entry;
}

void
mtm_config_gui_set_file_entry (MtmConfigGui *gui, GnomeFileEntry *entry)
{
	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	g_return_if_fail (GNOME_IS_FILE_ENTRY (entry));

	g_signal_emit (G_OBJECT (gui),
		       mtm_config_gui_signals[SET_FILE_ENTRY],
		       0, entry);

	gui->file_entry = entry;
}

