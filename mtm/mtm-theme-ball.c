/*
 *
 * mtm-theme-ball.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include "mtm-theme-ball.h"
#include "libgnome/libgnome.h"

static MtmStatefulClass *mtm_theme_ball_parent_class;

struct _MtmThemeBallPrivate
{
};

enum
{
	ARG_0,
	ARG_DIRECTORY,
	ARG_FILENAME,
	ARG_THEME
};

static void mtm_theme_ball_set_arg (GtkObject *object, GtkArg *arg, guint arg_id);
static void mtm_theme_ball_get_arg (GtkObject *object, GtkArg *arg, guint arg_id);

static void
mtm_theme_ball_destroy (GtkObject *object)
{
	MtmThemeBall *mtb = MTM_THEME_BALL (object);

	if (mtb->directory)
		g_free (mtb->directory);
	
	if (mtb->filename)
		g_free (mtb->filename);
	
	if (mtb->theme)
		gtk_object_unref (GTK_OBJECT (mtb->theme));

	g_free (mtb->priv);

	if (GTK_OBJECT_CLASS (mtm_theme_ball_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (mtm_theme_ball_parent_class)->destroy) (object);
}

static void
mtm_theme_ball_class_init (GtkObjectClass *object_class)
{
	mtm_theme_ball_parent_class = gtk_type_class (mtm_stateful_get_type ());

	object_class->set_arg = mtm_theme_ball_set_arg;
	object_class->get_arg = mtm_theme_ball_get_arg;

	gtk_object_add_arg_type ("MtmThemeBall::directory",
		GTK_TYPE_STRING, GTK_ARG_READABLE, ARG_DIRECTORY);
	gtk_object_add_arg_type ("MtmThemeBall::filename",
		GTK_TYPE_STRING, GTK_ARG_READWRITE, ARG_FILENAME);
	gtk_object_add_arg_type ("MtmThemeBall::theme",
		MTM_THEME_TYPE, GTK_ARG_READWRITE, ARG_THEME);

	object_class->destroy = mtm_theme_ball_destroy;
}

static void
mtm_theme_ball_init (GtkObject *object)
{
	MtmThemeBall *mtb = MTM_THEME_BALL (object);
	mtb->priv = g_new0 (MtmThemeBallPrivate, 1);
}

GtkType
mtm_theme_ball_get_type (void)
{
	static GtkType type = 0;
	
	if (!type)
	{
		GtkTypeInfo info =
		{
			"MtmThemeBall",
			sizeof (MtmThemeBall),
			sizeof (MtmThemeBallClass),
			(GtkClassInitFunc) mtm_theme_ball_class_init,
			(GtkObjectInitFunc) mtm_theme_ball_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (mtm_stateful_get_type (), &info);
	}

	return type;
}

MtmThemeBall*
mtm_theme_ball_new (MtmEnv *env)
{
	MtmThemeBall *mtb;

	g_return_val_if_fail (env != NULL, NULL);
	
	mtb = gtk_type_new (mtm_theme_ball_get_type ());
	mtm_theme_ball_construct (mtb, env);

	return mtb;
}

void
mtm_theme_ball_construct (MtmThemeBall *mtb, MtmEnv *env)
{
	g_return_if_fail (mtb != NULL);
	g_return_if_fail (env != NULL);

	MTM_STATEFUL (mtb)->env = env;
}

MtmThemeBall*
mtm_theme_ball_new_from_file (MtmEnv *env, gchar *filename)
{
	MtmThemeBall *mtb;

	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (filename != NULL, NULL);

	mtb = gtk_type_new (mtm_theme_ball_get_type ());
	mtm_theme_ball_construct_from_file (mtb, env, filename);

	if (!mtb->theme)
	{
		gtk_object_destroy (GTK_OBJECT (mtb));
		mtb = NULL;
	}

	return mtb;
}

void
mtm_theme_ball_construct_from_file (MtmThemeBall *mtb, MtmEnv *env, gchar *filename)
{
	MtmTheme *theme;
	gchar *index;
	gchar *path;
	gchar *themefile;
	gchar *realfile;
	
	g_return_if_fail (env != NULL);
	g_return_if_fail (filename != NULL);
	g_return_if_fail (mtm_file_is_targz (filename));

	MTM_STATEFUL (mtb)->env = env;

	mtb->directory = mtm_file_open_targz (env, filename, NULL);
	if (!mtb->directory)
		return;
	
	index = g_concat_dir_and_file (mtb->directory, "INDEX");
	if (!g_file_exists (index))
	{
		g_free (index);
		return;
	}
	
	path = g_strconcat ("=", index, "=", "/theme/filename", NULL);
	themefile = gnome_config_get_string (path);
	if (!themefile)
	{
		g_free (path);
		return;
	}
	
	realfile = g_concat_dir_and_file (mtb->directory, themefile);
	g_print ("Realfile: %s\n", realfile);
	theme = mtm_theme_new_from_file (env, realfile);

	g_free (index);
	g_free (path);
	g_free (themefile);
	g_free (realfile);

	if (!theme)
		return;

	mtm_theme_ball_set_theme (mtb, theme);
	mtm_theme_ball_set_filename (mtb, filename);
}

gchar*
mtm_theme_ball_get_filename (MtmThemeBall *mtb)
{
	g_return_val_if_fail (mtb != NULL, NULL);
	
	return mtb->filename;
}

void
mtm_theme_ball_set_filename (MtmThemeBall *mtb, gchar *filename)
{
	g_return_if_fail (mtb != NULL);
	
	if (mtb->filename)
		g_free (mtb->filename);

	if (filename)
		mtb->filename = g_strdup (filename);
}

MtmTheme*
mtm_theme_ball_get_theme (MtmThemeBall *mtb)
{
	g_return_val_if_fail (mtb != NULL, NULL);

	return mtb->theme;
}

void
mtm_theme_ball_set_theme (MtmThemeBall *mtb, MtmTheme *theme)
{
	g_return_if_fail (mtb != NULL);

	if (mtb->theme)
		gtk_object_unref (GTK_OBJECT (mtb->theme));

	if (theme)
	{
		mtb->theme = theme;
		gtk_object_ref (GTK_OBJECT (theme));
		gtk_object_sink (GTK_OBJECT (theme));
	}
}

MtmResult
mtm_theme_ball_save (MtmThemeBall *mtb)
{
	g_return_val_if_fail (mtb != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (mtb->filename != NULL, MTM_GENERAL_ERROR);
	
	g_print ("mtm_theme_ball_save: not implemented yet\n");
	return MTM_OK;
}

MtmResult
mtm_theme_ball_save_as (MtmThemeBall *mtb, gchar *filename)
{
	g_return_val_if_fail (mtb != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (filename != NULL, MTM_GENERAL_ERROR);

	g_print ("mtm_theme_ball_save_as: not implemented yet\n");
	return MTM_OK;
}

gchar*
mtm_theme_ball_get_directory (MtmThemeBall *mtb)
{
	g_return_val_if_fail (mtb != NULL, NULL);

	return mtb->directory;
}

static void
mtm_theme_ball_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	MtmThemeBall *mtb = MTM_THEME_BALL (object);

	switch (arg_id)
	{
		case ARG_FILENAME:
			mtm_theme_ball_set_filename (mtb, GTK_VALUE_STRING (*arg));
			break;
		case ARG_THEME:
			mtm_theme_ball_set_theme (mtb, MTM_THEME (GTK_VALUE_OBJECT (*arg)));
			break;
	}
}

static void
mtm_theme_ball_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	MtmThemeBall *mtb = MTM_THEME_BALL (object);

	switch (arg_id)
	{
		case ARG_DIRECTORY:
			GTK_VALUE_STRING (*arg) = mtm_theme_ball_get_directory (mtb);
			break;
		case ARG_FILENAME:
			GTK_VALUE_STRING (*arg) = mtm_theme_ball_get_filename (mtb);
			break;
		case ARG_THEME:
			GTK_VALUE_OBJECT (*arg) = GTK_OBJECT (mtm_theme_ball_get_theme (mtb));
			break;
	}
}
