#ifndef __MTM_THEME_BALL_H__
#define __MTM_THEME_BALL_H__

#define MTM_THEME_BALL_TYPE		(mtm_theme_ball_get_type ())
#define MTM_THEME_BALL(o)		(GTK_CHECK_CAST ((o), MTM_THEME_BALL_TYPE, MtmThemeBall))
#define MTM_THEME_BALL_CLASS(k)		(GTK_CHECK_CLASS_CAST((k), MTM_THEME_BALL_TYPE, MtmThemeBallClass))
#define MTM_IS_THEME_BALL(o)		(GTK_CHECK_TYPE ((o), MTM_THEME_BALL_TYPE))
#define MTM_IS_THEME_BALL_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_THEME_BALL_TYPE))

typedef struct _MtmThemeBall MtmThemeBall;
typedef struct _MtmThemeBallPrivate MtmThemeBallPrivate;

typedef struct _MtmThemeBallClass MtmThemeBallClass;

#include <mtm/mtm-theme.h>

struct _MtmThemeBall
{
	MtmStateful parent;

	gchar *directory;
	gchar *filename;
	MtmTheme *theme;

	MtmThemeBallPrivate *priv;
};

struct _MtmThemeBallClass
{
	MtmStatefulClass parent_class;
};

GtkType mtm_theme_ball_get_type (void);

MtmThemeBall* mtm_theme_ball_new (MtmEnv *env);
MtmThemeBall* mtm_theme_ball_new_from_file (MtmEnv *env, gchar *filename);

void mtm_theme_ball_construct (MtmThemeBall *mtb, MtmEnv *env);
void mtm_theme_ball_construct_from_file (MtmThemeBall *mtb, MtmEnv *env, gchar *filename);

gchar* mtm_theme_ball_get_directory (MtmThemeBall *mtb);

gchar* mtm_theme_ball_get_filename (MtmThemeBall *mtb);
void mtm_theme_ball_set_filename (MtmThemeBall *mtb, gchar *filename);

MtmTheme* mtm_theme_ball_get_theme (MtmThemeBall *mtb);
void mtm_theme_ball_set_theme (MtmThemeBall *mtb, MtmTheme *theme);

MtmResult mtm_theme_ball_save (MtmThemeBall *mtb);
MtmResult mtm_theme_ball_save_as (MtmThemeBall *mtb, gchar *filename);

#endif /* __MTM_THEME_BALL_H__ */
