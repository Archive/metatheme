#ifndef __MTM_ENV_LIB_H__
#define __MTM_ENV_LIB_H__

#include "plugin-manager.h"
#include "handler-manager.h"

struct _MtmEnv
{
	GtkObject parent;
	
	/* Path to our temporary directory */
	gchar *tmpdir;
	
	PluginManager *plugin_manager;
	HandlerManager *handler_manager;
};

struct _MtmEnvClass
{
	GtkObjectClass parent_class;
};

#endif /* __MTM_ENV_LIB_H__ */
