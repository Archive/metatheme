/*
 *
 * mtm-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm/mtm-plugin.h>
#include <gmodule.h>
#include <string.h>

static MtmStatefulClass *mtm_plugin_parent_class;

struct _MtmPluginPrivate
{
	GModule *handle;
};

static void
mtm_plugin_destroy (GtkObject *object)
{
	MtmPlugin *plugin = MTM_PLUGIN (object);
	gchar *base;
	(*plugin->cleanup_plugin) (plugin);
	/* FIXME: EEEEK */
	base = g_path_get_basename (plugin->filename);
	if (strcmp (base, "libmtmxmms.so") != 0 && strcmp (base, "libnautilus.so") != 0)
	{
		g_module_close (plugin->priv->handle);
	}
	g_free (base);
	g_free (plugin->priv);
	g_free (plugin->filename);

	if (GTK_OBJECT_CLASS(mtm_plugin_parent_class)->destroy)
		(*GTK_OBJECT_CLASS(mtm_plugin_parent_class)->destroy)(object);
}

static void
mtm_plugin_class_init (GObjectClass *object_class)
{
	mtm_plugin_parent_class = gtk_type_class (mtm_stateful_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = mtm_plugin_destroy;
}

static void
mtm_plugin_init (GObject *object)
{
	MtmPlugin *plugin = MTM_PLUGIN (object);

	plugin->priv = g_new0 (MtmPluginPrivate, 1);
	plugin->cleanup_plugin = mtm_plugin_default_cleanup;
}

/**
 * mtm_plugin_get_type:
 * @void:
 *
 * Registers the #MtmPlugin class if necessary, and returns the type ID
 * associated to it.
 * 
 * Return value: The type ID of the #MtmPlugin class.
 **/
GType
mtm_plugin_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmPluginClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_plugin_class_init,
			NULL,
			NULL,
			sizeof (MtmPlugin),
			0,	
			(GInstanceInitFunc) mtm_plugin_init,
			NULL
		};

		type = g_type_register_static (MTM_STATEFUL_TYPE,
					       "MtmPlugin",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_plugin_new:
 * @env: The environment object.
 * @filename: The plugin file to load.
 *
 * Loads a new plugin from the provided filename.
 *
 * Return value: A newly-created plugin.
 **/
MtmPlugin *
mtm_plugin_new (MtmEnv *env, const gchar *filename)
{
	MtmPlugin *plugin;

	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (filename != NULL, NULL);

	plugin = g_object_new (mtm_plugin_get_type (), NULL);
	mtm_plugin_construct (plugin, env, filename);

	return plugin;
}

/**
 * mtm_plugin_construct:
 * @plugin: The uninitialized plugin.
 * @env: The environment object.
 * @filename: The plugin file to load.
 *
 * Intializes the provided plugin.
 **/
void
mtm_plugin_construct (MtmPlugin *plugin, MtmEnv *env,
				  const gchar *filename)
{
	int (*init_func) (MtmPlugin*);
	GModule *handle;

	g_return_if_fail (plugin != NULL);
	g_return_if_fail (env != NULL);
	g_return_if_fail (filename != NULL);

	MTM_STATEFUL (plugin)->env = env;
	
	handle = plugin->priv->handle = g_module_open (filename, 0);
	
	if (!handle)
	{
		g_print ("Canna g_module_open, aborting...\n");
		g_print ("%s\n", g_module_error ());
		return;
	}
	
	if (!g_module_symbol (handle, "mtm_init_plugin",
		(gpointer *)&init_func))
	{
		g_print ("Canna find mtm_init_plugin, aborting...\n");
		g_module_close (handle);
		return;
	}
	
	if (!init_func (plugin))
	{
		g_module_close (handle);
		return;
	}

	plugin->filename = g_strdup (filename);
}

/**
 * mtm_plugin_default_cleanup:
 * @handler: The handler.
 *
 * This is what the "cleanup_plugin" member method of all plugins
 * initially points to. Frees the "title" member if it exists.
 *
 * Return value: 1 if successful, -1 otherwise.
 */
int
mtm_plugin_default_cleanup (MtmPlugin *plugin)
{
	g_return_val_if_fail (plugin != NULL, -1);

	if (plugin->title)
		g_free (plugin->title);
	
	return 1;
}
