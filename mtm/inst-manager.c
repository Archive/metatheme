/*
 *
 * inst-manager.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include "inst-manager.h"
#include "mtm-util.h"
#include <libgnome/libgnome.h>

struct _InstManagerPrivate
{
};

static GtkObjectClass *inst_manager_parent_class;

static void
inst_manager_destroy (GtkObject *object)
{
	if (inst_manager_parent_class->destroy)
		(*inst_manager_parent_class->destroy)(object);
}

static void
inst_manager_class_init (GtkObjectClass *object_class)
{
	inst_manager_parent_class = gtk_type_class (gtk_object_get_type ());
	object_class->destroy = inst_manager_destroy;
}

static void
inst_manager_init (GtkObject *object)
{
}

GtkType
inst_manager_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{	
		GtkTypeInfo info =
		{
			"InstManager",
			sizeof (InstManager),
			sizeof (InstManagerClass),
			(GtkClassInitFunc) inst_manager_class_init,
			(GtkObjectInitFunc) inst_manager_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}
	
InstManager *
inst_manager_new (void)
{
	return gtk_type_new (inst_manager_get_type ());
}

void inst_manager_set_installed (InstManager *manager, gchar *extfile,
	gchar *ext, gboolean is_root, gchar *installed)
{
	gchar *config;
	gchar *key;
	gchar *realfile;
	
	g_return_if_fail (manager != NULL);
	g_return_if_fail (extfile != NULL);
	g_return_if_fail (ext != NULL);
	
	if (is_root)
		config = g_strconcat ("=", SYSCONFDIR, "/metatheme_installs=", NULL);
	else
		config = g_strdup ("/metatheme_installs");

	realfile = mtm_strip_ext (g_basename (extfile), ".tar.gz");
	/* FIXME: Are there any other two-part suffixes? */
	if (!realfile)
		realfile = mtm_strip_ext (g_basename (extfile), NULL);

	key = g_strconcat (config, "/", ext, "/", realfile, NULL);
	gnome_config_pop_prefix ();
	
	if (!installed)
		gnome_config_clean_key (key);
	else
		gnome_config_set_string (key, installed);
		
	gnome_config_sync ();
	
	g_free (key);
	g_free (config);
	g_free (realfile);
}

gchar *inst_manager_get_installed (InstManager *manager, gchar *extfile,
	gchar *ext, gboolean is_root)
{
	gchar *config;
	gchar *key;
	gchar *ret;
	gboolean def = FALSE;
	gchar *realfile;
	
	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (extfile != NULL, NULL);

	if (is_root)
		config = g_strconcat ("=", SYSCONFDIR, "/metatheme_installs=", NULL);
	else
		config = g_strdup ("metatheme_installs");
	
	realfile = mtm_strip_ext (g_basename (extfile), ".tar.gz");
	if (!realfile) 
	{
		realfile = mtm_strip_ext (g_basename (extfile), NULL);
		if (!realfile)
			return NULL;
	}

	key = g_strconcat (config, "/", ext, "/", realfile, NULL);
	gnome_config_pop_prefix ();
	ret = gnome_config_get_string_with_default (key, &def);
	if (def)
	{
		g_free (ret);
		ret = NULL;
	}
	
	g_free (key);
	g_free (config);
	g_free (realfile);
	return ret;
}
