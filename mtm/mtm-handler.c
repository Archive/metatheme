/*
 *
 * mtm-handler.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm/mtm-handler.h>
#include "internals.h"

static MtmStatefulClass *mtm_handler_parent_class;

static void
mtm_handler_destroy (GtkObject *object)
{
	MtmHandler *handler = MTM_HANDLER (object);
	(*handler->cleanup_handler)(handler);

	if (GTK_OBJECT_CLASS(mtm_handler_parent_class)->destroy)
		(*GTK_OBJECT_CLASS(mtm_handler_parent_class)->destroy)(object);
}

static void
mtm_handler_class_init (GObjectClass *object_class)
{
	mtm_handler_parent_class = gtk_type_class (mtm_stateful_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = mtm_handler_destroy;
}

static void
mtm_handler_init (GObject *object)
{
	MtmHandler *handler = MTM_HANDLER (object);
	handler->cleanup_handler = mtm_handler_default_cleanup;
}

/**
 * mtm_handler_get_type:
 * @void:
 *
 * Registers the #MtmHandler class if necessary, and returns the type ID
 * associated to it.
 * 
 * Return value: The type ID of the #MtmHandler class.
 */
GType
mtm_handler_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmHandlerClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_handler_class_init,
			NULL,
			NULL,
			sizeof (MtmHandler),
			0,	
			(GInstanceInitFunc) mtm_handler_init,
			NULL
		};

		type = g_type_register_static (MTM_STATEFUL_TYPE,
					       "MtmHandler",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_handler_register:
 * @handler: The handler object
 *
 * Registers the handler with the current environment.
 * This is a virtual function and must be implemented by subclasses.
 */
void
mtm_handler_register (MtmHandler *handler)
{
	MtmHandlerClass *class;
	g_return_if_fail (handler != NULL);

	class = MTM_HANDLER_CLASS (G_OBJECT_GET_CLASS (handler));
	
	if (class->_register)
		class->_register (handler);
}

/**
 * mtm_handler_default_cleanup:
 * @handler: The handler object
 *
 * Frees "desc" and "title" members if they exist.
 *
 * Return value: 1 if successful, -1 otherwise.
 */
int
mtm_handler_default_cleanup (MtmHandler *handler)
{
	g_return_val_if_fail (handler != NULL, -1);
	if (handler->desc)
		g_free (handler->desc);
	if (handler->key)
		g_free (handler->key);

	return 1;
}

