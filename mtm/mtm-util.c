/*
 *
 * mtm-util.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>

#include <mtm/mtm-util.h>
#include "internals.h"

const gchar*
mtm_get_prefix (void)
{
	return METATHEME_PREFIX;
}

MtmResult
mtm_check_dir (const char *dir)
{
	struct stat buf;
	int statret;
	
	g_return_val_if_fail (dir != NULL, MTM_GENERAL_ERROR);

	statret = stat (dir, &buf);
	if (statret != 0)
	{
		int ret;
		ret = mkdir (dir,
			S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
		if (ret == 0)
			return MTM_OK;
		else
			return MTM_NO_ACCESS;
	}
	else
	{
		if (S_ISDIR (buf.st_mode))
			return MTM_OK;
		else
		{
			return MTM_NO_ACCESS;
		}
	}
}

char*
mtm_readline (FILE *file)
{
	char *line;
	int len = 0;
	int c;
	int i;
	fpos_t pos;
	
	g_return_val_if_fail (file != NULL, NULL);
	
	fgetpos (file, &pos);
	
	c = fgetc (file);
	if (c == EOF)
		return NULL;
	if (c == '\n')
		return g_strdup ("");

	while (c != EOF && c != '\n')
	{
		len++;
		c = fgetc (file);
	}
	
	fsetpos (file, &pos);
	line = g_new0 (char, len + 1);
	for (i = 0; i < len; i++)
		line[i] = fgetc (file);
	fgetc (file); /* Get rid of \n */
	return line;
}

gboolean mtm_file_is_targz (const gchar *filename)
{	
	FILE *file;
	guchar buf[1024];
	
	g_return_val_if_fail (filename != NULL, FALSE);
	
	file = fopen (filename, "r");
	if (file)
	{
		fread (buf, 1, 1000, file);
		fclose (file);
		if ((buf[0] == 31) && (buf[1] == 139))
			return TRUE;
	}
	return FALSE;
}

gchar *mtm_strip_ext (const gchar *filename, const gchar *suffix)
{	
	gchar *ret;
	int flen, elen;
	
	g_return_val_if_fail (filename != NULL, NULL);
	
	flen = strlen (filename);
	
	if (!suffix)
	{
		int i;
		for (i = flen - 1; i >=0; i--)
		{
			if (filename[i] == '/')
				return NULL;

			if (filename[i] == '.')
			{
				ret = g_new0 (gchar, i + 1);
				strncpy (ret, filename, i); 
				return ret;
			}
		}
		return NULL;
	}
				
	elen = strlen (suffix);
	if (g_ascii_strcasecmp (filename + flen - elen, suffix) == 0)
	{
		ret = g_new0 (gchar, flen - elen + 1);
		strncpy (ret, filename, flen - elen);
		return ret;
	}
	return NULL;
}

gchar *mtm_file_open_targz (MtmEnv *env, const gchar *filename, const gchar *suffix)
{
	struct stat st;
	int ret;
	gchar *command;
	gchar *base;
	gchar *dir;
	gchar *theme;
	
	if (stat (filename, &st) != 0)
		return NULL;
	if (S_ISDIR (st.st_mode))
	{
		if (filename[strlen(filename)-1] == '/')
			return g_strdup (filename);
		else
			return g_strconcat (filename, "/", NULL);
	}
	else if (mtm_file_is_targz (filename))
		command = g_strdup_printf ("gzip -d -c < %s | tar xf - -C %s",
			filename, env->tmpdir);
	else
		return NULL;
		
	ret = system (command);
	g_free (command);
	if (ret != 0)
		return NULL;
		
	/* This is an evil assumption here */
	base = g_path_get_basename (filename);
	dir = mtm_strip_ext (base, suffix);
	g_free (base);
	if (!dir)
		return NULL;
		
	theme = g_strconcat (env->tmpdir, dir, "/", NULL);
	g_free (dir);
	return theme;
}

void mtm_file_close_targz (const gchar *dirname)
{
	g_return_if_fail (dirname != NULL);
	
	mtm_remove_directory (dirname);
}

/* TODO: Clean this function up */
gchar *mtm_file_untargz (const gchar *filename, const gchar *todir, const gchar *suffix)
{
	gchar *command, *dir, *ret;
	gchar *base;
	
	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (todir != NULL, NULL);
	g_return_val_if_fail (suffix != NULL, NULL);

	command = g_strdup_printf ("gzip -d -c < \"%s\" | tar xf - -C \"%s\"",
		filename, todir);
	system (command);
	
	base = g_path_get_basename (filename);
	dir = mtm_strip_ext (base, suffix);
	g_free (base);
	if (!dir)
		return NULL;
	
	ret = g_strconcat (todir, "/", dir, "/", NULL);
	g_free (dir);
	return ret;
}

gchar *
mtm_find_file_in_path (gchar **path, const gchar *name, gboolean is_root)
{
	gchar *tmp;
	gboolean exists;
	struct stat buf;
	gchar *ret = NULL;
	int i = 0;

	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	while (path[i])
	{
		if (is_root)
			tmp = g_strconcat (path[i], "/", name, NULL);
		else
			tmp = g_strconcat (g_get_home_dir (), "/",
				path[i], "/", name, NULL);

		exists = (stat (tmp, &buf) == 0);
		if (exists)
		{
			ret = tmp;
			break;
		}
		g_free (tmp);
		i++;
	}
	return ret;
}

MtmResult
mtm_remove_directory (const gchar *dirname)
{
	DIR *dir;
	struct dirent *dent;
	struct stat buf;
	gchar *path;
	
	g_return_val_if_fail (dirname != NULL, MTM_GENERAL_ERROR);

	dir = opendir (dirname);
	if (!dir)
		return MTM_NO_ACCESS;
	
	for (dent = readdir (dir); dent != NULL; dent = readdir (dir))
	{
		if (!strcmp (dent->d_name, ".") || !strcmp (dent->d_name, ".."))
			continue;
		
		path = g_strconcat (dirname, "/", dent->d_name, NULL);
		
		if (stat (path, &buf) != 0)
		{
			g_free (path);
			closedir (dir);
			return MTM_NO_ACCESS;
		}
		
		if (S_ISDIR (buf.st_mode))
		{
			MtmResult res = mtm_remove_directory (path);
			if (res != MTM_OK)
			{
				g_free (path);
				closedir (dir);
				return res;
			}
		}
		else if (remove (path) != 0)
		{
			g_free (path);
			closedir (dir);
			return MTM_NO_ACCESS;
		}
		
		g_free (path);
	}
	closedir (dir);
	
	remove (dirname);
	
	return MTM_OK;
}

MtmResult
mtm_copy_file (const gchar *fromfile, const gchar *tofile)
{
	FILE *ifile, *ofile;
	guchar c;
	struct stat buf;
	
	g_return_val_if_fail (fromfile != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (tofile != NULL, MTM_GENERAL_ERROR);

	/* Do not copy the same file to itself */	
	if (!strcmp (fromfile, tofile))
		return MTM_OK;
		
	if (stat (fromfile, &buf) != 0)
		return MTM_NO_ACCESS;

	ifile = fopen (fromfile, "r");
	if (!ifile)
		return MTM_GENERAL_ERROR;
	
	ofile = fopen (tofile, "w");
	if (!ofile)
		return MTM_GENERAL_ERROR;

	while (fread (&c, 1, 1, ifile))
		fwrite (&c, 1, 1, ofile);

	fclose (ifile);
	fclose (ofile);
	
	chmod (tofile, buf.st_mode);

	return MTM_OK;
}

MtmResult
mtm_move_file (const gchar *fromfile, const gchar *tofile)
{
	g_return_val_if_fail (fromfile != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (tofile != NULL, MTM_GENERAL_ERROR);

	if (mtm_copy_file (fromfile, tofile) == MTM_OK)
		unlink (fromfile);

	return MTM_OK;
}

MtmResult
mtm_copy_directory (const gchar *fromdir, const gchar *todir)
{
	DIR *dir;
	struct dirent *dent;
	struct stat buf;
	gchar *path, *topath;
	
	g_return_val_if_fail (fromdir != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (todir != NULL, MTM_GENERAL_ERROR);

	if (!strcmp (fromdir, todir))
		return MTM_OK;

	if (stat (fromdir, &buf) != 0 || !S_ISDIR (buf.st_mode))
	       return MTM_NO_ACCESS;
	
	dir = opendir (fromdir);
	if (!dir)
		return MTM_NO_ACCESS;
	
	mkdir (todir, buf.st_mode);

	for (dent = readdir (dir); dent != NULL; dent = readdir (dir))
	{
		if (!strcmp (dent->d_name, ".") || !strcmp (dent->d_name, ".."))
			continue;
		
		path = g_strconcat (fromdir, "/", dent->d_name, NULL);
		
		if (stat (path, &buf) != 0)
		{
			g_free (path);
			closedir (dir);
			return MTM_NO_ACCESS;
		}
		
		topath = g_strconcat (todir, "/", dent->d_name, NULL);
		
		if (S_ISDIR (buf.st_mode))
		{
			MtmResult res = mtm_copy_directory (path, topath);
			if (res != MTM_OK)
			{
				g_free (path);
				g_free (topath);
				closedir (dir);
				return res;
			}
		}
		else
		{	
			MtmResult res = mtm_copy_file (path, topath);
			if (res != MTM_OK)
			{
				g_free (path);
				closedir (dir);
				return res;
			}
		}
		
		g_free (path);
		g_free (topath);
	}
	closedir (dir);
	
	return MTM_OK;
}
