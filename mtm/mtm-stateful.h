#ifndef __MTM_STATEFUL_H__
#define __MTM_STATEFUL_H__

#define MTM_STATEFUL_TYPE	(mtm_stateful_get_type ())
#define MTM_STATEFUL(o)		(GTK_CHECK_CAST ((o), MTM_STATEFUL_TYPE, MtmStateful))
#define MTM_STATEFUL_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_STATEFUL_TYPE, MtmStatefulClass)
#define MTM_IS_STATEFUL(o)	(GTK_CHECK_TYPE ((o), MTM_STATEFUL_TYPE))
#define MTM_IS_STATEFUL_CLASS	(GTK_CHECK_CLASS_TYPE ((k), MTM_STATEFUL_TYPE))

typedef struct _MtmStateful MtmStateful;
typedef struct _MtmStatefulPrivate MtmStatefulPrivate;
typedef struct _MtmStatefulClass MtmStatefulClass;

#include <gtk/gtkobject.h>
#include <mtm/mtm-env.h>

struct _MtmStateful
{
	GtkObject parent;

	MtmEnv *env;
	
	MtmStatefulPrivate *priv;
};

struct _MtmStatefulClass
{
	GtkObjectClass parent_class;
};

GType mtm_stateful_get_type (void);

MtmEnv *mtm_stateful_get_env (MtmStateful *stateful);
void mtm_stateful_set_env (MtmStateful *stateful, MtmEnv *env);

#endif /* __MTM_STATEFUL_H__ */
