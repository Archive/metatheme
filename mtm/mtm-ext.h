#ifndef __MTM_EXT_H__
#define __MTM_EXT_H__

#define MTM_EXT_TYPE		(mtm_ext_get_type ())
#define MTM_EXT(o)		(GTK_CHECK_CAST ((o), MTM_EXT_TYPE, MtmExt))
#define MTM_EXT_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_EXT_TYPE, MtmExtClass))
#define MTM_IS_EXT(o)		(GTK_CHECK_TYPE ((o), MTM_EXT_TYPE))
#define MTM_IS_EXT_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_EXT_TYPE))

typedef struct _MtmExt MtmExt;
typedef struct _MtmExtPrivate MtmExtPrivate;

typedef struct _MtmExtClass MtmExtClass;

#include <mtm/mtm-stateful.h>
#include <mtm/mtm-ext-handler.h>

struct _MtmExt
{
	MtmStateful parent;
	
	/* These are all GtkArg-enabled */
	gboolean activate; /* Whether to activate*/
	gboolean update_ext; /* You get the idea now */
	
	/* Note that if you change the type, the stuff mirrored from the
	 * handler changes auto-magically.
	 * This will not affect editcmd if it has been changed
	 * from the default. */
	gchar *type;
	MtmExtHandler *handler;
	gchar *editcmd;

	gchar *file;
	
	MtmExtPrivate *priv;
};

struct _MtmExtClass
{
	void (*changed) (MtmExt *ext);

	MtmStatefulClass parent_class;
};

GType mtm_ext_get_type (void);

MtmExt * mtm_ext_new (MtmEnv *env);
void     mtm_ext_construct (MtmExt *ext, MtmEnv *env);

MtmExt * mtm_ext_new_from_default (MtmEnv *env, gchar *type);
void     mtm_ext_construct_from_default (MtmExt *ext, MtmEnv *env, gchar *type);

MtmExt * mtm_ext_new_with_type (MtmEnv *env, const gchar *type);
void     mtm_ext_construct_with_type (MtmExt *ext, MtmEnv *env, const gchar *type);

MtmExt *mtm_ext_dup (MtmExt *ext);

/* Hooray for accessor functions! You can
 * safely ignore the ::get ones. But *please* use the ::set ones. */
gboolean mtm_ext_get_activate (MtmExt *ext);
void     mtm_ext_set_activate (MtmExt *ext, gboolean activate);

gboolean mtm_ext_get_update_ext (MtmExt *ext);
void     mtm_ext_set_update_ext (MtmExt *ext, gboolean update_ext);

const gchar *mtm_ext_get_ext_type (MtmExt *ext);
void   mtm_ext_set_ext_type (MtmExt *ext, const gchar *type);

MtmExtHandler* mtm_ext_get_handler (MtmExt *ext);

const gchar *mtm_ext_get_editcmd (MtmExt *ext);
void   mtm_ext_set_editcmd (MtmExt *ext, const gchar *editcmd);

const gchar *mtm_ext_get_file (MtmExt *ext);
void   mtm_ext_set_file (MtmExt *ext, const gchar *file);

void mtm_ext_changed (MtmExt *ext);

gint mtm_ext_compare (gconstpointer a_ptr, gconstpointer b_ptr);

#endif /* __MTM_EXT_H__ */
