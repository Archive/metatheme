#ifndef __MTM_HANDLER_H__
#define __MTM_HANDLER_H__

#define MTM_HANDLER_TYPE	(mtm_handler_get_type ())
#define MTM_HANDLER(o)		(GTK_CHECK_CAST ((o), MTM_HANDLER_TYPE, MtmHandler))
#define MTM_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_HANDLER_TYPE, MtmHandlerClass))
#define MTM_IS_HANDLER(o)	(GTK_CHECK_TYPE ((o), MTM_HANDLER_TYPE))
#define MTM_IS_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_HANDLER_TYPE))

typedef struct _MtmHandler MtmHandler;
typedef struct _MtmHandlerPrivate MtmHandlerPrivate;

typedef struct _MtmHandlerClass MtmHandlerClass;

#include <mtm/mtm-stateful.h>

struct _MtmHandler {
	MtmStateful parent;
	
	int (*cleanup_handler)  (MtmHandler *handler);
	
	gchar *desc;
	gchar *key;
	
	gpointer user_data; /* This will be sent to your functions */
	
	MtmHandlerPrivate *priv;
};

struct _MtmHandlerClass {
	MtmStatefulClass parent_class;

	/* Virtual method */
	void (*_register) (MtmHandler *handler);
};

GType mtm_handler_get_type (void);

void mtm_handler_register (MtmHandler *handler);

/* Default cleanup function:
 * This is what 'cleanup_handler' initially points to, it is also 
 * useful to manually call in custom cleanup funcs.
 * Destroys members such as desc, key, etc if they
 * are non-NULL. Does not touch user_data -- thus if you are allocing it,
 * you need to hook up a custom cleanup to free it. 
 */
int mtm_handler_default_cleanup (MtmHandler *handler);

#endif /* __MTM_HANDLER_H__ */
