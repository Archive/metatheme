#ifndef __MTM_ENV_H__
#define __MTM_ENV_H__

#define MTM_ENV_TYPE		(mtm_env_get_type ())
#define MTM_ENV(o)		(GTK_CHECK_CAST ((o), MTM_ENV_TYPE, MtmEnv))
#define MTM_ENV_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), MTM_ENV_TYPE, MtmEnvClass))
#define MTM_IS_ENV(o)		(GTK_CHECK_TYPE ((o), MTM_ENV_TYPE))
#define MTM_IS_ENV_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_ENV_TYPE))

typedef struct _MtmEnv MtmEnv;
typedef struct _MtmEnvClass MtmEnvClass;

#include <gtk/gtkobject.h>

/* This is a completely opaque type. */

GType mtm_env_get_type (void);
MtmEnv *mtm_env_new (void);
const gchar *mtm_env_get_tmpdir (MtmEnv *env);

#endif /* __MTM_ENV_H__ */ 
