/*
 *
 * mtm-stateful.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm/mtm-stateful.h>

static GtkObjectClass *mtm_stateful_parent_class;

enum
{
	ARG_0,
	ARG_ENV
};

static void mtm_stateful_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec);
static void mtm_stateful_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec);

static void
mtm_stateful_class_init (GObjectClass *object_class)
{
	mtm_stateful_parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->set_property = mtm_stateful_set_arg;
	object_class->get_property = mtm_stateful_get_arg;

	g_object_class_install_property
		(object_class, ARG_ENV,
		 g_param_spec_object ("env",
			 	      "Environment",
				      "The stateful object's environment",
				      MTM_ENV_TYPE,
				      G_PARAM_READWRITE));
}

static void
mtm_stateful_init (GObject *object)
{
};

/**
 * mtm_stateful_get_type:
 * @void:
 *
 * Registers the #MtmStateful class if necessary, and returns the type ID
 * associated to it.
 * 
 * Return value: The type ID of the #MtmStateful class.
 **/
GType
mtm_stateful_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmStatefulClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_stateful_class_init,
			NULL,
			NULL,
			sizeof (MtmStateful),
			0,	
			(GInstanceInitFunc) mtm_stateful_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "MtmStateful",
					       &info, 0);
	}

	return type;
}

static void
mtm_stateful_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec)
{
	MtmStateful *stateful = MTM_STATEFUL (object);

	switch (arg_id)
	{
		case ARG_ENV:
			mtm_stateful_set_env (stateful, g_value_get_object (value));
			break;
	}
}

static void
mtm_stateful_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec)
{
	MtmStateful *stateful = MTM_STATEFUL (object);

	switch (arg_id)
	{
		case ARG_ENV:
			g_value_set_object (value, G_OBJECT (mtm_stateful_get_env (stateful)));
			break;
	}
}

/**
 * mtm_stateful_get_env:
 * @stateful: The stateful object.
 *
 * Queries the stateful object for the current environment object.
 *
 * Return value: The current environment object.
 */
MtmEnv *mtm_stateful_get_env (MtmStateful *stateful)
{
	g_return_val_if_fail (stateful != NULL, NULL);
	return stateful->env;
}

/**
 * mtm_stateful_set_env:
 * @stateful: The stateful object.
 * @env: An environment object.
 *
 * Sets the current environment object to the one provided.
 */
void mtm_stateful_set_env (MtmStateful *stateful, MtmEnv *env)
{
	g_return_if_fail (stateful != NULL);
	g_return_if_fail (env != NULL);
	
	stateful->env = env;
}

