#ifndef __MTM_CONFIG_GUI_H__
#define __MTM_CONFIG_GUI_H__

#define MTM_CONFIG_GUI_TYPE		(mtm_config_gui_get_type ())
#define MTM_CONFIG_GUI(o)		(GTK_CHECK_CAST ((o), MTM_CONFIG_GUI_TYPE, MtmConfigGui))
#define MTM_CONFIG_GUI_CLASS(k)		(GTK_CHECK_CLASS_CAST ((k), MTM_CONFIG_GUI_TYPE, MtmConfigGuiClass))
#define MTM_IS_CONFIG_GUI(o)		(GTK_CHECK_TYPE ((o), MTM_CONFIG_GUI_TYPE))
#define MTM_IS_CONFIG_GUI_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), MTM_CONFIG_GUI_TYPE))

typedef struct _MtmConfigGui MtmConfigGui;
typedef struct _MtmConfigGuiPrivate MtmConfigGuiPrivate;

typedef struct _MtmConfigGuiClass MtmConfigGuiClass;

#include <mtm/mtm-ext.h>
#include <gtk/gtkwidget.h>
#include <libgnomeui/gnome-file-entry.h>

struct _MtmConfigGui
{
	GtkObject parent;
	
	MtmExt *ext;
	GtkWidget *config_area;
	gchar **file_list;
	GnomeFileEntry *file_entry;

	MtmConfigGuiPrivate *priv;
};

struct _MtmConfigGuiClass
{
	GtkObjectClass parent_class;

	/* Signals */
	void (*set_ext) (MtmConfigGui *gui, MtmExt *ext);
	void (*set_config_area) (MtmConfigGui *gui, GtkWidget *config_area);
	void (*set_file_list) (MtmConfigGui *gui, const gchar** file_list);
	void (*set_file_entry) (MtmConfigGui *gui, GnomeFileEntry *entry);

	/*void (*select_file) (MtmConfigGui *gui, const gchar *filename);*/
	void (*ext_modified) (MtmConfigGui *gui, MtmExt *ext);
};

GType mtm_config_gui_get_type (void);

MtmConfigGui* mtm_config_gui_new (void);

void mtm_config_gui_set_file_list (MtmConfigGui *gui, const gchar **file_list);
gchar** mtm_config_gui_get_file_list (MtmConfigGui *gui);

void mtm_config_gui_set_ext (MtmConfigGui *gui, MtmExt *ext);
const MtmExt* mtm_config_gui_get_ext (MtmConfigGui *gui);

GtkWidget* mtm_config_gui_get_config_area (MtmConfigGui *gui);
void mtm_config_gui_set_config_area (MtmConfigGui *gui, GtkWidget *widget);

GnomeFileEntry* mtm_config_gui_get_file_entry (MtmConfigGui *gui);
void mtm_config_gui_set_file_entry (MtmConfigGui *gui, GnomeFileEntry *entry);

#endif /* __MTM_CONFIG_GUI_H__ */
