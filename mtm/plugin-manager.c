/*
 *
 * plugin-manager.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <sys/types.h>
#include <dirent.h>
#include <string.h>

#include "plugin-manager.h"
#include <mtm/mtm-util.h>

static GtkObjectClass *plugin_manager_parent_class;

struct _PluginManagerPrivate
{
	GList *plugins; /* MtmPlugin */
	GHashTable *files; /* gchar* -> MtmPlugin */
};

gboolean
plugin_manager_load (PluginManager *manager, MtmPlugin *plugin)
{
	g_return_val_if_fail (manager != NULL, FALSE);
	g_return_val_if_fail (plugin != NULL, FALSE);
	
	/* Already loaded? */
	if (g_hash_table_lookup (manager->priv->files, plugin->filename)) 		
		return TRUE;
	
	manager->priv->plugins = g_list_append (manager->priv->plugins, plugin);
	g_hash_table_insert (manager->priv->files, plugin->filename, plugin);
	
	g_object_ref (G_OBJECT (plugin));
	gtk_object_sink (GTK_OBJECT (plugin));

	return TRUE;
}

/* This could be optimized if we want to expose it to the world */
static gboolean
plugin_manager_unload (PluginManager *manager, MtmPlugin *plugin)
{
	GList *l;
	
	g_return_val_if_fail (manager != NULL, FALSE);
	g_return_val_if_fail (plugin != NULL, FALSE);
	
	l = g_list_find (manager->priv->plugins, plugin);
	if (!l) /* Not loaded with us */
		return FALSE;

	if (manager->priv->plugins == l)
	{
		manager->priv->plugins = l->next;
		if (l->next)
			l->next->prev = NULL;
	}
	else	
		manager->priv->plugins = g_list_remove_link (manager->priv->plugins, l);
	g_list_free_1 (l);
	g_hash_table_remove (manager->priv->files, plugin->filename);
	g_object_unref (G_OBJECT (plugin));
	
	return TRUE;
}

gboolean
plugin_manager_load_dir (PluginManager *manager, MtmEnv *env,
					const gchar *dirname)
{
	DIR *dir;
	struct dirent *de;
	gchar *filename;

	g_return_val_if_fail (manager != NULL, FALSE);
	g_return_val_if_fail (dirname != NULL, FALSE);
	
	if (mtm_check_dir (dirname) != MTM_OK)
		return FALSE;
		
	dir = opendir (dirname);
	if (!dir)
		return FALSE;
	
	while ((de = readdir (dir)))
	{
		if (strncmp (de->d_name + strlen (de->d_name) - 3, ".so", 3) != 0)
			continue;
		filename = g_strconcat (dirname, de->d_name, NULL);
		plugin_manager_load_file (manager, env, filename);
		g_free (filename);
	}
	closedir (dir);

	return TRUE;
}

gboolean
plugin_manager_load_file (PluginManager *manager, MtmEnv *env,
					 const gchar *filename)
{
	MtmPlugin *plugin;
	gboolean ret;

	g_return_val_if_fail (manager != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	plugin = mtm_plugin_new (env, filename);
	if (!plugin->filename)
	{
		gtk_object_destroy (GTK_OBJECT (plugin));
		return FALSE;
	}

	ret = plugin_manager_load (manager, plugin);
		
	return ret;
}

static void
plugin_manager_destroy (GtkObject *object)
{
	PluginManager *manager = PLUGIN_MANAGER (object);
	GList *l;

	l = manager->priv->plugins;
	while (l)
	{
		GList *l2 = l->next;
		plugin_manager_unload (manager, l->data);
		l = l2;
	}
	
	g_hash_table_destroy (manager->priv->files);

	g_free (manager->priv);

	if (plugin_manager_parent_class->destroy)
		(*plugin_manager_parent_class->destroy)(object);
}

static void
plugin_manager_class_init (GObjectClass *object_class)
{
	plugin_manager_parent_class = gtk_type_class (gtk_object_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = plugin_manager_destroy;
}

static void
plugin_manager_init (GObject *object)
{
	PluginManager *manager = PLUGIN_MANAGER (object);
	manager->priv = g_new0 (PluginManagerPrivate, 1);
	manager->priv->files = g_hash_table_new (g_str_hash, g_str_equal);
}

GType
plugin_manager_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (PluginManagerClass),
			NULL,
			NULL,
			(GClassInitFunc) plugin_manager_class_init,
			NULL,
			NULL,
			sizeof (PluginManager),
			0,	
			(GInstanceInitFunc) plugin_manager_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_OBJECT,
					       "PluginManager",
					       &info, 0);
	}

	return type;
}

PluginManager *
plugin_manager_new (void)
{
	return g_object_new (plugin_manager_get_type (), NULL);
}

