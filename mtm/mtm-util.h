#ifndef __MTM_UTIL_H__
#define __MTM_UTIL_H__

#include <stdio.h>
#include <mtm/mtm-env.h>

typedef enum
{
	MTM_OK,
	MTM_NO_ACCESS,
	MTM_GENERAL_ERROR  /* Say, a NULL pointer */
} MtmResult;

const gchar *mtm_get_prefix (void);

/* And now some random utility functions */

gchar *mtm_strip_ext (const gchar *filename, const gchar *suffix);

gboolean mtm_file_is_targz (const gchar *filename);
/* Will return temporary dirname. Suffix is, say, .tar.gz */
gchar *mtm_file_open_targz (MtmEnv *env, const gchar *filename, const gchar *suffix);
/* After closing, you need to free dirname */
void mtm_file_close_targz (const gchar *dirname);

gchar *mtm_file_untargz (const gchar *filename, const gchar *todir, const gchar *suffix);

/* Check for existence of dir. If not there, attempt to create it.
 * Returns OK on: Successful creation, or already exists
 * Returns NO_ACCESS on: Dir exists but no access,
 * or no such dir and no permission to create one,
 * or the file exists but is not a dir
 */
MtmResult mtm_check_dir (const gchar *dir);

/* Your standard allocate-a-string-consisting-of-next-line-of-text
 * convenience function. This oughta be in glib.
 */
gchar* mtm_readline (FILE *file);

/* Null-terminated path, returns allocated string */
gchar* mtm_find_file_in_path (gchar **path, const gchar *name, gboolean is_root);

MtmResult mtm_remove_directory (const gchar *dirname);
MtmResult mtm_move_file (const gchar *fromfile, const gchar *tofile);
MtmResult mtm_copy_file (const gchar *fromfile, const gchar *tofile);
MtmResult mtm_copy_directory (const gchar *fromdir, const gchar *todir);

#endif /* __MTM_UTIL_H__ */
