/*
 *
 * mtm-gui-handler :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm-gui-handler.h>
#include "internals.h"

static MtmHandlerClass *mtm_gui_handler_parent_class;

/* Destroy handler for MtmGuiHandler */
static void
mtm_gui_handler_destroy (GtkObject *object)
{
	if (GTK_OBJECT_CLASS (mtm_gui_handler_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (mtm_gui_handler_parent_class)->destroy)(object);
}

/* Overridden MtmHandler::register. Registers the GuiHandler with the handler
 * manager. */
static void
mtm_gui_handler_register (MtmHandler *handler)
{
	handler_manager_add (MTM_STATEFUL (handler)->env->handler_manager,
		MTM_GUI_HANDLER_ID, handler);
}

/* Class initialization function for MtmGuiHandler */
static void
mtm_gui_handler_class_init (GObjectClass *object_class)
{
	mtm_gui_handler_parent_class = gtk_type_class (mtm_handler_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = mtm_gui_handler_destroy;
	MTM_HANDLER_CLASS (object_class)->_register = mtm_gui_handler_register;
}

/* Object initialization function for MtmGuiHandler */
static void
mtm_gui_handler_init (GObject *object)
{
	MtmHandler *handler = MTM_HANDLER (object);
	handler->cleanup_handler = mtm_gui_handler_default_cleanup;
}

/**
 * mtm_gui_handler_get_type:
 * @void:
 *
 * Registers the #MtmGuiHandler class if necessary, and returns the type ID
 * associated to it.
 *
 * Return value: The type ID of the #MtmGuiHandler class.
 */
GType
mtm_gui_handler_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmGuiHandlerClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_gui_handler_class_init,
			NULL,
			NULL,
			sizeof (MtmGuiHandler),
			0,	
			(GInstanceInitFunc) mtm_gui_handler_init,
			NULL
		};

		type = g_type_register_static (MTM_HANDLER_TYPE,
					       "MtmGuiHandler",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_gui_handler_new:
 * @env: The environment object.
 *
 * Creates a new gui handler.
 *
 * Return value: A newly-created extension handler.
 */
MtmGuiHandler*
mtm_gui_handler_new (MtmEnv *env)
{
	MtmGuiHandler *handler;

	g_return_val_if_fail (MTM_IS_ENV (env), NULL);

	handler = g_object_new (mtm_gui_handler_get_type (), NULL);

	mtm_gui_handler_construct (handler, env);

	return handler;
}

/**
 * mtm_gui_handler_construct:
 * @handler: The uninitialized gui handler.
 * @env: The environment object.
 *
 * Initializes the provided handler.
 */
void
mtm_gui_handler_construct (MtmGuiHandler *handler, MtmEnv *env)
{
	g_return_if_fail (MTM_IS_GUI_HANDLER (handler));
	g_return_if_fail (MTM_IS_ENV (env));

	MTM_STATEFUL (handler)->env = env;
}

/**
 * mtm_env_get_gui_handler:
 * @env: The environment object.
 * @key: The key of the requested gui handler (corresponds to MtmExt::type).
 * 
 * Return value: The gui handler if found, NULL if otherwise.
 */
MtmGuiHandler*
mtm_env_get_gui_handler (MtmEnv *env, gchar *key)
{
	MtmHandler *handler;
	
	g_return_val_if_fail (MTM_IS_ENV (env), NULL);
	g_return_val_if_fail (key != NULL, NULL);

	handler = handler_manager_get (env->handler_manager,
			MTM_GUI_HANDLER_ID, key);
	if (handler)
		return MTM_GUI_HANDLER (handler);
	else
		return NULL;
}

/**
 * mtm_env_get_all_gui_handlers:
 * @env: The environment object.
 *
 * Return value: A pointer to the environment's list of known extension
 * handlers.
 **/
GList*
mtm_env_get_all_gui_handlers (MtmEnv *env)
{
	g_return_val_if_fail (MTM_IS_ENV (env), NULL);

	return handler_manager_get_all (env->handler_manager,
		MTM_GUI_HANDLER_ID);
}

/**
 * mtm_gui_handler_default_cleanup:
 * @handler: The handler.
 *
 * This is what the "cleanup_handler" member method of all gui handlers
 * initially points to.
 * Frees editcmd and default_suffix if they exist, then calls
 * mtm_handler_default_cleanup.
 *
 * Return value: 1 if successful, -1 otherwise.
 */
int
mtm_gui_handler_default_cleanup (MtmHandler *handler)
{
	MtmGuiHandler *gh;
	g_return_val_if_fail (MTM_IS_GUI_HANDLER (handler), -1);

	gh = MTM_GUI_HANDLER (handler);
	
	if (gh->name)
		g_free (gh->name);

	return mtm_handler_default_cleanup (handler);
}

