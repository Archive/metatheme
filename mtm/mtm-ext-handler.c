/*
 *
 * mtm-ext-handler :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm-ext-handler.h>
#include "internals.h"

/* FIXME: eep */
#include <gdk/gdkcolor.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

struct _MtmExtHandlerPrivate
{
	gchar **args;
};

static MtmHandlerClass *mtm_ext_handler_parent_class;

/* Destroy handler for MtmExtHandler */
static void
mtm_ext_handler_destroy (GtkObject *object)
{
	MtmExtHandler *handler = MTM_EXT_HANDLER (object);

	if (handler->subclass_args)
	{
		g_free (handler->subclass_args);
		handler->subclass_args = NULL;
	}
	
	if (handler->priv->args)
	{
		g_free (handler->priv->args);
		handler->priv->args = NULL;
	}

	if (handler->priv)
	{
		g_free (handler->priv);
		handler->priv = NULL;
	}
	
	if (GTK_OBJECT_CLASS (mtm_ext_handler_parent_class)->destroy)
		(*GTK_OBJECT_CLASS(mtm_ext_handler_parent_class)->destroy)(object);
}

/* Overridden MtmHandler::register. Registers the ExtHandler with the handler
 * manager. */
static void
mtm_ext_handler_register (MtmHandler *handler)
{
	handler_manager_add (MTM_STATEFUL (handler)->env->handler_manager,
		MTM_EXT_HANDLER_ID, handler);
}

/* Class initialization function for MtmExtHandler. */
static void
mtm_ext_handler_class_init (GObjectClass *object_class)
{
	mtm_ext_handler_parent_class = gtk_type_class (mtm_handler_get_type ());
	GTK_OBJECT_CLASS (object_class)->destroy = mtm_ext_handler_destroy;
	MTM_HANDLER_CLASS (object_class)->_register = mtm_ext_handler_register;
}

/* Object initialization function for MtmExtHandler. */
static void
mtm_ext_handler_init (GObject *object)
{
	MtmHandler *handler = MTM_HANDLER (object);
	handler->cleanup_handler = mtm_ext_handler_default_cleanup;
	MTM_EXT_HANDLER (handler)->save = mtm_ext_handler_default_save;
	MTM_EXT_HANDLER (handler)->priv = g_new0 (MtmExtHandlerPrivate, 1);
}

/**
 * mtm_ext_handler_get_type:
 * @void:
 *
 * Registers the #MtmExtHandler class if necessary, and returns the type ID
 * associated to it.
 * 
 * Return value: The type ID of the #MtmExtHandler class.
 */
GType
mtm_ext_handler_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (MtmExtHandlerClass),
			NULL,
			NULL,
			(GClassInitFunc) mtm_ext_handler_class_init,
			NULL,
			NULL,
			sizeof (MtmExtHandler),
			0,	
			(GInstanceInitFunc) mtm_ext_handler_init,
			NULL
		};

		type = g_type_register_static (MTM_HANDLER_TYPE,
					       "MtmExtHandler",
					       &info, 0);
	}

	return type;
}

/**
 * mtm_ext_handler_new:
 * @env: The environment object.
 *
 * Creates a new extension handler.
 *
 * Return value: A newly-created extension handler.
 */
MtmExtHandler *
mtm_ext_handler_new (MtmEnv *env)
{
	MtmExtHandler *handler;

	g_return_val_if_fail (env != NULL, NULL);
	
	handler = g_object_new (mtm_ext_handler_get_type (), NULL);
	
	mtm_ext_handler_construct (handler, env);
	
	return handler;
}

/**
 * mtm_ext_handler_construct:
 * @handler: The uninitialized extension handler.
 * @env: The environment object.
 * 
 * Intializes the provided handler.
 */
void
mtm_ext_handler_construct (MtmExtHandler *handler, MtmEnv *env)
{
	g_return_if_fail (handler != NULL);
	g_return_if_fail (env != NULL);
	
	MTM_STATEFUL (handler)->env = env;
}

/**
 * mtm_ext_handler_default_cleanup:
 * @handler: The handler.
 *
 * This is what the "cleanup_handler" member method of all extension handlers
 * initially points to.
 * Frees editcmd and default_suffix if they exist, then calls
 * mtm_handler_default_cleanup.
 *
 * Return value: 1 if successful, -1 otherwise.
 */
int
mtm_ext_handler_default_cleanup (MtmHandler *handler)
{
	MtmExtHandler *eh;
	g_return_val_if_fail (handler != NULL, -1);

	eh = MTM_EXT_HANDLER (handler);
	if (eh->editcmd)
		g_free (eh->editcmd);
	if (eh->default_suffix)
		g_free (eh->default_suffix);

	return mtm_handler_default_cleanup (handler);
}

/**
 * mtm_env_get_ext_handler:
 * @env: The environment object.
 * @key: The key of the requested extension handler (corresponds to
 * MtmExt::type).
 *
 * Return value: The extension handler if found, NULL otherwise.
 */
MtmExtHandler*
mtm_env_get_ext_handler (MtmEnv *env, gchar *key)
{
	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);
	
	return MTM_EXT_HANDLER (handler_manager_get (env->handler_manager,
		MTM_EXT_HANDLER_ID, key)); 
}

/**
 * mtm_env_get_all_ext_handlers:
 * @env: The environment object.
 *
 * Return value: A pointer to the environment's list of known extension
 * handlers.
 **/
GList*
mtm_env_get_all_ext_handlers (MtmEnv *env)
{
	g_return_val_if_fail (env != NULL, NULL);

	return handler_manager_get_all (env->handler_manager,
		MTM_EXT_HANDLER_ID);
}

/**
 * mtm_ext_handler_check_args:
 * @handler: The extension handler.
 * 
 * Fills in the appropriate subclass argument information for the given
 * extension handler. This must be called before attempting to access
 * MtmExtHandler::subclass_args, or MtmExtHandler::n_subclass_args.
 */
void
mtm_ext_handler_check_args (MtmExtHandler *handler)
{
	GArray *arr;
	GParamSpec *tmp;
	GType type;
	guint i, len = 0;
	
	g_return_if_fail (handler != NULL);
	
	if (!handler->priv->args)
		return;
	
	type = G_OBJECT_TYPE (handler);
	arr = g_array_new (FALSE, TRUE, sizeof (GParamSpec*));

	for (i = 0; handler->priv->args[i] != NULL; i++)
	{
		tmp = g_object_class_find_property (G_OBJECT_GET_CLASS (handler), handler->priv->args[i]);
		arr = g_array_append_val (arr, tmp);
		len++;
	}
	
	if (!len)
		return;

	handler->subclass_args = (GParamSpec**) arr->data;
	handler->n_subclass_args = len; 
	g_array_free (arr, FALSE);
}

/**
 * mtm_ext_handler_get_arg_name:
 * @handler: The extension handler.
 * @fullname: The fully qualified argument name.
 *
 * For a string of the form "Class::Arg", returns "Arg".
 * Used for subclassed extension handlers.
 *
 * Return value: A pointer to the beginning of the argument name.
 */
gchar *
mtm_ext_handler_get_arg_name (MtmExtHandler *handler, gchar *fullname)
{
	int len;
	int i;

	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (fullname != NULL, NULL);

	len = strlen (fullname);
	if (len <= 2)
		return NULL;

	for (i = len - 2; i >= 0; i--)
	{
		if (fullname[i] == ':')
			return fullname + i + 1;
	}

	return NULL;
}

/**
 * mtm_ext_handler_print_arg:
 * @handler: The extension handler.
 * @file: A pointer to a file stream.
 * @arg: A Gtk+ argument.
 *
 * Prints the given argument using the stream provided.
 * Used for subclassed extension handlers.
 */
void
mtm_ext_handler_print_arg (MtmExtHandler *handler, FILE *file,
					  GValue *arg)
{
	MtmExtHandlerClass *class;

	g_return_if_fail (handler != NULL); 
	g_return_if_fail (file != NULL); 
	g_return_if_fail (arg != NULL);

	class = MTM_EXT_HANDLER_CLASS (G_OBJECT_GET_CLASS (handler));

	fprintf (file, "type=\"%s\" value=\"", g_type_name (arg->g_type));
	/* FIXME: Add more types */
	if (arg->g_type == G_TYPE_STRING)
		fprintf (file, g_value_get_string (arg));
	else if (arg->g_type == G_TYPE_BOOLEAN)
		fprintf (file, "%i", g_value_get_boolean (arg));
	else if (arg->g_type == GDK_TYPE_COLOR)
	{
		GdkColor *color = g_value_get_boxed (arg);
		if (!color)
			fprintf (file, "#005060");
		else
			fprintf (file, "#%02x%02x%02x",
				color->red >> 8,
				color->green >> 8,
				color->blue >> 8);
	}
	else /* We assume it's an enum */
		fprintf (file, "%i", g_value_get_enum (arg));

	fprintf (file, "\"");
}

MtmResult
mtm_ext_handler_default_save (MtmExtHandler *handler, MtmExt *ext, const gchar *dirname)
{
	gchar *file;
	struct stat buf;
	gchar *base;
	
	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), MTM_GENERAL_ERROR);
	g_return_val_if_fail (MTM_IS_EXT (ext), MTM_GENERAL_ERROR);
	g_return_val_if_fail (ext->file != NULL, MTM_GENERAL_ERROR);
	g_return_val_if_fail (dirname != NULL, MTM_GENERAL_ERROR);

	if (mtm_check_dir (dirname) != MTM_OK)
		return MTM_NO_ACCESS;
	
	base = g_path_get_basename (ext->file);
	if (!strcmp (base, ext->file) && handler->find)
	{
		file = handler->find (handler, ext->file, FALSE);
		if (!file)
			file = handler->find (handler, ext->file, TRUE);
		if (!file)
		{
			g_free (base);
			return MTM_NO_ACCESS;
		}
	}
	else
		file = g_strdup (ext->file);
	
	if (stat (ext->file, &buf) != 0)
	{
		g_free (base);
		g_free (file);
		return MTM_NO_ACCESS;
	}
	
	if (mtm_file_is_targz (file))
	{
		gchar *tmp = mtm_file_untargz (file, dirname, ".tar.gz");
		g_free (tmp);
	}
	else 
	{
		gchar *tmp = g_strconcat (dirname, "/", base, NULL);

		if S_ISDIR (buf.st_mode)
			mtm_copy_directory (ext->file, tmp);
		else
			mtm_copy_file (ext->file, tmp);

		g_free (tmp);
	}

	g_free (base);
	g_free (file);

	return MTM_OK;
}

void
mtm_ext_handler_register_args (MtmExtHandler *handler, gchar **args)
{
	g_return_if_fail (MTM_IS_EXT_HANDLER (handler));
	g_return_if_fail (args != NULL);

	handler->priv->args = g_strdupv (args);
}
