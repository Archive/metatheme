#ifndef __PLUGIN_MANAGER_H__
#define __PLUGIN_MANAGER_H__

#define PLUGIN_MANAGER_TYPE		(plugin_manager_get_type ())
#define PLUGIN_MANAGER(o)		(GTK_CHECK_CAST ((o), PLUGIN_MANAGER_TYPE, PluginManager))
#define PLUGIN_MANAGER_CLASS(k)		(GTK_CHECK_CLASS_CAST((k), PLUGIN_MANAGER_TYPE, PluginManagerClass))
#define IS_PLUGIN_MANAGER(o)		(GTK_CHECK_TYPE ((o), PLUGIN_MANAGER_TYPE))
#define IS_PLUGIN_MANAGER_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), PLUGIN_MANAGER_TYPE))

typedef struct _PluginManager PluginManager;
typedef struct _PluginManagerPrivate PluginManagerPrivate;

typedef struct _PluginManagerClass PluginManagerClass;

#include <gtk/gtkobject.h>
#include <mtm/mtm-plugin.h>

struct _PluginManager
{
	GtkObject parent;
	
	PluginManagerPrivate *priv;
};

struct _PluginManagerClass
{
	GtkObjectClass parent_class;
};

GType plugin_manager_get_type (void);
PluginManager* plugin_manager_new (void);

gboolean plugin_manager_load (PluginManager *manager, MtmPlugin *plugin);
gboolean plugin_manager_load_file (PluginManager *manager, MtmEnv *env,
							const gchar *filename);
gboolean plugin_manager_load_dir (PluginManager *manager, MtmEnv *env,
						    const gchar *dirname);

/* Todo: add accessor methods if necessary */

#endif /* __PLUGIN_MANAGER_H__ */
