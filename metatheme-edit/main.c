/*
 *
 * main.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include "editor-shell.h"
#include <gnome.h>
#include <mtm/mtm.h>
#include "save.h"

static gchar*
compute_title (gchar *filename)
{
	return g_strdup_printf (_("%s - Metatheme Editor"),
				filename ? filename : _("Unnamed"));
}

static void
save_clicked_cb (GtkWidget *widget, MtmTheme *theme)
{
	save_theme (theme);
}

static void
close_clicked_cb (GtkWidget *widget, MtmTheme *theme)
{
	gtk_main_quit ();
}

int
main (int argc, char **argv)
{
	GtkWidget *shell;
	GtkWidget *win;
	MtmEnv *env;
	MtmTheme *theme = NULL;
	gchar *title;
	GtkWidget *bbox, *vbox;
	GtkWidget *save, *close;

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	gnome_program_init ("metatheme-edit", VERSION, LIBGNOMEUI_MODULE,
			    argc, argv, NULL);

	env = mtm_env_new ();
	
	if (argc > 1)
		theme = mtm_theme_new_from_file (env, argv[1]);
	
	if (!theme)
		theme = mtm_theme_new_from_defaults (env);
	
	title = compute_title (argc > 1 ? argv[1] : NULL);
	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (win), title);
	g_free (title);
	
	shell = editor_shell_new_from_theme (env, theme, win);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (win), vbox);

	gtk_box_pack_start (GTK_BOX (vbox), shell,
			    TRUE, TRUE, 0);
	bbox =	gtk_hbox_new (FALSE, 8);
	gtk_box_pack_start (GTK_BOX (vbox), bbox,
			    FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (bbox), 4);
	
	close = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
	gtk_box_pack_end (GTK_BOX (bbox), close, FALSE, FALSE, 0);
	save = gtk_button_new_from_stock (GTK_STOCK_SAVE);
	gtk_box_pack_end (GTK_BOX (bbox), save, FALSE, FALSE, 0);
	
	g_signal_connect (G_OBJECT (win), "destroy", (GCallback) gtk_main_quit, NULL);
	g_signal_connect (G_OBJECT (save), "clicked", (GCallback) save_clicked_cb, theme);
	g_signal_connect (G_OBJECT (close), "clicked", (GCallback) close_clicked_cb, theme);

	gtk_widget_show_all (win);

	gtk_main ();

	gtk_object_destroy (GTK_OBJECT (env));
	return 0;
}
