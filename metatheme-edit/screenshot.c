#include "screenshot.h"
#include "io-png.h"

#define SCALE_WIDTH 640
#define SCALE_HEIGHT 480

#include <gnome.h>

static GdkPixbuf*
scale_aspect (GdkPixbuf *buf)
{
	int width, height;
	int rwidth, rheight;

	g_return_val_if_fail (buf != NULL, NULL);

	rwidth = gdk_pixbuf_get_width (buf);
	rheight = gdk_pixbuf_get_height (buf);

	if (rwidth < SCALE_WIDTH && rheight < SCALE_HEIGHT)
		return gdk_pixbuf_copy (buf);
	else if (rwidth > rheight)
	{
		width = SCALE_WIDTH;
		height = SCALE_WIDTH * (((double)rheight)/(double)rwidth);
	}
	else
	{
		height = SCALE_HEIGHT;
		width = SCALE_HEIGHT * (((double)rwidth)/(double)rheight);
	}
	return gdk_pixbuf_scale_simple (buf, width, height, GDK_INTERP_BILINEAR);
}
		
gboolean
screenshot_grab (const gchar *filename)
{
	GdkPixbuf *buf, *scaled;
	GdkWindow *root;
	int width, height;
	gboolean result;
	
	g_return_val_if_fail (filename != NULL, FALSE);

	/* Say cheese! */
	root = gdk_get_default_root_window ();
	gdk_drawable_get_size (GDK_DRAWABLE (root), &width, &height);
			
	buf = gdk_pixbuf_get_from_drawable (NULL, root, NULL,
		       			    0, 0, 0, 0, width, height);
	scaled = scale_aspect (buf);
	g_object_unref (G_OBJECT (buf));

	result = save_png (scaled, filename);
	g_object_unref (G_OBJECT (scaled));
	
	return result;
}
