/*
 *
 * editor-page.c:
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */
#include <config.h>
#include <mtm/mtm-config-gui.h>
#include <mtm/mtm-gui-handler.h>
#include <mtm/mtm-ext.h>
#include <glade/glade-xml.h>
#include <gtk/gtk.h>
#include <gnome.h>
#include "editor-page.h"

#define GLADE_FILENAME "metatheme-edit.glade"

static GtkVBoxClass *editor_page_parent_class;

struct _EditorPagePrivate
{
	GladeXML *xml;
	MtmConfigGui *gui;
	GtkWidget *config_area_box;
	GtkWidget *entry;
	MtmExt *ext;
};

static void editor_page_ext_enabled (GtkToggleButton *button, EditorPage *page);
static void editor_page_set_file_list (MtmConfigGui *gui, const gchar **file_list, EditorPage *page);
static void file_changed_cb (GtkWidget *item, EditorPage *page);
static void selected_cb (GtkWidget *item, EditorPage *page);

/* Destroy handler for EditorPage */
static void
editor_page_destroy (GtkObject *object)
{
	EditorPage *page = EDITOR_PAGE (object);
	
	if (page->priv->gui)
		gtk_object_destroy (GTK_OBJECT (page->priv->gui));
	
	g_free (page->priv);

	if (GTK_OBJECT_CLASS (editor_page_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (editor_page_parent_class)->destroy)(object);
}

/* Class initialization function for EditorPage */
static void
editor_page_class_init (GtkObjectClass *object_class)
{
	editor_page_parent_class = gtk_type_class (gtk_vbox_get_type ());

	object_class->destroy = editor_page_destroy;
}

static void
editor_page_init (GtkObject *object)
{
	EditorPage *page = EDITOR_PAGE (object);
	GtkWidget *w;
	gchar *pixfile;
	
	page->priv = g_new0 (EditorPagePrivate, 1);
	/* Is having a GladeXML copy for each pane costly? I dunno. */
	/* Next line useful for quick changes in UI */
	if (g_file_test (GLADE_FILENAME, G_FILE_TEST_EXISTS))
		page->priv->xml = glade_xml_new (GLADE_FILENAME, "vbox11", NULL);
	if (!page->priv->xml)
		page->priv->xml = glade_xml_new (SHAREDIR GLADE_FILENAME, "vbox11", NULL);
	
	g_return_if_fail (page->priv->xml != NULL);

	page->priv->config_area_box = glade_xml_get_widget (page->priv->xml, "config_area");
	
	w = glade_xml_get_widget (page->priv->xml, "vbox11");
	gtk_box_pack_start (GTK_BOX (page), w, TRUE, TRUE, 0);

	w = glade_xml_get_widget (page->priv->xml, "enabled_checkbutton");
	g_signal_connect (G_OBJECT (w), "toggled", (GCallback) editor_page_ext_enabled, page);

	pixfile = gnome_program_locate_file (gnome_program_get (),
					     GNOME_FILE_DOMAIN_PIXMAP,
					     "32_butteflylogo.png", TRUE,
					     NULL);
	if (pixfile)
	{
		w = glade_xml_get_widget (page->priv->xml, "left_pixmap_holder");
		gtk_box_pack_start (GTK_BOX (w),
				    gtk_image_new_from_file (pixfile),
				    TRUE, TRUE, 0);
		g_free (pixfile);
	}
	
	pixfile = gnome_program_locate_file (gnome_program_get (),
					     GNOME_FILE_DOMAIN_PIXMAP,
					     "settings.png", TRUE,
					     NULL);
	if (pixfile)
	{
		w = glade_xml_get_widget (page->priv->xml, "right_pixmap_holder");
		gtk_box_pack_start (GTK_BOX (w),
				    gtk_image_new_from_file (pixfile),
				    TRUE, TRUE, 0);
		g_free (pixfile);
	}
}


GType
editor_page_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (EditorPageClass),
			NULL,
			NULL,
			(GClassInitFunc) editor_page_class_init,
			NULL,
			NULL,
			sizeof (MtmExtHandler),
			0,	
			(GInstanceInitFunc) editor_page_init,
			NULL
		};

		type = g_type_register_static (GTK_TYPE_VBOX,
					       "EditorPage",
					       &info, 0);
	}

	return type;
}

GtkWidget*
editor_page_new (void)
{
	EditorPage *page;

	page = g_object_new (editor_page_get_type (), NULL);
	return GTK_WIDGET (page);
}

static void
editor_page_add_no_info (EditorPage *page)
{
	GtkWidget *label;
	
	g_return_if_fail (page != NULL);
	label = gtk_label_new (_("[No information available]"));
	gtk_box_pack_start (GTK_BOX (page->priv->config_area_box),
			    label, TRUE, TRUE, 5);
}

GtkWidget*
editor_page_new_from_type (MtmEnv *env, gchar *type)
{
	EditorPage *page;
	MtmGuiHandler *handler;
	gchar *theme_label;
	gchar *history;
	GtkWidget *entry;

	g_return_val_if_fail (MTM_IS_ENV (env), NULL);
	g_return_val_if_fail (type != NULL, NULL);

	page = EDITOR_PAGE (editor_page_new ());
	page->type = type;
	
	handler = mtm_env_get_gui_handler (env, type);
	theme_label = g_strdup_printf (_("%s theme: "), (handler && handler->name) ? handler->name : type);
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (page->priv->xml, "theme_label")), theme_label);
	g_free (theme_label);

	history = g_strdup_printf ("mtm_edit_%s", type);
	entry = gnome_file_entry_new (history, NULL);
	g_free (history);
	page->priv->entry = entry;
	gtk_box_pack_start (GTK_BOX (glade_xml_get_widget (page->priv->xml, "file_list_hbox")), entry, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (entry), "activate", (GCallback) file_changed_cb, page);
	g_signal_connect (G_OBJECT (GTK_COMBO (gnome_file_entry_gnome_entry (GNOME_FILE_ENTRY (entry)))->list), "selection_changed", (GCallback) selected_cb, page);

	if (!handler)
	{
		editor_page_add_no_info (page);
		return GTK_WIDGET (page);
	}

	page->priv->gui = handler->create_gui (handler);

	if (MTM_HANDLER (handler)->desc)
		editor_page_set_desc_label (page, MTM_HANDLER (handler)->desc);
		
	if (!page->priv->gui)
	{
		editor_page_add_no_info (page);
		return GTK_WIDGET (page);
	}
	
	if (page->priv->gui->config_area)
	{
		gtk_box_pack_start (GTK_BOX (page->priv->config_area_box),
			page->priv->gui->config_area, TRUE, TRUE, 0);
		gtk_widget_show (page->priv->gui->config_area);
	}
	else
	{
		editor_page_add_no_info (page);
	}

	g_signal_connect (G_OBJECT (page->priv->gui), "set_file_list", (GCallback) editor_page_set_file_list, page);
	/* Of course, by now the initial one is already set */
	editor_page_set_file_list (page->priv->gui, (const gchar**) page->priv->gui->file_list, page); 
	mtm_config_gui_set_file_entry (page->priv->gui, GNOME_FILE_ENTRY (page->priv->entry));

	return GTK_WIDGET (page);
}

GtkWidget*
editor_page_new_from_ext (MtmExt *ext)
{
	EditorPage *page;

	g_return_val_if_fail (MTM_IS_EXT (ext), NULL);
	
	page = EDITOR_PAGE (editor_page_new_from_type (MTM_STATEFUL (ext)->env, ext->type));
	
	/* Set it before the gui hears it */
	if (ext->file)
	{
		gchar *base = g_path_get_basename (ext->file);
		gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (page->priv->entry))), base);
		g_free (base);
	}

	if (page->priv->gui)
		mtm_config_gui_set_ext (page->priv->gui, ext);
	else
		page->priv->ext = ext;

	return GTK_WIDGET (page);
}

static void
selected_cb (GtkWidget *item, EditorPage *page)
{
	g_return_if_fail (IS_EDITOR_PAGE (page));
	
	if (!GNOME_FILE_ENTRY (page->priv->entry)->fsw)
	{
	}
}

static void
file_changed_cb (GtkWidget *item, EditorPage *page)
{
	MtmExt *ext;
	gchar *file;
	
	g_return_if_fail (IS_EDITOR_PAGE (page));
	if (page->priv->gui)
		ext = page->priv->gui->ext;
	else
		ext = page->priv->ext;
	/* No need setting the file */
	if (!ext)
		return;
	
	file = gnome_file_entry_get_full_path (GNOME_FILE_ENTRY (page->priv->entry), FALSE);
	if (!file)
		return;
	if (!g_file_test (file, G_FILE_TEST_IS_DIR))
	{
		gchar *base = g_path_get_basename (file);
		gchar *tmp = ext->handler->find (ext->handler, base, FALSE);
		if (!tmp)
			tmp = ext->handler->find (ext->handler, base, TRUE);
		if (tmp)
			file = tmp;
		g_free (base);
	}

	mtm_ext_set_file (ext, file);
	g_free (file);
}

static void
editor_page_set_file_list (MtmConfigGui *gui, const gchar **file_list, EditorPage *page)
{
	g_return_if_fail (IS_EDITOR_PAGE (page));
	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));

	return;
	if (file_list)
	{
		GtkWidget *menu, *option;
		
		option = glade_xml_get_widget (page->priv->xml,
					       "file_list_entry");
		//menu = make_file_list_menu (page, file_list);
		gtk_option_menu_set_menu (GTK_OPTION_MENU (option), menu);
	}
}

static void
editor_page_ext_enabled (GtkToggleButton *button, EditorPage *page)
{
	gchar *widget_names[] = { "theme_label", "file_list_hbox", NULL};
	GtkWidget *w;
	gboolean active;
	int i;
	
	g_return_if_fail (GTK_IS_TOGGLE_BUTTON (button));
	g_return_if_fail (IS_EDITOR_PAGE (page));
	
	active = gtk_toggle_button_get_active (button);
	
	for (i = 0; widget_names[i] != NULL; i++)
	{
		w = glade_xml_get_widget (page->priv->xml, widget_names[i]);
		gtk_widget_set_sensitive (w, active);
	}
	
	gtk_widget_set_sensitive (GTK_WIDGET (page->priv->config_area_box), active);

	if (page->priv->gui && page->priv->gui->ext)
		mtm_ext_set_activate (page->priv->gui->ext, active);
	else if (page->priv->ext)
		mtm_ext_set_activate (page->priv->ext, active);
}

void
editor_page_set_desc_label (EditorPage *page, gchar *text)
{
	g_return_if_fail (IS_EDITOR_PAGE (page));
	g_return_if_fail (text != NULL);
	
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (page->priv->xml, "long_desc_label")), text);
}

GtkWidget*
editor_page_get_config_area (EditorPage *page)
{
	g_return_val_if_fail (IS_EDITOR_PAGE (page), NULL);

	return page->priv->config_area_box;
}

void
editor_page_set_is_ext (EditorPage *page, gboolean is_ext)
{
	GtkWidget *w;
	
	g_return_if_fail (IS_EDITOR_PAGE (page));
	
	w = glade_xml_get_widget (page->priv->xml, "ext_hbox");
	
	/* This is not a reversible operation.*/
	if (!is_ext)
		gtk_widget_destroy (w);
}
