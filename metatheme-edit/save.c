#include "save.h"
#include <gnome.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

static void
actual_save_theme (MtmTheme *theme)
{
	MtmResult res;
	
	g_return_if_fail (MTM_IS_THEME (theme));
	g_return_if_fail (theme->filename != NULL);

	res = mtm_theme_save (theme);
	if (res != MTM_OK)
	{
		GtkWidget *dlg = gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Unable to save the metatheme. Please check to make sure you have write permissions to the directory specified."));
		gtk_widget_show (dlg);
		gtk_dialog_run (GTK_DIALOG (dlg));
		mtm_theme_set_filename (theme, NULL);
	}
}

static gboolean
is_writable (const gchar* filename)
{
	struct stat stbuf;
	
	g_return_val_if_fail (filename != NULL, FALSE);

	if (stat (filename, &stbuf) != 0)
	{
		gchar *name = g_path_get_dirname (filename);
		gboolean ret = is_writable (name);
		g_free (name);
		return ret;
	}
	else if (!((getuid () == stbuf.st_uid && (stbuf.st_mode & S_IWUSR))
		|| (getgid () == stbuf.st_gid && (stbuf.st_mode & S_IWGRP))
		|| (stbuf.st_mode & S_IWOTH)))
		return FALSE;
	else
		return TRUE;
}

void
save_theme (MtmTheme *theme)
{
	g_return_if_fail (MTM_IS_THEME (theme));

	if (!(theme->filename && is_writable (theme->filename)))
		save_theme_as (theme);
	else 
		actual_save_theme (theme);
}

static void
fsel_cancel_cb (GtkWidget *widget, gboolean* cancelled)
{
	g_return_if_fail (cancelled != NULL);
	
	*cancelled = TRUE;
	gtk_main_quit ();
}

static void
fsel_ok_cb (GtkWidget *button, MtmTheme *theme)
{
	GtkFileSelection *fsel;
	
	g_return_if_fail (MTM_IS_THEME (theme));

	fsel = g_object_get_data (G_OBJECT (theme), "fsel");
	mtm_theme_set_filename (theme, gtk_file_selection_get_filename (fsel));
	gtk_main_quit ();
}

void
save_theme_as (MtmTheme *theme)
{
	GtkWidget *fsel;
	gboolean cancelled = FALSE;

	g_return_if_fail (MTM_IS_THEME (theme));

	fsel = gtk_file_selection_new (NULL);
	g_signal_connect (G_OBJECT (fsel), "delete_event",
			  (GCallback) fsel_cancel_cb, &cancelled);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (fsel)->cancel_button),
			  "clicked", (GCallback) fsel_cancel_cb, &cancelled);
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (fsel)->ok_button),
			  "clicked", (GCallback) fsel_ok_cb, theme);
	g_object_set_data (G_OBJECT (theme), "fsel", fsel);
		
	gtk_widget_show (fsel);
	
	gtk_main ();
	
	if (!cancelled)
		actual_save_theme (theme);
	gtk_widget_destroy (GTK_WIDGET (fsel));
}

static void
mod_reply_cb (GtkWidget *dlg, gint reply, gboolean *do_save)
{
	if (reply == GTK_RESPONSE_YES)
		*do_save = TRUE;
	else
		*do_save = FALSE;
}

void
run_modified_dlg (MtmTheme *theme)
{
	GtkWidget *dlg;
	gboolean do_save;

	g_return_if_fail (MTM_IS_THEME (theme));
	dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
				      GTK_MESSAGE_QUESTION,
				      GTK_BUTTONS_YES_NO,
				     _("Theme unsaved. Save before exiting?"));
	g_signal_connect (G_OBJECT (dlg), "response",
			  (GCallback) mod_reply_cb, &do_save);
	gtk_widget_show (dlg);
	gtk_dialog_run (GTK_DIALOG (dlg));
	
	if (do_save)
		save_theme (theme);
}
