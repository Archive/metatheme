#ifndef __EDITOR_SHELL_H__
#define __EDITOR_SHELL_H__

#include <gtk/gtkwidget.h>
#include <mtm/mtm-theme.h>

GtkWidget* editor_shell_new (MtmEnv *env);
GtkWidget* editor_shell_new_from_theme (MtmEnv *env, MtmTheme *theme, GtkWidget *parent_window);

#endif /* __EDITOR_SHELL_H__ */
