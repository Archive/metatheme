#ifndef __EDITOR_MAIN_PAGE_H__
#define __EDITOR_MAIN_PAGE_H__

#define EDITOR_MAIN_PAGE_TYPE		(editor_main_page_get_type ())
#define EDITOR_MAIN_PAGE(o)		(GTK_CHECK_CAST ((o), EDITOR_MAIN_PAGE_TYPE, EditorMainPage))
#define EDITOR_MAIN_PAGE_CLASS(k)		(GTK_CHECK_CLASS_CAST ((k), EDITOR_MAIN_PAGE_TYPE, EditorMainPageClass))
#define IS_EDITOR_MAIN_PAGE(o)		(GTK_CHECK_TYPE ((o), EDITOR_MAIN_PAGE_TYPE))
#define IS_EDITOR_MAIN_PAGE_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), EDITOR_MAIN_PAGE_TYPE))

typedef struct _EditorMainPage EditorMainPage;
typedef struct _EditorMainPagePrivate EditorMainPagePrivate;

typedef struct _EditorMainPageClass EditorMainPageClass;

#include <mtm/mtm-theme.h>
#include "editor-page.h"

struct _EditorMainPage
{
	EditorPage parent_class;
	
	EditorMainPagePrivate *priv;
};

struct _EditorMainPageClass
{
	EditorPageClass parent_class;
};

GType editor_main_page_get_type (void);

GtkWidget* editor_main_page_new (MtmTheme *theme, GtkWidget *parent_window);

#endif /* __EDITOR_MAIN_PAGE_H__ */
