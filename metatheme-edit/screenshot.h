#ifndef __SCREENSHOT_H__
#define __SCREENSHOT_H__

#include <glib.h>

gboolean screenshot_grab (const gchar *filename);

#endif /* __SCREENSHOT_H__ */
