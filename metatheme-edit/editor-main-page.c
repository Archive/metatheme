/*
 *
 * editor-main-page.c:
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include "editor-main-page.h"
#include <libgnome/libgnome.h>
#include <libgnomeui/gnome-pixmap-entry.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <glade/glade.h>
#include <gtk/gtkentry.h>
#include "screenshot.h"

#define GLADE_FILENAME "metatheme-edit.glade"

static EditorMainPageClass *editor_main_page_parent_class;

struct _EditorMainPagePrivate
{
	GladeXML *xml;
	MtmTheme *theme;
	GtkWidget *parent_window;
};

/* Destroy handler for EditorMainPage */
static void
editor_main_page_destroy (GtkObject *object)
{
	EditorMainPage *page = EDITOR_MAIN_PAGE (object);

	g_free (page->priv);

	if (GTK_OBJECT_CLASS (editor_main_page_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (editor_main_page_parent_class)->destroy)(object);
}

/* Class initialization function for EditorMainPage */
static void
editor_main_page_class_init (GtkObjectClass *object_class)
{
	editor_main_page_parent_class = gtk_type_class (editor_page_get_type ());

	object_class->destroy = editor_main_page_destroy;
}

static void
preview_changed (GtkWidget *entry, MtmTheme *theme)
{
	mtm_theme_set_preview (theme, gtk_entry_get_text (GTK_ENTRY (entry)));
}

static void
name_changed (GtkWidget *entry, MtmTheme *theme)
{
	mtm_theme_set_name (theme, gtk_entry_get_text (GTK_ENTRY (entry)));
}

static void
editor_main_page_init (GtkObject *object)
{
	EditorMainPage *page = EDITOR_MAIN_PAGE (object);
	
	page->priv = g_new0 (EditorMainPagePrivate, 1);
}

static void
grab_preview_cb (GtkWidget *widget, EditorMainPage *page)
{
	GtkWidget *w;
	gchar *filename = g_strconcat (mtm_env_get_tmpdir (MTM_STATEFUL (page->priv->theme)->env), "/theme-preview.png", NULL);
	
	gtk_widget_hide (page->priv->parent_window);
	while (gtk_events_pending ())
		gtk_main_iteration ();
	
	screenshot_grab (filename);

	gtk_widget_show (page->priv->parent_window);

	mtm_theme_set_preview (page->priv->theme, filename);

	w = glade_xml_get_widget (page->priv->xml, "preview_entry");
	gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (w))), filename);

	g_free (filename);
}

GType
editor_main_page_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (EditorMainPageClass),
			NULL,
			NULL,
			(GClassInitFunc) editor_main_page_class_init,
			NULL,
			NULL,
			sizeof (MtmExtHandler),
			0,	
			(GInstanceInitFunc) editor_main_page_init,
			NULL
		};

		type = g_type_register_static (EDITOR_PAGE_TYPE,
					       "EditorMainPage",
					       &info, 0);
	}

	return type;
}

GtkWidget*
editor_main_page_new (MtmTheme *theme, GtkWidget *parent_window)
{
	EditorMainPage *page;
	GtkWidget *w, *vbox;

	page = g_object_new (editor_main_page_get_type (), NULL);
	editor_page_set_desc_label (EDITOR_PAGE (page), _("Select an existing metatheme for your desktop, or create your own. To edit a theme, change the settings in the following tabs, and press \"Save\"."));
	editor_page_set_is_ext (EDITOR_PAGE (page), FALSE);
	
	page->priv->xml = glade_xml_new (SHAREDIR "metatheme-edit.glade",
					 "main_page", NULL);
	w = glade_xml_get_widget (page->priv->xml, "main_page");

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (editor_page_get_config_area (EDITOR_PAGE (page))),
			    w, TRUE, TRUE, 8);
	//gtk_box_pack_start (GTK_BOX (vbox), w, TRUE, TRUE, 8);
	
	w = glade_xml_get_widget (page->priv->xml, "preview_entry");
	/* Buggy */
/*	gnome_pixmap_entry_set_preview (GNOME_PIXMAP_ENTRY (w), FALSE);
	gnome_pixmap_entry_set_preview (GNOME_PIXMAP_ENTRY (w), TRUE);*/

	if (theme->preview)
		gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (w))), theme->preview);

	g_signal_connect (G_OBJECT (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (w))),
			  "changed", (GCallback) preview_changed, theme);

	w = glade_xml_get_widget (page->priv->xml, "name_entry");
	if (theme->name)
		gtk_entry_set_text (GTK_ENTRY (w), theme->name);

	g_signal_connect (G_OBJECT (w), "changed", (GCallback) name_changed, theme);

	w = glade_xml_get_widget (page->priv->xml, "preview_grab_button");
	g_signal_connect (G_OBJECT (w), "clicked", (GCallback) grab_preview_cb, page);

	page->priv->theme = theme;
	page->priv->parent_window = parent_window;
	return GTK_WIDGET (page);
}
