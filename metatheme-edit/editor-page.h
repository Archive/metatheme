#ifndef __EDITOR_PAGE_H__
#define __EDITOR_PAGE_H__

#define EDITOR_PAGE_TYPE		(editor_page_get_type ())
#define EDITOR_PAGE(o)		(GTK_CHECK_CAST ((o), EDITOR_PAGE_TYPE, EditorPage))
#define EDITOR_PAGE_CLASS(k)		(GTK_CHECK_CLASS_CAST ((k), EDITOR_PAGE_TYPE, EditorPageClass))
#define IS_EDITOR_PAGE(o)		(GTK_CHECK_TYPE ((o), EDITOR_PAGE_TYPE))
#define IS_EDITOR_PAGE_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), EDITOR_PAGE_TYPE))

typedef struct _EditorPage EditorPage;
typedef struct _EditorPagePrivate EditorPagePrivate;

typedef struct _EditorPageClass EditorPageClass;

#include <mtm/mtm-ext.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkhbox.h>

struct _EditorPage
{
	GtkVBox parent;
	gchar *type;	
	EditorPagePrivate *priv;
};

struct _EditorPageClass
{
	GtkVBoxClass parent_class;
};

GType editor_page_get_type (void);

GtkWidget* editor_page_new (void);
GtkWidget* editor_page_new_from_type (MtmEnv *env, gchar *type);
GtkWidget* editor_page_new_from_ext (MtmExt *ext);

void editor_page_set_desc_label (EditorPage *page, gchar *text);
GtkWidget* editor_page_get_config_area (EditorPage *page);
void editor_page_set_is_ext (EditorPage *page, gboolean is_ext);

#endif /* __EDITOR_PAGE_H__ */
