#ifndef __SAVE_H__
#define __SAVE_H__

#include <mtm/mtm-theme.h>

void save_theme (MtmTheme *theme);
void save_theme_as (MtmTheme *theme);
void run_modified_dlg (MtmTheme *theme);

#endif /* __SAVE_H__ */
