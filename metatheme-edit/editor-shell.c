/*
 *
 * editor-shell.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include "editor-shell.h"
#include "editor-page.h"
#include "editor-main-page.h"
#include <mtm/mtm-gui-handler.h>
#include <gtk/gtk.h>
#include <string.h>
#include <libgnome/libgnome.h>

static gint
key_compare_func (gconstpointer a_ptr, gconstpointer b_ptr)
{
	const MtmExt *a = a_ptr;
	const MtmExt *b = b_ptr;
	
	return strcmp (a->type, b->type);
}

GtkWidget*
editor_shell_new (MtmEnv *env)
{
	GtkWidget *notebook;
	
	notebook = gtk_notebook_new ();
	return notebook;
}

GtkWidget*
editor_shell_new_from_theme (MtmEnv *env, MtmTheme *theme, GtkWidget *parent_window)
{
	GtkWidget *shell;
	GtkWidget *main_page;
	GList *l;

	g_return_val_if_fail (MTM_IS_ENV (env), NULL);
	g_return_val_if_fail (MTM_IS_THEME (theme), NULL);
	
	shell = editor_shell_new (env);
	g_object_set_data (G_OBJECT (shell), "theme", theme);

	l = g_list_copy (mtm_theme_get_exts (theme));
	l = g_list_sort (l, key_compare_func);

	main_page = editor_main_page_new (theme, parent_window);
	gtk_notebook_append_page (GTK_NOTEBOOK (shell), main_page,
				  gtk_label_new (_("Metatheme")));

	for (; l != NULL; l = l->next)
	{
		MtmExt *ext = MTM_EXT (l->data);
		GtkWidget *page = editor_page_new_from_ext (ext);
		MtmGuiHandler *handler = mtm_env_get_gui_handler (env, ext->type);
		gtk_notebook_append_page (GTK_NOTEBOOK (shell),
					  page,
					  gtk_label_new ((handler && handler->name) ? handler->name : ext->type));
	}

	g_list_free (l);

	return shell;
}

