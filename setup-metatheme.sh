#!/bin/sh

which metatheme-install || no_mtm="no"
if test "$no_mtm" = no; then
	echo "You don't seem to have run 'make install'. Please check the README for installation instructions."
	exit 1
fi

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

(test -d $srcdir/examples) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome directory"
    exit 1
}

metatheme-install ./examples/*.mtm && {
    echo
    echo "*******************************************************"
    echo "The example metathemes have been succesfully installed."
    echo "*******************************************************"
}
