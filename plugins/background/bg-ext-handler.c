/*
 *
 * bg-ext-handler.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <libgnome/libgnome.h>
#include "bg-ext-handler.h"
#include <gdk/gdkcolor.h>
#include <string.h>

static GObjectClass *bg_ext_handler_parent_class;

struct _BgExtHandlerPrivate
{
	GList *configs;
	MtmExt *ext;
	GHashTable *descs;
	GList *strs;
};

enum
{
	ARG_0,
	ARG_COLOR1,
	ARG_COLOR2,
	ARG_COLOR_GRADIENT,
	ARG_GRADIENT_TYPE,
	ARG_WALLPAPER_LAYOUT,
	ARG_EXT_CONTEXT
};

static GEnumValue _bg_wp_type_values[] = {
	{ WPTYPE_TILED, "WPTYPE_TILED", "Tiled" },
	{ WPTYPE_CENTERED, "WPTYPE_CENTERED", "Centered" },
	{ WPTYPE_SCALED, "WPTYPE_SCALED", "Scaled - keep aspect"} ,
	{ WPTYPE_STRETCHED, "WPTYPE_STRETCHED", "Scaled" },
	{ 0, NULL, NULL }
};

static GEnumValue _bg_grad_type_values[] = {
	{ ORIENTATION_SOLID, "ORIENTATION_SOLID", "Solid" },
	{ ORIENTATION_HORIZ, "ORIENTATION_HORIZ", "Horizontal" },
	{ ORIENTATION_VERT, "ORIENTATION_VERT", "Vertical" },
	{ 0, NULL, NULL }
};

GType bg_wp_get_type (void);
GType bg_grad_get_type (void);

static gchar* bg_ext_handler_describe_arg (MtmExtHandler *handler, gchar *arg);

GType
bg_wp_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		type = g_enum_register_static ("BgWallpaperType",
			_bg_wp_type_values);
	}

	return type;
}

GType
bg_grad_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		type = g_enum_register_static ("BgGradientType",
			_bg_grad_type_values);
	}

	return type;
}

static void bg_ext_handler_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec);
static void bg_ext_handler_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec);

static void
bg_ext_handler_dispose (GObject *object)
{
	BgExtHandler *handler = BG_EXT_HANDLER (object);

	if (handler->priv->configs)
	{
		g_list_foreach (handler->priv->configs, (GFunc) g_object_unref, NULL);
		g_list_free (handler->priv->configs);
		handler->priv->configs = NULL;
	}
	
	if (handler->priv->descs)
	{
		g_hash_table_destroy (handler->priv->descs);
		handler->priv->descs = NULL;
	}

	if (handler->priv->strs)
	{
		g_list_foreach (handler->priv->strs, (GFunc) g_free, NULL);
		g_list_free (handler->priv->strs);
		handler->priv->strs = NULL;
	}

	bg_ext_handler_parent_class->dispose (object);
}

static void
bg_ext_handler_finalize (GObject *object)
{
	BgExtHandler *handler = BG_EXT_HANDLER (object);

	g_free (handler->priv);

	bg_ext_handler_parent_class->finalize (object);
}

static void
bg_ext_handler_class_init (GObjectClass *object_class)
{
	bg_ext_handler_parent_class = gtk_type_class (mtm_ext_handler_get_type ());

	object_class->set_property = bg_ext_handler_set_arg;
	object_class->get_property= bg_ext_handler_get_arg;
	object_class->dispose = bg_ext_handler_dispose;
	object_class->finalize = bg_ext_handler_finalize;

	g_object_class_install_property
		(object_class, ARG_COLOR1,
		 g_param_spec_boxed ("Color1",
			 	     "Color1",
				     "Color1",
				     gdk_color_get_type (),
				     G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_COLOR2,
		 g_param_spec_boxed ("Color2",
			 	     "Color2",
				     "Color2",
				     gdk_color_get_type (),
				     G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_COLOR_GRADIENT,
		 g_param_spec_boolean ("ColorGradient",
			 	       "ColorGradient",
				       "ColorGradient",
				       FALSE,
				       G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_GRADIENT_TYPE,
		 g_param_spec_enum ("GradientType",
			 	    "GradientType",
				    "GradientType",
				    bg_grad_get_type (),
				    0,
				    G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_WALLPAPER_LAYOUT,
		 g_param_spec_enum ("WallpaperLayout",
			 	    "WallpaperLayout",
				    "WallpaperLayout",
				    bg_wp_get_type (),
				    0,
				    G_PARAM_READWRITE));
	g_object_class_install_property
		(object_class, ARG_EXT_CONTEXT,
		 g_param_spec_object ("ext_context",
			 	      "ext_context",
				      "ext_context",
				      MTM_EXT_TYPE,
				      G_PARAM_READWRITE));
}

static void
bg_ext_handler_init (GtkObject *object)
{
	BgExtHandler *handler = BG_EXT_HANDLER (object);
	int i = 0;
	gchar *key, *val;
	static gchar* args[] = {"Color1", "Color2", "ColorGradient", "GradientType", "WallpaperLayout", NULL};
	static gchar* descs[] = {_("Primary Color"), _("Secondary Color"), _("Use Gradient"), _("Gradient Type"), _("Wallpaper Layout Type"), NULL};

	MTM_EXT_HANDLER (handler)->describe_arg = bg_ext_handler_describe_arg;

	handler->priv = g_new0 (BgExtHandlerPrivate, 1);
	
	handler->priv->descs = g_hash_table_new (g_str_hash, g_str_equal);

	while (args[i] != NULL)
	{
		key = g_strdup (args[i]);
		handler->priv->strs = g_list_prepend (handler->priv->strs, key);

		val = g_strdup (descs[i]);
		handler->priv->strs = g_list_prepend (handler->priv->strs, val);

		g_hash_table_insert (handler->priv->descs, key, val);
		i++;
	}

	mtm_ext_handler_register_args (MTM_EXT_HANDLER (handler), args);
}

GType
bg_ext_handler_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info =
		{
			sizeof (BgExtHandlerClass),
			NULL,
			NULL,
			(GClassInitFunc) bg_ext_handler_class_init,
			NULL,
			NULL,
			sizeof (BgExtHandler),
			0,	
			(GInstanceInitFunc) bg_ext_handler_init,
			NULL
		};

		type = g_type_register_static (MTM_EXT_HANDLER_TYPE,
					       "BgExtHandler",
					       &info, 0);
	}

	return type;
}

BgExtHandler*
bg_ext_handler_new (MtmEnv *env)
{
	BgExtHandler *handler;

	g_return_val_if_fail (env != NULL, NULL);

	handler = g_object_new (bg_ext_handler_get_type (), NULL);
	MTM_STATEFUL (handler)->env = env;
	return handler;
}

static void
bg_ext_handler_set_arg (GObject *object, guint arg_id, const GValue *value, GParamSpec *spec)
{
	BgExtHandler *handler = BG_EXT_HANDLER (object);
	BGPreferences *config;
	GdkColor *color;

	switch (arg_id)
	{
		case ARG_COLOR1:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			color = g_value_get_boxed (value);
			*(config->color1) = *color;
			break;
		case ARG_COLOR2:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			color = g_value_get_boxed (value);
			*(config->color2) = *color;
			break;
		case ARG_COLOR_GRADIENT:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			config->gradient_enabled = g_value_get_boolean (value);
			break;
		case ARG_GRADIENT_TYPE:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			config->orientation = g_value_get_enum (value);
			break;
		case ARG_WALLPAPER_LAYOUT:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			config->wallpaper_type = g_value_get_enum (value);
			break;
		case ARG_EXT_CONTEXT:
			handler->priv->ext = MTM_EXT (g_value_get_object (value));
			config = g_object_get_data (
				G_OBJECT (handler->priv->ext), "bgconfig");
			if (!config)
			{
				config = BG_PREFERENCES (bg_preferences_new ());
				bg_preferences_load (config);
				handler->priv->configs = g_list_append (
					handler->priv->configs, config);
				g_object_set_data (
					G_OBJECT (handler->priv->ext),
					"bgconfig", config);
			}
			break;
	}
}

static void
bg_ext_handler_get_arg (GObject *object, guint arg_id, GValue *value, GParamSpec *spec)
{
	BgExtHandler *handler = BG_EXT_HANDLER (object);
	BGPreferences *config;

	switch (arg_id)
	{
		case ARG_COLOR1:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			g_value_set_boxed (value, config->color1);
			break;
		case ARG_COLOR2:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			g_value_set_boxed (value, config->color2);
			break;
		case ARG_COLOR_GRADIENT:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			g_value_set_boolean (value, config->gradient_enabled);
			break;
		case ARG_GRADIENT_TYPE:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			g_value_set_enum (value, config->orientation);
			break;
		case ARG_WALLPAPER_LAYOUT:
			config = bg_ext_handler_get_config (handler);
			g_return_if_fail (config != NULL);
			g_value_set_enum (value, config->wallpaper_type);
			break;
		case ARG_EXT_CONTEXT:
			g_value_set_object (value, G_OBJECT (handler->priv->ext));
			break;
	}
}

BGPreferences*
bg_ext_handler_get_config (BgExtHandler *handler)
{
	BGPreferences *prefs;
	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (handler->priv->ext != NULL, NULL);
	prefs = g_object_get_data (G_OBJECT (handler->priv->ext), "bgconfig");
	
	if (prefs->wallpaper_filename)
	{
		if (handler->priv->ext->file)
		{
			if (strcmp (prefs->wallpaper_filename, handler->priv->ext->file))
			{
				g_free (prefs->wallpaper_filename);
				prefs->wallpaper_filename = g_strdup (handler->priv->ext->file);
			}
		}
		else
		{
			g_free (prefs->wallpaper_filename);
			prefs->wallpaper_filename = NULL;
		}
	}

	if (!prefs->wallpaper_filename)
		prefs->wallpaper_enabled = FALSE;

	return prefs;
}

static gchar*
bg_ext_handler_describe_arg (MtmExtHandler *handler, gchar *arg)
{
	g_return_val_if_fail (arg != NULL, NULL);

	return g_strdup (g_hash_table_lookup (BG_EXT_HANDLER (handler)->priv->descs, arg));
}
