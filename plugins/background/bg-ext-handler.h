#ifndef __BG_EXT_HANDLER_H__
#define __BG_EXT_HANDLER_H__

#define BG_EXT_HANDLER_TYPE		(bg_ext_handler_get_type ())
#define BG_EXT_HANDLER(o)		(GTK_CHECK_CAST ((o), BG_EXT_HANDLER_TYPE, BgExtHandler))
#define BG_EXT_HANDLER_CLASS(k)		(GTK_CHECK_CLASS_CAST ((k), BG_EXT_HANDLER, BgExtHandlerClass))
#define BG_IS_EXT_HANDLER(o)		(GTK_CHECK_TYPE ((o), BG_EXT_HANDLER_TYPE))
#define BG_IS_EXT_HANDLER_CLASS(k)	(GTK_CHECK_CLASS_CAST_TYPE ((k), BG_EXT_HANDLER_TYPE))

typedef struct _BgExtHandler BgExtHandler;
typedef struct _BgExtHandlerPrivate BgExtHandlerPrivate;

typedef struct _BgExtHandlerClass BgExtHandlerClass;

#include <mtm/mtm-ext-handler.h>
#include <gdk/gdktypes.h>
#include <preferences.h>

struct _BgExtHandler
{
	MtmExtHandler parent;

	BgExtHandlerPrivate *priv;
};

struct _BgExtHandlerClass
{
	MtmExtHandlerClass parent_class;
};

GType bg_ext_handler_get_type (void);

BgExtHandler* bg_ext_handler_new (MtmEnv *env);
 
BGPreferences* bg_ext_handler_get_config (BgExtHandler *handler);

#endif /* __BG_EXT_HANDLER_H__ */
