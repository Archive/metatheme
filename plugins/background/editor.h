#ifndef __EDITOR_H__
#define __EDITOR_H__

#include <gnome.h>
#include <mtm/mtm.h>
#include <gmodule.h>

typedef enum {
	WALLPAPER_TILED,
	WALLPAPER_CENTERED,
	WALLPAPER_SCALED,
	WALLPAPER_SCALED_KEEP,
} WpType;

typedef enum {
	VERTICAL,
	HORIZONTAL
} GradType;

typedef struct {
	char *filename;
	WpType align;
	GdkColor color1, color2;
	gboolean gradient;
	GradType gradtype;
} BgConfig;

/* gnome_config stuff */
BgConfig *bgconfig_load (char *filename);
void bgconfig_save (BgConfig *config, char *filename);
void bgconfig_destroy (BgConfig *config);

#endif /* __EDITOR_H__ */
