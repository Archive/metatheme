/*
 *
 * bg-config-gui.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm/mtm-config-gui.h>
#include <mtm/mtm-ext-handler.h>
#include "bg-config-gui.h"
#include <sys/types.h>
#include <dirent.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glade/glade-xml.h>
#include <string.h>
#include <applier.h>
#include "bg-ext-handler.h"

#define GLADEFILE (SHAREDIR "bg-config-gui.glade")

#define PREVIEW_WIDTH 256
#define PREVIEW_HEIGHT 192

typedef struct 
{
	MtmExt *ext;
	GladeXML *xml;
	BGApplier *applier;
	gboolean realized;
} ConfigPreview;

#if 0
static gchar**
generate_file_list (void)
{
	GArray *files;
	gchar **ret;
	/* FIXME */
	gchar *dirnames[] = { "/usr/share/sawfish/themes", "/.sawfish/themes", NULL};
	int i;

	dirnames[1] = g_strconcat (g_get_home_dir (), dirnames[1], NULL);
	
	files = g_array_new (TRUE, TRUE, sizeof (gchar*));
	
	for (i = 0; dirnames[i] != NULL; i++)
	{
		DIR *dir;
		struct dirent *de;
		dir = opendir (dirnames[i]);
		if (!dir)
			continue;

		while ((de = readdir (dir)))
		{
			gchar *filename;
			
			if (de->d_name[0] == '.')
				continue;
			
			filename = g_strconcat (dirnames[i], "/", de->d_name, NULL);
			g_array_append_val (files, filename);
		}
		
		closedir (dir);
	}

	g_free (dirnames[1]);
	ret = (gchar**) files->data;
	g_array_free (files, FALSE);

	return ret;
}
#endif

static void
destroy_cb (MtmConfigGui *gui, ConfigPreview *preview)
{
	g_return_if_fail (preview != NULL);
	g_object_unref (G_OBJECT (preview->applier));
	g_object_unref (G_OBJECT (preview->xml));
	g_free (preview);
}

static void
update_preview (ConfigPreview *preview)
{
	if (!(preview->ext && preview->realized))
		return;
	bg_applier_apply_prefs (preview->applier, bg_ext_handler_get_config (BG_EXT_HANDLER (preview->ext->handler)));
}

static void
update_widgets (ConfigPreview *preview)
{
	GtkWidget *w;
	MtmExtHandler *handler;
	GdkColor *color1, *color2;
	gboolean gradient;
	int gtype, ltype;

	g_return_if_fail (preview != NULL);
	g_return_if_fail (preview->ext != NULL);
	
	handler = mtm_ext_get_handler (preview->ext);
	g_object_set (G_OBJECT (handler), "ext_context", preview->ext, NULL);
	g_object_get (G_OBJECT (handler),
		      "Color1", &color1,
		      "Color2", &color2,
		      "ColorGradient", &gradient,
		      "GradientType", &gtype,
		      "WallpaperLayout", &ltype,
		      NULL);

	w = glade_xml_get_widget (preview->xml, "colorpicker1");
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (w),
				    color1->red, color1->green, color1->blue,
				    65535);
	w = glade_xml_get_widget (preview->xml, "colorpicker2");
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (w),
				    color2->red, color2->green, color2->blue,
				    65535);
	w = glade_xml_get_widget (preview->xml, "pattern_menu");
	gtk_option_menu_set_history (GTK_OPTION_MENU (w),
				     gtype);
	w = glade_xml_get_widget (preview->xml, "align_menu");
	gtk_option_menu_set_history (GTK_OPTION_MENU (w), ltype);
}

static void
ext_changed_cb (MtmConfigGui *gui, MtmExt *ext, ConfigPreview *preview)
{
	BGPreferences *prefs;
	g_return_if_fail (preview != NULL);

	g_object_set (G_OBJECT (preview->ext->handler), "ext_context", ext, NULL);
	prefs = bg_ext_handler_get_config (BG_EXT_HANDLER (preview->ext->handler));
	if (ext->file)
		prefs->wallpaper_enabled = TRUE;
	else
		prefs->wallpaper_enabled = FALSE;

	update_widgets (preview);
	update_preview (preview);
}
	
static void
set_ext_cb (MtmConfigGui *gui, MtmExt *ext, ConfigPreview *preview)
{
	g_return_if_fail (preview != NULL);
	preview->ext = ext;
	update_widgets (preview);
	update_preview (preview);
}

static void
set_file_entry_cb (MtmConfigGui *gui, GnomeFileEntry *entry, ConfigPreview *preview)	
{
	preview_file_selection_hookup_file_entry (entry, _("Please select a background image"));
}

static void
set_ext_arg (ConfigPreview *preview, gchar *arg, gboolean is_int,
	     gpointer pvalue, int ivalue)
{
	g_return_if_fail (preview != NULL);
	g_return_if_fail (arg != NULL);
	
	if (!preview->ext)
		return;

	g_object_set (G_OBJECT (preview->ext->handler),
		      "ext_context", preview->ext, NULL);
	if (is_int)
		g_object_set (G_OBJECT (preview->ext->handler), arg, ivalue, NULL);
	else
		g_object_set (G_OBJECT (preview->ext->handler), arg, pvalue, NULL);

	update_preview (preview);
}

static void
generic_color_set_cb (GnomeColorPicker *cp, guint r, guint g, guint b, ConfigPreview *preview, gchar *arg)
{
	GdkColor color;

	g_return_if_fail (preview != NULL);
	g_return_if_fail (arg != NULL);
	
	color.red = r;
	color.green = g;
	color.blue = b;
	
	set_ext_arg (preview, arg, FALSE, &color, -1);
}

static void
color1_set_cb (GnomeColorPicker *cp, guint r, guint g, guint b, guint a,
	       ConfigPreview *preview)
{
	generic_color_set_cb (cp, r, g, b, preview, "Color1");
}

static void
color2_set_cb (GnomeColorPicker *cp, guint r, guint g, guint b, guint a,
	       ConfigPreview *preview)
{
	generic_color_set_cb (cp, r, g, b, preview, "Color2");
}

static void
pattern_set_cb (GtkMenuItem *item, ConfigPreview *preview)
{
	orientation_t type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "index"));
	gboolean gradient;
	
	if (type == 0)
		gradient = FALSE;
	else
		gradient = TRUE;
	
	set_ext_arg (preview, "ColorGradient", TRUE, NULL, gradient);
	if (gradient)
		set_ext_arg (preview, "GradientType", TRUE, NULL, type);
}

static void
align_set_cb (GtkMenuItem *item, ConfigPreview *preview)
{
	wallpaper_type_t type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "index"));
	set_ext_arg (preview, "WallpaperLayout", TRUE, NULL, type);
}

static void
connect_menu (gchar *wname, GtkSignalFunc func, ConfigPreview *preview)
{
	GtkWidget *w, *menu;
	GtkMenuItem *item;
	GList *l;
	int index = 0;
	
	g_return_if_fail (wname != NULL);
	g_return_if_fail (preview != NULL);
	g_return_if_fail (preview->xml != NULL);
	
	w = glade_xml_get_widget (preview->xml, wname);
	g_return_if_fail (w != NULL);
	
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (w));
	for (l = GTK_MENU_SHELL (menu)->children; l != NULL; l = l->next)
	{
		item = GTK_MENU_ITEM (l->data);
		g_object_set_data (G_OBJECT (item), "index", GINT_TO_POINTER (index++));
		g_signal_connect (G_OBJECT (item), "activate", (GCallback) func, preview);
	}
}

static void
realized_cb (GtkWidget *w, ConfigPreview *preview)
{
	preview->realized = TRUE;
	update_preview (preview);
}

MtmConfigGui*
bg_config_gui_new (MtmGuiHandler *handler)
{
	GladeXML *xml;
	GtkWidget *w;
	MtmConfigGui *gui;
	ConfigPreview *preview;

	xml = glade_xml_new (GLADEFILE, "hbox1", NULL);
	if (!xml)
		return NULL;

	gui = mtm_config_gui_new ();
#if 0
	files = generate_file_list ();
	mtm_config_gui_set_file_list (gui, files);
	g_strfreev (files);
#endif

	w = glade_xml_get_widget (xml, "hbox1");
	mtm_config_gui_set_config_area (gui, w); 

	preview = g_new0 (ConfigPreview, 1);
	preview->xml = xml;
	preview->ext = NULL;
	preview->realized = FALSE;
	preview->applier = BG_APPLIER (bg_applier_new_at_size (BG_APPLIER_PREVIEW, PREVIEW_WIDTH, PREVIEW_HEIGHT));

	w = glade_xml_get_widget (xml, "preview_frame");
	gtk_container_add (GTK_CONTAINER (w), bg_applier_get_preview_widget (preview->applier));
	g_signal_connect (G_OBJECT (w), "realize", (GCallback) realized_cb, preview);
	
	g_signal_connect_after (G_OBJECT (gui), "destroy",
			          (GCallback) destroy_cb, preview);
	
	g_signal_connect (G_OBJECT (gui), "set_ext",
			    (GCallback) set_ext_cb, preview);
	g_signal_connect (G_OBJECT (gui), "ext_modified",
			    (GCallback) ext_changed_cb, preview);
	g_signal_connect (G_OBJECT (gui), "set_file_entry",
			  (GCallback) set_file_entry_cb, preview);

	w = glade_xml_get_widget (xml, "colorpicker1");
	g_signal_connect (G_OBJECT (w), "color_set",
			    (GCallback) color1_set_cb, preview);
	w = glade_xml_get_widget (xml, "colorpicker2");
	g_signal_connect (G_OBJECT (w), "color_set",
			    (GCallback) color2_set_cb, preview);
	connect_menu ("pattern_menu",
		      (GCallback) pattern_set_cb, preview);
	connect_menu ("align_menu",
		      (GCallback) align_set_cb, preview);

	gtk_widget_show_all (w);

	return gui;
}
