#ifndef __BG_CONFIG_GUI_H__ 
#define __BG_CONFIG_GUI_H__

#include <mtm/mtm-gui-handler.h>

MtmConfigGui* bg_config_gui_new (MtmGuiHandler *handler);

#endif /* __BG_CONFIG_GUI_H__ */
