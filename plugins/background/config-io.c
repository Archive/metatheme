#include "editor.h"
#include <string.h>

BgConfig *bgconfig_load (char *filename)
{
	BgConfig *config;
	char *path;
	char *tmp;
	
	g_return_val_if_fail (filename != NULL, NULL);
	
	gnome_config_pop_prefix ();

	config = g_new0 (BgConfig, 1);
	
	path = g_strconcat ("=", filename, "=/Default/wallpaper=none", NULL);
	config->filename = gnome_config_get_string (path);
	g_free (path);
	if (strcmp (config->filename, "none") == 0)
	{
		g_free (config->filename);
		config->filename = NULL;
	}

	path = g_strconcat ("=", filename, "=/Default/wallpaperAlign=0", NULL);
	config->align = gnome_config_get_int (path);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/color1=#005060", NULL);
	tmp = gnome_config_get_string (path);
	gdk_color_parse (tmp, &config->color1);
	g_free (tmp);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/color2=#0000ff", NULL);
	tmp = gnome_config_get_string (path);
	gdk_color_parse (tmp, &config->color2);
	g_free (tmp);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/simple=solid", NULL);
	tmp = gnome_config_get_string (path);
	config->gradient = (strcasecmp (tmp, "solid") != 0);
	g_free (tmp);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/gradient=vertical", NULL);
	tmp = gnome_config_get_string (path);
	config->gradtype = (strcasecmp (tmp, "horizontal") == 0);
	g_free (tmp);
	g_free (path);

	return config;
}

void bgconfig_save (BgConfig *config, char *filename)
{
	char *path;
	char *tmp;
	
	g_return_if_fail (config != NULL);
	g_return_if_fail (filename != NULL);
	
	gnome_config_pop_prefix ();

	path = g_strconcat ("=", filename, "=/Default/wallpaper", NULL);
	gnome_config_set_string (path,
		config->filename ? config->filename : "none");
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/wallpaperAlign", NULL);
	gnome_config_set_int (path, config->align);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/color1", NULL);
	tmp = g_strdup_printf ("#%02x%02x%02x",
		config->color1.red >> 8,
		config->color1.green >> 8,
		config->color1.blue >> 8);
	gnome_config_set_string (path, tmp);
	g_free (tmp);
	g_free (path);
	
	path = g_strconcat ("=", filename, "=/Default/color2", NULL);
	tmp = g_strdup_printf ("#%02x%02x%02x",
		config->color2.red >> 8,
		config->color2.green >> 8,
		config->color2.blue >> 8);
	gnome_config_set_string (path, tmp);
	g_free (tmp);
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/simple", NULL);
	gnome_config_set_string (path,
		config->gradient ? "gradient" : "solid");
	g_free (path);

	path = g_strconcat ("=", filename, "=/Default/gradient", NULL);
	gnome_config_set_string (path,
		(config->gradtype == VERTICAL) ? "vertical" : "horizontal");
	g_free (path);

	gnome_config_sync ();
}

void bgconfig_destroy (BgConfig *config)
{
	g_return_if_fail (config != NULL);

	if (config->filename)
		g_free (config->filename);
	g_free (config);
}
