/*
 *
 * background-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include "editor.h"
#include <mtm/mtm.h>
#include <gdk/gdkrgb.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include "bg-ext-handler.h"
#include "bg-config-gui.h"
#include <preferences.h>

static MtmResult bg_plugin_theme_activate  (MtmExtHandler *handler, MtmExt *theme);
static gchar *   bg_plugin_theme_find      (MtmExtHandler *handler, const gchar *name, gboolean is_root);

static MtmResult bg_plugin_update_ext (MtmExtHandler *handler);

static gboolean bg_plugin_ext_is_installed (MtmExtHandler *handler);

static gchar *bg_plugin_get_current_theme (MtmExtHandler *handler);
static gchar *bg_plugin_get_ext_version (MtmExtHandler *handler);
static gboolean bg_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2);

static MtmResult
bg_plugin_theme_activate (MtmExtHandler *handler, MtmExt *theme)
{
	BGPreferences *config;
	
	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);
	
	if (!theme)
		return MTM_GENERAL_ERROR;
	
	g_object_set (G_OBJECT (handler), "ext_context", theme, NULL);
	config = bg_ext_handler_get_config (BG_EXT_HANDLER (handler));
	bg_preferences_save (config);

	return MTM_OK;
}

static MtmResult bg_plugin_update_ext (MtmExtHandler *handler)
{
	gconf_client_suggest_sync (gconf_client_get_default (), NULL);
	return MTM_OK;
}

static gchar *
bg_plugin_get_current_theme (MtmExtHandler *handler)
{
	BGPreferences *config;
	gchar *filename;
	
	/* no ext_context yet so we'll just load a fresh one */
	config = BG_PREFERENCES (bg_preferences_new ());
	bg_preferences_load (config);
	filename = g_strdup (config->wallpaper_filename);
	g_object_unref (G_OBJECT (config));

	return filename;
}

#undef DGB

static gboolean
bg_plugin_ext_is_installed (MtmExtHandler *handler)
{
	return TRUE;
}

static gchar *
bg_plugin_theme_find (MtmExtHandler *handler, const gchar *name, gboolean is_root)
{
	gchar *root_prefixes[] = {"/usr/share/pixmaps/backgrounds",
		"/usr/share/pixmaps/backgrounds/tiles",
		"/usr/share/wallpapers",
		NULL};
	gchar *user_prefixes[] = {".metatheme-backgrounds",
		".backgrounds",
		"GNUstep/Library/WindowMaker/Backgrounds", NULL};
	gchar **arr;
	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (is_root)
		arr = root_prefixes;
	else
		arr = user_prefixes;
	
	return mtm_find_file_in_path (arr, name, is_root);
}

static gchar *
bg_plugin_get_ext_version (MtmExtHandler *handler)
{
	return NULL;
}

static gboolean
bg_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	/* FIXME*/
	return TRUE;
}

int mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;
	
	g_return_val_if_fail (pd != NULL, -1);

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	pd->title = g_strdup ("Gnome Background Plugin");

	env = MTM_STATEFUL (pd)->env;
	
	handler = MTM_EXT_HANDLER (bg_ext_handler_new (env));

	handler->activate  = bg_plugin_theme_activate;
	handler->find      = bg_plugin_theme_find;
	
	handler->get_current_theme = bg_plugin_get_current_theme;
	handler->update_ext = bg_plugin_update_ext;
	handler->ext_is_installed = bg_plugin_ext_is_installed;
	handler->get_ext_version = bg_plugin_get_ext_version;
	handler->version_is_compat = bg_plugin_version_is_compat;
	
	MTM_HANDLER (handler)->desc = g_strdup (_("Gnome Background Image"));
	MTM_HANDLER (handler)->key = g_strdup ("background");
	handler->editcmd = NULL;
	
	mtm_handler_register (MTM_HANDLER (handler));

	guih = mtm_gui_handler_new (env);
	guih->create_gui = bg_config_gui_new; 
	guih->name = g_strdup (_("Background"));
	MTM_HANDLER (guih)->desc = g_strdup (_("Your background is composed of two parts: the background image, and the background pattern, which appears behind the background image."));
	MTM_HANDLER (guih)->key = g_strdup ("background");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}
