/*
 *
 * nautilus-plugin.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <mtm/mtm.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <string.h>
#include <libgnome/libgnome.h>
#include <unistd.h>
#include "nautilus-config-gui.h"

#define GCONF_KEY "/apps/nautilus/preferences/theme"

static MtmResult
nautilus_plugin_theme_activate (MtmExtHandler *handler, MtmExt *ext)
{
	GConfClient *client;
	gchar *path, *newfile;
	
	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), MTM_GENERAL_ERROR);
	g_return_val_if_fail (MTM_IS_EXT (ext), MTM_GENERAL_ERROR);

	path = g_strconcat (g_get_home_dir (), "/.nautilus/themes", NULL);
	mtm_check_dir (path);
	newfile = g_strconcat (path, "/", g_basename (ext->file), NULL);
	
	g_free (path);
	/* Can't check for existing file errors here, or else we confuse
	 * ourselves. */
	symlink (ext->file, newfile);
	
	client = gconf_client_get_default ();
	if (!gconf_client_set_string (client, GCONF_KEY, g_basename (ext->file), NULL))
		return MTM_NO_ACCESS;

	gconf_client_suggest_sync (client, NULL);

	g_free (newfile);

	return MTM_OK;
}

/* FIXME: This does not handle custom theme dirs, nautilus dirs, etc */
static gchar*
nautilus_plugin_theme_find (MtmExtHandler *handler, const gchar *name,
			    gboolean is_root)
{
	gchar *root_prefixes[] = {PIXMAPSDIR"/nautilus", NULL};
	gchar *user_prefixes[] = {".nautilus/themes/", NULL};
	
	return mtm_find_file_in_path ((is_root) ? root_prefixes : user_prefixes,
				      name, is_root);
}

static MtmResult
nautilus_plugin_update_ext (MtmExtHandler *handler)
{
	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), MTM_GENERAL_ERROR);

	/* GConf handles this for us */
	return MTM_OK;
}

static gchar*
nautilus_plugin_get_ext_version (MtmExtHandler *handler)
{
	FILE *pipe;
	gchar ver[1000];
	gchar *str;
	gchar *ret;
	int len;

	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), NULL);
	
	pipe = popen ("nautilus --version", "r");
	if (!pipe)
		return NULL;
	fgets (ver, 1000, pipe);
	pclose (pipe);
	str = strstr (ver, "Gnome nautilus ");
	len = strlen ("Gnome nautilus ");
	ret = g_strdup (str + len);
	ret = g_strchomp (ret);
	return ret;
}

static gboolean
nautilus_plugin_ext_is_installed (MtmExtHandler *handler)
{
	gchar *ver;

	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), FALSE);
	
	ver = nautilus_plugin_get_ext_version (handler);

	if (!ver)
		return FALSE;

	g_free (ver);
	return TRUE;
}

static gchar*
nautilus_plugin_get_current_theme (MtmExtHandler *handler)
{
	GConfClient *client;

	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), NULL);

	client = gconf_client_get_default ();
	return gconf_client_get_string (client, GCONF_KEY, NULL);
}

static gboolean
nautilus_plugin_version_is_compat (MtmExtHandler *handler, gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (MTM_IS_EXT_HANDLER (handler), FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	/* FIXME */
	return TRUE;
}

int
mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;
	gchar *argv[] = {"mtm-nautilus-plugin"};
	
	g_return_val_if_fail (MTM_IS_PLUGIN (pd), -1);

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	pd->title = g_strdup ("Nautilus Plugin");

	env = MTM_STATEFUL (pd)->env;

	handler = mtm_ext_handler_new (env);

	handler->activate = nautilus_plugin_theme_activate;
	handler->find = nautilus_plugin_theme_find;
	handler->get_current_theme = nautilus_plugin_get_current_theme;
	handler->update_ext = nautilus_plugin_update_ext;
	handler->ext_is_installed = nautilus_plugin_ext_is_installed;
	handler->get_ext_version = nautilus_plugin_get_ext_version;
	handler->version_is_compat = nautilus_plugin_version_is_compat;

	MTM_HANDLER (handler)->desc = g_strdup (_("Nautilus file manager"));
	MTM_HANDLER (handler)->key = g_strdup ("nautilus");
	handler->editcmd = NULL;
	handler->default_suffix = NULL;

	if (!gconf_is_initialized ())
		gconf_init (1, argv, NULL);
	
	mtm_handler_register (MTM_HANDLER (handler));
	guih = mtm_gui_handler_new (env);
	guih->create_gui = nautilus_config_gui_new;
	guih->name = g_strdup (_("Nautilus"));
	MTM_HANDLER (guih)->desc = g_strdup (_("Nautilus is the GNOME file manager. Nautilus also controls the appearance and behavior of desktop icons."));
	MTM_HANDLER (guih)->key = g_strdup ("nautilus");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}
