/*
 *
 * nautilus-config-gui.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */
#include <config.h>
#include "nautilus-config-gui.h"
#include <gtk/gtkpixmap.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkframe.h>
#include <gtk/gtklabel.h>
#include <gtk/gtksignal.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnome/libgnome.h>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include <string.h>

typedef struct
{
	GtkWidget *pixmap;
	GtkWidget *label;
	GtkWidget *hbox;	
} ConfigPreview;

static void
destroy_cb (GtkWidget *widget, ConfigPreview *preview)
{
	g_free (preview);
}

static void
ext_changed_cb (MtmConfigGui *gui, MtmExt *ext, ConfigPreview *preview)
{
	gchar *file;
	int len;
	xmlDocPtr doc;
	xmlNodePtr node;
	gchar *xmlfile;
	GdkPixbuf *imgbuf, *buf;
	gchar *imgfile;
	gchar *name, *desc;
	gchar *labelstr;
	GdkPixmap *pmap;
	GdkBitmap *bmap;

	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	g_return_if_fail (MTM_IS_EXT (ext));
	g_return_if_fail (preview != NULL);

	file = ext->file;
	if (!file)
		return;

	len = strlen (file);
	if (file[len - 1] == '/')
		file[len - 1] = '\0';
	
	/* Description label stuff */	
	xmlfile = g_strconcat (file, "/", g_basename (file), ".xml", NULL);
	doc = xmlParseFile (xmlfile);
	g_free (xmlfile);
	if (!doc)
	{
		g_warning ("uh-oh!\n");
		return;
	}
	
	node = doc->children;
	if (!(node && node->children && !strcmp (node->name, "theme")))
	{
		xmlFreeDoc (doc);
		g_warning ("invalid doc %s\n", node->name);
		return;
	}

	name = xmlGetProp (node, "name");
	if (!name)
		name = xmlGetProp (node, "_name");
	
	desc = xmlGetProp (node, "description");
	if (!desc)
		desc = xmlGetProp (node, "_description");

	labelstr = g_strdup_printf ("%s:\n%s",
				    (name) ? name : g_basename (file),
				    (desc) ? desc : "");
	gtk_label_set_text (GTK_LABEL (preview->label), labelstr);
	
	if (name)
		xmlFree (name);
	
	if (desc)
		xmlFree (desc);

	g_free (labelstr);

	xmlFreeDoc (doc);

	/* Preview image stuff */
	imgfile = g_strconcat (ext->file, "/theme_preview.png", NULL);
	imgbuf = gdk_pixbuf_new_from_file (imgfile, NULL);
	g_free (imgfile);
	if (!imgbuf)
		return;
		
	buf = gdk_pixbuf_composite_color_simple (imgbuf,
		       				 gdk_pixbuf_get_width (imgbuf),
					 	 gdk_pixbuf_get_height (imgbuf),
						 GDK_INTERP_BILINEAR,
						 255,
						 2,
						 0xffffff,
						 0xffffff);

	gdk_pixbuf_render_pixmap_and_mask (buf, &pmap, &bmap, 255);
	
	if (preview->pixmap)
		gtk_pixmap_set (GTK_PIXMAP (preview->pixmap), pmap, bmap);
	else
	{
		preview->pixmap = gtk_pixmap_new (pmap, bmap);
		gtk_box_pack_start (GTK_BOX (preview->hbox), preview->pixmap, FALSE, FALSE, 8);
	}

	gdk_pixbuf_unref (imgbuf);
	gdk_pixbuf_unref (buf);
}

MtmConfigGui*
nautilus_config_gui_new (MtmGuiHandler *handler)
{
	GtkWidget *frame;
	MtmConfigGui *gui;
	ConfigPreview *preview;
	gchar **files;
	
	g_return_val_if_fail (MTM_IS_GUI_HANDLER (handler), NULL);
	
	gui = mtm_config_gui_new ();

	files = g_new0 (gchar*, 1);
	mtm_config_gui_set_file_list (gui, (const gchar**) files);
	g_strfreev (files);

	preview = g_new0 (ConfigPreview, 1);
	g_signal_connect_after (G_OBJECT (gui), "destroy", (GCallback) destroy_cb, preview);
	g_signal_connect (G_OBJECT (gui), "set_ext", (GCallback) ext_changed_cb, preview);
	g_signal_connect (G_OBJECT (gui), "ext_modified", (GCallback) ext_changed_cb, preview);

	frame = gtk_frame_new (_("Information"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), 8);
	
	preview->hbox = gtk_hbox_new (FALSE, 4);
	gtk_container_add (GTK_CONTAINER (frame), preview->hbox);

	preview->label = gtk_label_new ("");
	gtk_box_pack_end (GTK_BOX (preview->hbox), preview->label, TRUE, TRUE, 0);

	mtm_config_gui_set_config_area (gui, frame);
	gtk_widget_show_all (frame);

	return gui;	
}
