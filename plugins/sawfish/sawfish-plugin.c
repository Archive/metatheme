/*
 *
 * sawfish-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <mtm/mtm.h>
#include <gtk/gtk.h>
#include <sys/stat.h>
#include <string.h>
#include <libgnome/libgnome.h>
#include "sawfish-config-gui.h"

#define MARK_STRING "# -- THEME AUTO-WRITTEN DO NOT EDIT"

static MtmResult sawfish_plugin_theme_activate  (MtmExtHandler *handler, MtmExt *theme);
static gchar *   sawfish_plugin_theme_find      (MtmExtHandler *handler,     const gchar *name, gboolean is_root);

static MtmResult sawfish_plugin_update_ext (MtmExtHandler *handler);
static gboolean  sawfish_plugin_ext_is_installed (MtmExtHandler *handler);
static gchar *   sawfish_plugin_get_current_theme (MtmExtHandler *handler);
static gchar *   sawfish_plugin_get_ext_version (MtmExtHandler *handler);
static gboolean  sawfish_plugin_version_is_compat (MtmExtHandler *handler,gchar *ver1, gchar *ver2);

static MtmResult
sawfish_plugin_theme_activate (MtmExtHandler *handler, MtmExt *theme)
{
	gchar *command;
	gchar *tname = NULL, *path;
	gchar *newfile, *base;
	struct stat s;
	
	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);

	if (theme)
		tname = g_strdup (theme->file);

	g_return_val_if_fail (tname != NULL, MTM_GENERAL_ERROR);
	
	if (tname[strlen(tname)-1] == '/')
		tname[strlen(tname)-1] = '\0';
	
	path = g_strconcat (g_get_home_dir (), "/.sawfish/themes", NULL);
	mtm_check_dir (path);
	base = g_path_get_basename (tname);
	newfile = g_strconcat (path, "/", base, NULL);
	/* Let's make a symlink because sawfish is dumb */
	if (lstat (newfile, &s) == 0)
	{
		if (S_ISLNK (s.st_mode))
			unlink (newfile);
	}

	if (!g_file_test (newfile, G_FILE_TEST_EXISTS))
		symlink (theme->file, newfile);

	g_free (path);
	g_free (newfile);

	command = g_strdup_printf (
"sawfish-client -e \"(progn (require 'customize) (customize-set 'default-frame-style '|%s|))\"", base);
	system (command);
	
	g_free (command);
	g_free (tname);
	g_free (base);
   
	return MTM_OK;
}

static MtmResult
sawfish_plugin_update_ext (MtmExtHandler *handler)
{
	gchar *command;
	gchar *tname;
	
	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);

	/* FIXME */
	tname = sawfish_plugin_get_current_theme (handler);
	command = g_strdup_printf (
"sawfish-client -e \"(reload-frame-style '|%s|)\"", tname);
	system (command);
	
	g_free (command);
	g_free (tname);
   
	return MTM_OK;
}

static gboolean
sawfish_plugin_ext_is_installed (MtmExtHandler *handler)
{
	FILE *pipe;
	
	g_return_val_if_fail (handler != NULL, FALSE);

	pipe = popen ("sawfish --version", "r");
	if (!pipe)
		return FALSE;
	pclose (pipe);
	return TRUE;
}

static gchar*
sawfish_plugin_get_ext_version (MtmExtHandler *handler)
{	
	FILE *pipe;
	char ver[1000];
	char *str;
	char *ret;
	int len;
	
	g_return_val_if_fail (handler != NULL, FALSE);

	pipe = popen ("sawfish --version", "r");
	if (!pipe)
		return NULL;
	fgets (ver, 1000, pipe);	
	pclose (pipe);
	g_print ("%s\n", ver);
	if (!ver)
		return NULL;
	str = strstr (ver, "sawfish version ");
	if (!str)
		return NULL;
	len = strlen ("sawfish version ");
	ret = g_strdup (str + len);
	ret = g_strchomp (ret);
	return ret;
}

static gboolean
sawfish_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	/* FIXME */
	return TRUE;
}

static gchar*
sawfish_plugin_get_current_theme (MtmExtHandler *handler)
{
	gchar *command;
	FILE *file;
	gchar *theme;
	gchar tmp[1000];

	command = g_strdup_printf (
		"sawfish-client -e \"default-frame-style\"");
	file = popen (command, "r");
	fgets (tmp, 1000, file);
	theme = tmp;
	pclose (file);
	g_free (command);
	if (theme)
	{
		theme = g_strchomp (theme);
		return g_strdup (theme);
	}
	else
		return NULL;
}

static gchar *
sawfish_plugin_theme_find (MtmExtHandler *handler, const gchar *name,gboolean is_root) 
{
	gchar *root_prefixes[] = {"share/sawfish/themes", NULL, NULL};
	gchar *pre_prefixes[] = {"/usr/", "/usr/local/", NULL};
	gchar *user_prefixes[] = {".sawfish/themes", NULL};
	gchar **arr;
	gchar *ret;
	gchar *other;
	gchar *ver;
	int i = 0, x = 0, y = 0;
	int pre_len = 0, root_len = 0, len;

	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	/* Ugh. Clean this up later */
	if (is_root)
	{
		ver = sawfish_plugin_get_ext_version (handler);
		root_prefixes[1] = g_strconcat ("share/sawfish/", ver, "/themes", NULL);
		g_free (ver);
		
		while (pre_prefixes[pre_len])
			pre_len++;
		while (root_prefixes[root_len])
			root_len++;
		
		len = pre_len * root_len;
		arr = g_new0 (gchar*, len + 1);

		for (x = 0; x < pre_len; x++) 
			for (y = 0; y < root_len; y++)
			{
				arr[i] = g_strconcat (pre_prefixes[x],
					  	        root_prefixes[y], NULL);
				i++;
			}
	}
	else
		arr = user_prefixes;
	
	ret = mtm_find_file_in_path (arr, name, is_root);
	if (!ret)
	{
		other = g_strconcat (name, handler->default_suffix, NULL);
		ret = mtm_find_file_in_path (arr, other, is_root);
		g_free (other);
	}

	if (is_root)
	{
		i = 0;
		while (arr[i])
		{
			g_free (arr[i]);
			i++;
		}
		g_free (arr);
	}

	return ret;
}

int mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;

	g_return_val_if_fail (pd != NULL, -1);

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	pd->title = g_strdup ("Sawfish Plugin");

	env = MTM_STATEFUL (pd)->env;

	handler = mtm_ext_handler_new (env);
	
	handler->activate  = sawfish_plugin_theme_activate;
	handler->find      = sawfish_plugin_theme_find;

	handler->get_current_theme = sawfish_plugin_get_current_theme;
	handler->update_ext = sawfish_plugin_update_ext;
	handler->ext_is_installed = sawfish_plugin_ext_is_installed;
	handler->get_ext_version = sawfish_plugin_get_ext_version;
	handler->version_is_compat = sawfish_plugin_version_is_compat;
	
	MTM_HANDLER (handler)->desc = g_strdup (_("Sawfish Window Manager"));
	MTM_HANDLER (handler)->key = g_strdup ("sawfish");
	handler->editcmd = NULL;
	handler->default_suffix = g_strdup (".tar.gz");
	
	mtm_handler_register (MTM_HANDLER (handler));

	guih = mtm_gui_handler_new (env);
	guih->create_gui = sawfish_config_gui_new; 
	guih->name = g_strdup (_("Sawfish"));
	MTM_HANDLER (guih)->desc = g_strdup (_("Sawfish is a window manager which controls the appearance and behavior of your windows."));
	MTM_HANDLER (guih)->key = g_strdup ("sawfish");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}

