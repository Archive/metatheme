#ifndef __SAWFISH_CONFIG_GUI_H__ 
#define __SAWFISH_CONFIG_GUI_H__

#include <mtm/mtm-gui-handler.h>

MtmConfigGui* sawfish_config_gui_new (MtmGuiHandler *handler);

#endif /* __SAWFISH_CONFIG_GUI_H__ */
