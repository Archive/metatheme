/*
 *
 * sawfish-config-gui.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */
#include <config.h>
#include <mtm/mtm-config-gui.h>
#include "sawfish-config-gui.h"
#include <sys/types.h>
#include <dirent.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>

static gchar**
generate_file_list (void)
{
	GArray *files;
	gchar **ret;
	/* FIXME */
	gchar *dirnames[] = { "/usr/share/sawfish/themes", "/.sawfish/themes", NULL};
	int i;

	dirnames[1] = g_strconcat (g_get_home_dir (), dirnames[1], NULL);
	
	files = g_array_new (TRUE, TRUE, sizeof (gchar*));
	
	for (i = 0; dirnames[i] != NULL; i++)
	{
		DIR *dir;
		struct dirent *de;
		dir = opendir (dirnames[i]);
		if (!dir)
			continue;

		while ((de = readdir (dir)))
		{
			gchar *filename;
			
			if (de->d_name[0] == '.')
				continue;
			
			filename = g_strconcat (dirnames[i], "/", de->d_name, NULL);
			g_array_append_val (files, filename);
		}
		
		closedir (dir);
	}

	g_free (dirnames[1]);
	ret = (gchar**) files->data;
	g_array_free (files, FALSE);

	return ret;
}

/* Update the readme text */
static void
ext_changed_cb (MtmConfigGui *gui, MtmExt *ext, GtkWidget *text)
{
	gchar *file;
	gboolean is_targz;
	gchar *readme;
	GtkTextBuffer *buf;
	
	g_return_if_fail (MTM_IS_EXT (ext));
	g_return_if_fail (GTK_IS_WIDGET (text));
	
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text));
	
	if (!ext->file)
	{
		gtk_text_buffer_set_text (buf, "", 0);
		return;
	}

	is_targz = mtm_file_is_targz (ext->file);
	if (is_targz)
		file = mtm_file_open_targz (MTM_STATEFUL (ext)->env, ext->file, ".tar.gz");
	else
		file = ext->file;
	
	readme = g_strconcat (file, "/README", NULL);
	if (g_file_test (readme, G_FILE_TEST_EXISTS))
	{
		FILE *fd;
		int c;
		GArray *data;

		fd = fopen (readme, "r");
		if (fd)
		{
			data = g_array_new (TRUE, TRUE, sizeof (gchar));
			c = fgetc (fd);
			while (c != EOF)
			{
				g_array_append_val (data, c);
				c = fgetc (fd);
			}
			
			fclose (fd);

			gtk_text_buffer_set_text (buf, data->data, data->len);
			g_array_free (data, TRUE);
		}
	}
	
	g_free (readme);
	
	if (is_targz)
	{
		mtm_file_close_targz (file);
		g_free (file);
	}
}
		
MtmConfigGui*
sawfish_config_gui_new (MtmGuiHandler *handler)
{
	GtkWidget *frame;
	MtmConfigGui *gui;
	GtkWidget *text, *scrolled;
	GtkTextBuffer *buf;
	gchar **files;

	gui = mtm_config_gui_new ();
	
	files = generate_file_list ();
	mtm_config_gui_set_file_list (gui, (const gchar**) files);
	g_strfreev (files);
			
	frame = gtk_frame_new (_("Information"));
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	buf = gtk_text_buffer_new (NULL);
	text = gtk_text_view_new_with_buffer (buf); 
        gtk_container_add (GTK_CONTAINER (scrolled), text);
        gtk_container_add (GTK_CONTAINER (frame), scrolled);

	g_signal_connect (G_OBJECT (gui), "set_ext", (GCallback) ext_changed_cb, text);
	g_signal_connect (G_OBJECT (gui), "ext_modified", (GCallback) ext_changed_cb, text);
	
	mtm_config_gui_set_config_area (gui, frame); 
	gtk_widget_show_all (frame);

	return gui;
}
