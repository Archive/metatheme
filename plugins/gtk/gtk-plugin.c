/*
 *
 * gtk-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <mtm/mtm.h>
#include <gtk/gtk.h>
#include <sys/stat.h>
#include <string.h>
#include <gnome.h>

#include "gtk-config-gui.h"

/* Some of the theme activation code taken from/inspired by
 * raster's theme-switcher capplet */
#define MARK_STRING "# -- THEME AUTO-WRITTEN DO NOT EDIT"
static void print_standard_stuff (FILE *file, gchar *theme, gchar *font);
static gchar* extract_theme (gchar *line);
static gchar* font_load (const gchar *filename);

static char* get_gtk_prefix (void);
static char* get_gtkrc_location (void);

static MtmResult gtk_plugin_theme_activate  (MtmExtHandler *handler, MtmExt *theme);
static gchar *   gtk_plugin_theme_find      (MtmExtHandler *handler, const gchar *name, gboolean is_root);

static MtmResult gtk_plugin_update_ext (MtmExtHandler *handler);

static gboolean gtk_plugin_ext_is_installed (MtmExtHandler *handler);

static gchar *gtk_plugin_get_current_theme (MtmExtHandler *handler);
static gchar *gtk_plugin_get_ext_version (MtmExtHandler *handler);
static gboolean gtk_plugin_version_is_compat (MtmExtHandler *handler,
									 gchar *ver1, gchar *ver2);

static MtmResult
gtk_plugin_theme_activate (MtmExtHandler *handler, MtmExt *theme)
{
	gchar *gtkrc;
	FILE *tmp, *file;
	int fd;
	gchar tmpname[] = "/tmp/gtkrc_XXXXXX";
	int nextline = 0, hastheme = 0;
	gchar *buf;
	gchar *installed;
	gchar *font;
	gchar *base;

	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);
	
	if (theme && theme->file)
		installed = theme->file;
	else
	{
		gchar *prefix = get_gtk_prefix ();
		installed = g_build_filename (prefix, "Default", NULL);
		g_free (prefix);
	}

	g_return_val_if_fail (installed != NULL, MTM_GENERAL_ERROR);

	gtkrc = get_gtkrc_location ();
	g_return_val_if_fail (gtkrc != NULL, MTM_GENERAL_ERROR);
	font = font_load (gtkrc);
	file = fopen (gtkrc, "r+");
	if (!file)
	{
		file = fopen (gtkrc, "w");
		print_standard_stuff (file, installed, font);
		fclose (file);
		g_free (gtkrc);
		g_free (installed);
		return MTM_OK;
	}
	
	fd = mkstemp (tmpname);
	tmp = fdopen (fd, "w");
	g_return_val_if_fail (tmp != NULL, MTM_GENERAL_ERROR);
	
	/* The logic of this is copied from theme-selector.
	 * Using mtm_readline means it's not entirely efficient,
	 * but I'm more concerned about how conceptually messy this
	 * seems. Parsing sucks :-)
	 */
	while ((buf = mtm_readline (file)))
	{
		if (strcmp (MARK_STRING, buf) == 0)
			hastheme++;
		g_free (buf);
	}
	rewind (file);
	if (!hastheme)
	{
		print_standard_stuff (tmp, installed, font);
		while ((buf = mtm_readline (file)))
		{
			if (strcmp (buf, "") != 0)
				fprintf (tmp, "%s\n", buf);
			g_free (buf);
		}
	}
	else if (hastheme == 1) /* backwards compatibility */
	{
		while ((buf = mtm_readline (file)))
		{
			if (nextline == 1)
				nextline = 0;
			else if (strcmp (MARK_STRING, buf) == 0)
			{
				print_standard_stuff (tmp, installed, font);
				nextline = 1;
			}
			else if (nextline == 0 && strcmp (buf, "") != 0)
				fprintf (tmp, "%s\n", buf);
			
			g_free (buf);
		}
	}
	else
	{
		while ((buf = mtm_readline (file)))
		{
			if (strcmp (MARK_STRING, buf) == 0)
			{
				if (nextline == 0)
				{
					nextline = 1;
					print_standard_stuff (tmp, installed, font);
				}
				else
					nextline = 0;
			} else if (nextline == 0 && strcmp (buf, "") != 0)
				fprintf (tmp, "%s\n", buf);

			g_free (buf);
		}
	}
	
	fclose (tmp);
	fclose (file);

	mtm_move_file (tmpname, gtkrc);
	g_free (gtkrc);
	g_free (font);

	base = g_path_get_basename (installed);
	gnome_config_set_string ("/theme-switcher-capplet/settings/theme",
				 base);
	g_free (base);

	return MTM_OK;
}

static MtmResult
gtk_plugin_update_ext (MtmExtHandler *handler)
{
	GtkWidget *widget;
	GdkEventClient rcevent;
	int a = 1;
	gchar **b;
	static gboolean initted = 0;

	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);

	if (!initted)
	{
		b = g_new0 (char *, 1);
		b[0] = "fake";
		gtk_init (&a, &b);
		g_free (b);
		initted = TRUE;
	}

	/* Now we send a re-read signal */
	widget = gtk_label_new (".");
	gtk_widget_show (widget);
	rcevent.type = GDK_CLIENT_EVENT;
	rcevent.window = widget->window;
	rcevent.send_event = TRUE;
	rcevent.message_type = gdk_atom_intern ("_GTK_READ_RCFILES", FALSE);
	rcevent.data_format = 8;
	gdk_event_send_clientmessage_toall ((GdkEvent *)&rcevent);
	gdk_flush ();

	return MTM_OK;
}

static gchar*
gtk_plugin_get_current_theme (MtmExtHandler *handler)
{
	char *gtkrc;
	FILE *file;
	gboolean hastheme = FALSE;
	char *buf;
	char *theme = NULL;
	static gchar *tmp;

	g_return_val_if_fail (handler != NULL, NULL);

	gtkrc = get_gtkrc_location ();
	g_return_val_if_fail (gtkrc != NULL, NULL);
	file = fopen (gtkrc, "r+");
	g_return_val_if_fail (file != NULL, NULL);
	
	while ((buf = mtm_readline (file)))
	{
		if (hastheme)
		{
			theme = extract_theme (buf);
			g_free (buf);
			break;
		}
		if (strcmp (MARK_STRING, buf) == 0)
			hastheme = TRUE;
		g_free (buf);
	}
	
	g_free (gtkrc);
	fclose (file);

	tmp = mtm_strip_ext (theme, "/gtk/gtkrc");
	if (tmp)
	{
		g_free (theme);
		return tmp;
	}
	else
		return theme;
}

static gchar*
font_load (const gchar *filename)
{
	FILE *file;
	gchar *font;
	gchar *buf;
	gchar *markstr = "style \"user-font\"";
	gchar *fontstr = "font=\"";
	gboolean scanfont = FALSE;
	int i;

	g_return_val_if_fail (filename != NULL, NULL);
	
	file = fopen (filename, "r");
	if (!file)
		return NULL;
	
	/* Grab font line */
	while ((buf = mtm_readline (file)))
	{
		buf = g_strstrip (buf);
		if (scanfont)
		{
			if (!strncmp (buf, fontstr, strlen (fontstr)))
				break;
		}
		else
		{
			if (!strncmp (buf, markstr, strlen (markstr)))
				scanfont = TRUE;
		}
		g_free (buf);
		buf = NULL;
	}

	if (!buf)
		return NULL;
	
	/* Strip out relevant part */
	for (i = strlen (buf) - 1; i >= 0; i--)
	{
		if (buf[i] == '\"')
		{
			buf[i] = '\0';
			break;
		}
	}	
	
	if (i < 0)
		return NULL;

	font = g_strdup (buf + strlen (fontstr));
	g_free (buf);

	return font;
}
	
static gchar*
extract_theme (gchar *line)
{
	int len, i;
	gboolean found = FALSE;
	
	g_return_val_if_fail (line != NULL, NULL);
	
	len = strlen (line);
	
	for (i = 0; i < len; i++)
	{
		if (line[i] == 'i')
		{
			found = TRUE;
			break;
		}
	}
	
	if (found)
	{
		gchar *ret;
		ret = g_new0 (gchar, len - i - 9);
		strncpy (ret, line + i + 9, len - i - 10);
						
		return ret;
	}
	return NULL;
}

static void
print_standard_stuff (FILE *file, gchar *theme, gchar *font)
{
	g_return_if_fail (file != NULL);
	g_return_if_fail (theme != NULL);
	
	fprintf (file, "%s\n", MARK_STRING);
	fprintf (file, "include \"%s/gtk/gtkrc\"\n\n", theme);

	if (font)
	{
		fprintf (file, "\nstyle \"user-font\"\n{\nfont=\"%s\"\n", font);
		fprintf (file, "}\nwidget_class \"*\" style \"user-font\"\n\n");
	}

	fprintf (file, "include \"%s/.gtkrc.mine\"\n\n", g_get_home_dir ());
	fprintf (file, "%s\n", MARK_STRING);
}

static char*
get_gtkrc_location (void)
{
	char *gtkrc;
	const char *home = g_get_home_dir ();

	g_return_val_if_fail (home != NULL, NULL);

	gtkrc = g_strconcat (home, "/.gtkrc", NULL);
	
	return gtkrc;
}

static char* 
get_gtk_prefix (void)
{
	return gtk_rc_get_theme_dir ();
}

static gchar *
gtk_plugin_theme_find (MtmExtHandler *handler, const gchar *name, gboolean is_root)
{
	gchar *root_prefixes[] = {NULL, NULL};
	gchar *user_prefixes[] = {".themes", NULL};
	gchar **arr;
	gchar *ret;
	int i = 0;

	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (is_root)
	{
		root_prefixes[0] = get_gtk_prefix ();
		arr = root_prefixes;
	}
	else
		arr = user_prefixes;
	
	ret = mtm_find_file_in_path (arr, name, is_root);

	if (is_root)
	{
		i = 0;
		while (arr[i])
		{
			g_free (arr[i]);
			i++;
		}
	}

	return ret;
}

static gboolean
gtk_plugin_ext_is_installed (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	return TRUE; /* eep */
}

static gchar*
gtk_plugin_get_ext_version (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, NULL);
	return g_strdup ("1.2"); /* Double eep */
}

static gboolean
gtk_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	return TRUE; /* eeeeeeep */	
}

int
mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;
	
	g_return_val_if_fail (pd != NULL, -1);

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif
	
	pd->title = g_strdup ("Gtk+ plugin");

	env = MTM_STATEFUL (pd)->env;
	
	handler = mtm_ext_handler_new (env);
	handler->activate  = gtk_plugin_theme_activate;
	handler->find      = gtk_plugin_theme_find;
	
	handler->get_current_theme = gtk_plugin_get_current_theme;
	handler->update_ext = gtk_plugin_update_ext;
	handler->ext_is_installed = gtk_plugin_ext_is_installed;
	handler->get_ext_version = gtk_plugin_get_ext_version;
	handler->version_is_compat = gtk_plugin_version_is_compat;
	
	MTM_HANDLER (handler)->desc = g_strdup (_("Gtk+/GNOME widgets"));
	MTM_HANDLER (handler)->key = g_strdup ("gtk");
	handler->editcmd = NULL;
	handler->default_suffix = g_strdup (".tar.gz");
	
	mtm_handler_register (MTM_HANDLER (handler));

	guih = mtm_gui_handler_new (env);
	guih->create_gui = gtk_config_gui_new; 
	guih->name = g_strdup (_("Gtk (1.x)"));
	MTM_HANDLER (guih)->desc = g_strdup (_("Gtk widgets are the building blocks which control the overall look of applications. This extension only applies to older (1.x) gtk+ applications."));
	MTM_HANDLER (guih)->key = g_strdup ("gtk");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}
