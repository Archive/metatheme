/*
 *
 * xmms-config-gui.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include "xmms-config-gui.h"
#include <gtk/gtkpixmap.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkframe.h>
#include <gtk/gtklabel.h>
#include <gtk/gtksignal.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnome/libgnome.h>
#include <string.h>

typedef struct
{
	GtkWidget *pixmap;
	GtkWidget *label;
	GtkWidget *hbox;	
} ConfigPreview;

static void
destroy_cb (GtkWidget *widget, ConfigPreview *preview)
{
	g_free (preview);
}

static void
ext_changed_cb (MtmConfigGui *gui, MtmExt *ext, ConfigPreview *preview)
{
	GdkPixbuf *imgbuf, *buf;
	gchar *imgfile;
	GdkPixmap *pmap;
	GdkBitmap *bmap;

	g_return_if_fail (MTM_IS_CONFIG_GUI (gui));
	g_return_if_fail (MTM_IS_EXT (ext));
	g_return_if_fail (preview != NULL);

	/* Preview image stuff */
	imgfile = g_strconcat (ext->file, "/main.bmp", NULL);
	imgbuf = gdk_pixbuf_new_from_file (imgfile);
	g_free (imgfile);
	if (!imgbuf)
		return;
		
	buf = gdk_pixbuf_composite_color_simple (imgbuf,
		       				 gdk_pixbuf_get_width (imgbuf),
					 	 gdk_pixbuf_get_height (imgbuf),
						 GDK_INTERP_BILINEAR,
						 255,
						 2,
						 0xffffff,
						 0xffffff);

	gdk_pixbuf_render_pixmap_and_mask (buf, &pmap, &bmap, 255);
	
	if (preview->pixmap)
		gtk_pixmap_set (GTK_PIXMAP (preview->pixmap), pmap, bmap);
	else
	{
		preview->pixmap = gtk_pixmap_new (pmap, bmap);
		gtk_box_pack_start (GTK_BOX (preview->hbox), preview->pixmap, TRUE, FALSE, 8);
	}

	gdk_pixbuf_unref (imgbuf);
	gdk_pixbuf_unref (buf);
}

MtmConfigGui*
xmms_config_gui_new (MtmGuiHandler *handler)
{
	GtkWidget *frame;
	MtmConfigGui *gui;
	ConfigPreview *preview;
	gchar **files;
	
	g_return_val_if_fail (MTM_IS_GUI_HANDLER (handler), NULL);
	
	gui = mtm_config_gui_new ();

	files = g_new0 (gchar*, 1);
	mtm_config_gui_set_file_list (gui, files);
	g_strfreev (files);

	preview = g_new0 (ConfigPreview, 1);
	gtk_signal_connect_after (GTK_OBJECT (gui), "destroy", destroy_cb, preview);
	gtk_signal_connect (GTK_OBJECT (gui), "set_ext", ext_changed_cb, preview);
	gtk_signal_connect (GTK_OBJECT (gui), "ext_modified", ext_changed_cb, preview);

	frame = gtk_frame_new (_("Preview"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), 8);
	
	preview->hbox = gtk_hbox_new (FALSE, 4);
	gtk_container_add (GTK_CONTAINER (frame), preview->hbox);

	mtm_config_gui_set_config_area (gui, frame);
	gtk_widget_show_all (frame);

	return gui;	
}
