/*
 *
 * xmms-config-gui.h :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#ifndef __XMMS_CONFIG_HUI__
#define __XMMS_CONFIG_GUI_H__

#include <mtm/mtm-gui-handler.h>

MtmConfigGui* xmms_config_gui_new (MtmGuiHandler *handler);

#endif /* __XMMS_CONFIG_GUI_H__ */
