/*
 *
 * xmms-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <mtm/mtm.h>
#include <xmms/xmmsctrl.h>
#include <gnome.h>
#include "xmms-config-gui.h"

static MtmResult xmms_plugin_theme_activate  (MtmExtHandler *handler, MtmExt *theme);
static gchar *   xmms_plugin_theme_find      (MtmExtHandler *handler, const gchar *name, gboolean is_root);

static MtmResult xmms_plugin_update_ext (MtmExtHandler *handler);

static gboolean xmms_plugin_ext_is_installed (MtmExtHandler *handler);

static gchar *xmms_plugin_get_current_theme (MtmExtHandler *handler);
static gchar *xmms_plugin_get_ext_version (MtmExtHandler *handler);
static gboolean xmms_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2);

static MtmResult
xmms_plugin_theme_activate (MtmExtHandler *handler, MtmExt *theme)
{
	gboolean running;
	gchar *path;
	gchar *installed;

	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);	

	running = xmms_remote_is_running (0);
	path = g_strdup_printf ("=%s/%s=/xmms/skin", g_get_home_dir (), ".xmms/config");
	
	if (!theme)
	{
		if (running)
		{
			xmms_remote_set_skin (0, NULL);
		}
		else
		{
			gnome_config_clean_key (path);
			g_free (path);
		}
		g_free (path);
		return MTM_OK;
	}

	installed = theme->file;

	g_return_val_if_fail (installed != NULL, MTM_GENERAL_ERROR);
	
	if (running)
		xmms_remote_set_skin (0, installed);
	else
	{
		gnome_config_set_string (path, installed);
		gnome_config_sync ();
	}
	
	g_free (path);

	return MTM_OK;
}

static gchar *
xmms_plugin_theme_find (MtmExtHandler *handler, const gchar *name, gboolean is_root)
{
	gchar *user_prefixes[] = {".xmms/Skins", NULL};
	gchar **arr;
	gchar *ret;

	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	is_root = FALSE;
	arr = user_prefixes;
	
	ret = mtm_find_file_in_path (arr, name, is_root);

	return ret;
}



static MtmResult
xmms_plugin_update_ext (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, MTM_GENERAL_ERROR);

	return MTM_OK;
}

static gchar*
xmms_plugin_get_current_theme (MtmExtHandler *handler)
{
	gboolean running;
	gchar *ret;
	
	g_return_val_if_fail (handler != NULL, NULL);
	
	running = xmms_remote_is_running (0);

	if (running)
	{
		ret = g_strdup (xmms_remote_get_skin (0));
	}
	else
	{
		gchar *path = g_strdup_printf ("=%s/%s=/xmms/skin", g_get_home_dir (), ".xmms/config");
		ret = gnome_config_get_string (path);
		g_free (path);
	}

	return ret;
}


static gboolean
xmms_plugin_ext_is_installed (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	return TRUE; /* eep */
}

static gchar*
xmms_plugin_get_ext_version (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, NULL);
	return g_strdup ("1.2"); /* Double eep */
}

static gboolean
xmms_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	return TRUE; /* eeeeeeep */	
}

int
mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;
	
	g_return_val_if_fail (pd != NULL, -1);
	
#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif

	pd->title = g_strdup (_("XMMS plugin"));

	env = MTM_STATEFUL (pd)->env;
	
	handler = mtm_ext_handler_new (env);

	handler->activate  = xmms_plugin_theme_activate;
	handler->find      = xmms_plugin_theme_find;

	handler->get_current_theme = xmms_plugin_get_current_theme;
	handler->update_ext = xmms_plugin_update_ext;
	handler->ext_is_installed = xmms_plugin_ext_is_installed;
	handler->get_ext_version = xmms_plugin_get_ext_version;
	handler->version_is_compat = xmms_plugin_version_is_compat;
	
	MTM_HANDLER (handler)->desc = g_strdup (_("XMMS skins"));
	MTM_HANDLER (handler)->key = g_strdup ("xmms");
	handler->editcmd = NULL;
	handler->default_suffix = g_strdup (".zip");
	
	mtm_handler_register (MTM_HANDLER (handler));

	guih = mtm_gui_handler_new (env);
	guih->create_gui = xmms_config_gui_new;
	guih->name = g_strdup (_("XMMS"));
	MTM_HANDLER (guih)->desc = g_strdup (_("XMMS is a cross-platform media player."));
	MTM_HANDLER (guih)->key = g_strdup ("xmms");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}
