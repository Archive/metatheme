#ifndef __GTK_CONFIG_GUI_H__ 
#define __GTK_CONFIG_GUI_H__

#include <mtm/mtm-gui-handler.h>

MtmConfigGui* gtk2_config_gui_new (MtmGuiHandler *handler);

#endif /* __GTK_CONFIG_GUI_H__ */
