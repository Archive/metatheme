/*
 *
 * gtk-config-gui.c :
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <mtm/mtm-config-gui.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include <bonobo.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "gtk-config-gui.h"

typedef struct 
{
	MtmExt *ext;
	GtkWidget *demo;
} ConfigPreview;

static void preview_gtk_theme (ConfigPreview *preview, gchar *theme);

static gchar**
generate_file_list (void)
{
	GArray *files;
	gchar **ret;
	/* FIXME */
	gchar *dirnames[] = { "/usr/share/themes", "/.themes", NULL};
	int i;

	dirnames[1] = g_strconcat (g_get_home_dir (), dirnames[1], NULL);
	
	files = g_array_new (TRUE, TRUE, sizeof (gchar*));
	
	for (i = 0; dirnames[i] != NULL; i++)
	{
		DIR *dir;
		struct dirent *de;
		dir = opendir (dirnames[i]);
		if (!dir)
			continue;

		while ((de = readdir (dir)))
		{
			gchar *tmp;
			gchar *filename;
			struct stat buf;
			
			if (de->d_name[0] == '.')
				continue;
			
			filename = g_strconcat (dirnames[i], "/", de->d_name, NULL);
			tmp = g_strconcat (filename, "/gtk/gtkrc", NULL);
			if (stat (tmp, &buf) == 0) 
				g_array_append_val (files, filename);
			else
				g_free (filename);

			g_free (tmp);
		}
		
		closedir (dir);
	}


	g_free (dirnames[1]);
	ret = (gchar**) files->data;
	g_array_free (files, FALSE);

	return ret;
}

static void
ext_changed_cb (MtmConfigGui *gui, MtmExt *ext, ConfigPreview *preview)
{
	g_return_if_fail (MTM_IS_EXT (ext));
	preview_gtk_theme (preview, ext->file);
}
		
static void
destroy_cb (GtkWidget *widget, ConfigPreview *preview)
{
	g_free (preview);
}

MtmConfigGui*
gtk2_config_gui_new (MtmGuiHandler *handler)
{
	GtkWidget *frame, *vbox, *demo;
	MtmConfigGui *gui;
	ConfigPreview *preview;
	gchar **files;

	demo = bonobo_widget_new_control ("OAFIID:GNOME_Theme_Preview", CORBA_OBJECT_NIL);
	gui = mtm_config_gui_new ();
	
	files = generate_file_list ();
	mtm_config_gui_set_file_list (gui, (const gchar**) files);
	g_strfreev (files);
			
	if (!demo)
		return NULL;
	
	preview = g_new0 (ConfigPreview, 1);
	preview->ext = NULL;
	preview->demo = demo;
	
	g_signal_connect_after (G_OBJECT (gui), "destroy", (GCallback) destroy_cb, preview);
	g_signal_connect (G_OBJECT (gui), "set_ext", (GCallback) ext_changed_cb, preview);
	g_signal_connect (G_OBJECT (gui), "ext_modified", (GCallback) ext_changed_cb, preview);

	vbox = gtk_vbox_new (FALSE, 0);
	frame = gtk_frame_new (_("Preview"));
        gtk_container_add (GTK_CONTAINER (frame), demo);
	gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
	mtm_config_gui_set_config_area (gui, vbox); 
	gtk_widget_show_all (vbox);

	return gui;
}

static void
preview_gtk_theme (ConfigPreview *preview, gchar *theme)
{
	gchar *gtkrc = g_build_filename (theme, "gtk-2.0", "gtkrc", NULL);
	bonobo_widget_set_property (BONOBO_WIDGET (preview->demo),
				    "theme", TC_CORBA_string, gtkrc,
				    NULL);
	g_free (gtkrc);
}

#if 0
static void
reset_gtk_theme (void)
{
        gchar *line;

        line = g_strdup_printf ("%s/.gtkrc", g_get_home_dir ());

        if (!exists (line)) {
                g_free (line);
                line = g_strdup (GTK_THEMEDIR "/Default/gtk/gtkrc");
        }

        cp (line, gtkrc_path);

        g_free (line);

        update_display ();
}
#endif
