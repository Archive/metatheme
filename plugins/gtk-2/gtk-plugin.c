/*
 *
 * gtk-plugin.c :
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Rachel Hestilow <hestilow@ximian.com>
 *
 */

#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <mtm/mtm.h>
#include <gtk/gtk.h>
#include <sys/stat.h>
#include <string.h>
#include <gnome.h>
#include <gconf/gconf-client.h>
#include "gtk-config-gui.h"

static char* get_gtk_prefix (void);

static MtmResult gtk_plugin_theme_activate  (MtmExtHandler *handler, MtmExt *theme);
static gchar *   gtk_plugin_theme_find      (MtmExtHandler *handler, const gchar *name, gboolean is_root);

static MtmResult gtk_plugin_update_ext (MtmExtHandler *handler);

static gboolean gtk_plugin_ext_is_installed (MtmExtHandler *handler);

static gchar *gtk_plugin_get_current_theme (MtmExtHandler *handler);
static gchar *gtk_plugin_get_ext_version (MtmExtHandler *handler);
static gboolean gtk_plugin_version_is_compat (MtmExtHandler *handler,
									 gchar *ver1, gchar *ver2);

static MtmResult
gtk_plugin_theme_activate (MtmExtHandler *handler, MtmExt *ext)
{
	gchar *theme;
	if (ext && ext->file)
	{
		gchar *path, *newfile, *base;
		struct stat s;
		
		theme = ext->file;
		base = g_path_get_basename (ext->file);
		
		/* Let's make a symlink because gtk+ is dumb */
		path = g_build_filename (g_get_home_dir (), ".themes", NULL);
		mtm_check_dir (path);
		newfile = g_build_filename (path, theme, NULL);
		
		if (lstat (newfile, &s) == 0)
		{
			if (S_ISLNK (s.st_mode))
				unlink (newfile);
		}

		if (!g_file_test (newfile, G_FILE_TEST_EXISTS))
			symlink (ext->file, newfile);
		g_free (base);
		g_free (path);
		g_free (newfile);
	}
	else
		theme = "Default";

	
	gconf_client_set_string (gconf_client_get_default (), "/desktop/gnome/interface/gtk_theme", theme, NULL);
	
	return MTM_OK;
}

static MtmResult
gtk_plugin_update_ext (MtmExtHandler *handler)
{
	gconf_client_suggest_sync (gconf_client_get_default (), NULL);
	return MTM_OK;
}

static gchar*
gtk_plugin_get_current_theme (MtmExtHandler *handler)
{
	return gconf_client_get_string (gconf_client_get_default (), "/desktop/gnome/interface/gtk_theme", NULL);
}

static char* 
get_gtk_prefix (void)
{
	return gtk_rc_get_theme_dir ();
}

static gchar *
gtk_plugin_theme_find (MtmExtHandler *handler, const gchar *name, gboolean is_root)
{
	gchar *root_prefixes[] = {NULL, NULL};
	gchar *user_prefixes[] = {".themes", NULL};
	gchar **arr;
	gchar *ret;
	int i = 0;

	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (is_root)
	{
		root_prefixes[0] = get_gtk_prefix ();
		arr = root_prefixes;
	}
	else
		arr = user_prefixes;
	
	ret = mtm_find_file_in_path (arr, name, is_root);

	if (is_root)
	{
		i = 0;
		while (arr[i])
		{
			g_free (arr[i]);
			i++;
		}
	}

	return ret;
}

static gboolean
gtk_plugin_ext_is_installed (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	return TRUE; /* eep */
}

static gchar*
gtk_plugin_get_ext_version (MtmExtHandler *handler)
{
	g_return_val_if_fail (handler != NULL, NULL);
	return g_strdup ("2.0"); /* Double eep */
}

static gboolean
gtk_plugin_version_is_compat (MtmExtHandler *handler,
	gchar *ver1, gchar *ver2)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (ver1 != NULL, FALSE);
	g_return_val_if_fail (ver2 != NULL, FALSE);

	return TRUE; /* eeeeeeep */	
}

int
mtm_init_plugin (MtmPlugin *pd)
{
	MtmExtHandler *handler;
	MtmGuiHandler *guih;
	MtmEnv *env;
	
	g_return_val_if_fail (pd != NULL, -1);

#ifdef ENABLE_NLS
	/* Initialize the i18n stuff */
	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif
	
	pd->title = g_strdup ("Gtk+ plugin");

	env = MTM_STATEFUL (pd)->env;
	
	handler = mtm_ext_handler_new (env);
	handler->activate  = gtk_plugin_theme_activate;
	handler->find      = gtk_plugin_theme_find;
	
	handler->get_current_theme = gtk_plugin_get_current_theme;
	handler->update_ext = gtk_plugin_update_ext;
	handler->ext_is_installed = gtk_plugin_ext_is_installed;
	handler->get_ext_version = gtk_plugin_get_ext_version;
	handler->version_is_compat = gtk_plugin_version_is_compat;
	
	MTM_HANDLER (handler)->desc = g_strdup (_("Gtk+/GNOME 2.0 widgets"));
	MTM_HANDLER (handler)->key = g_strdup ("gtk-2");
	handler->editcmd = NULL;
	handler->default_suffix = g_strdup (".tar.gz");
	
	mtm_handler_register (MTM_HANDLER (handler));

	guih = mtm_gui_handler_new (env);
	guih->create_gui = gtk2_config_gui_new; 
	guih->name = g_strdup (_("Gtk"));
	MTM_HANDLER (guih)->desc = g_strdup (_("Gtk widgets are the building blocks which control the overall look of applications. The Gtk+ theme also controls the icons used by applications."));
	MTM_HANDLER (guih)->key = g_strdup ("gtk-2");

	mtm_handler_register (MTM_HANDLER (guih));

	return 1;
}
